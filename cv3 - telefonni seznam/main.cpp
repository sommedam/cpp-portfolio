#ifndef __PROGTEST__
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <stdexcept>
#include <vector>
#include <array>
#include <cassert>
using namespace std;
#endif /* __PROGTEST__ */
bool validateRecordVector(const vector<string> & exp);
bool checkName(const string & name);
vector<string> explode(string & str);

class Record {
    string name;
    string surname;
    string telephone;

public:
    Record(const string &name, const string &surname, const string &telephone) : name(name), surname(surname),
                                                                                 telephone(telephone) {

    }

    const string &getName() const {
        return name;
    }

    void setName(const string &name) {
        Record::name = name;
    }

    const string &getSurname() const {
        return surname;
    }

    void setSurname(const string &surname) {
        Record::surname = surname;
    }

    const string &getTelephone() const {
        return telephone;
    }

    void setTelephone(const string &telephone) {
        Record::telephone = telephone;
    }
};

bool report ( const string & fileName, ostream & out ) {
    vector<Record> records;

    string line;
    ifstream input(fileName);
    if(!input.good()){return false;}

    while (getline(input, line)) {
        //cout << line << endl << endl;
        vector<string> exp = explode(line);
        if (exp[0].empty() &&
            exp[1].empty() &&
            exp[2].empty()) {
            break;
        }
        if (validateRecordVector(exp) == 0) {
            return false;
        }

        //cout << "name: " << exp[0] << endl;
        //cout << "sur: " << exp[1] << endl;
        //cout << "num: " << exp[2] << endl;
        records.emplace_back(exp[0], exp[1], exp[2]);
    }

    // TODO nula vyhledavacich retezcu
    while (getline(input, line)) {
        if (!checkName(line)) {
            return false;
        } else {
            int count = 0;
            for (Record &rec: records) {
                if (rec.getName()==line||
                    rec.getSurname()==line) {
                    out << rec.getName() << " " << rec.getSurname() << " " << rec.getTelephone() << "\n";
                    count++;
                }
            }
            out << "-> " << count << "\n";
        }
    }


    input.close();
    return true;
}



bool checkName(const string & name){

    if(name.empty()){
        return false;
    }

    for (char i: name) {
        if (!(isalpha(i))&&i!='-') {
            return false;
        }
    }
    return true;
}

bool checkNumber(const string & number) {
    if (number.empty() || number.size() != 9 || number[0] == '0') {
        return false;
    }
    for (char i: number) {
        if (!(isdigit(i))) {
            return false;
        }
    }
    return true;
}

vector<string> explode(string & str) {
    vector<string> result;
    istringstream iss(str);
    string add;
    string name, surname, tel, balast;
    iss >> name >> surname >> tel >> balast;

    result.push_back(name);
    result.push_back(surname);
    result.push_back(tel);
    result.push_back(balast);
    return result;
}

bool validateRecordVector(const vector<string> & exp){
    bool success = true;
    success &= checkName(exp[0]);
    success &= checkName(exp[1]);
    success &= checkNumber(exp[2]);
    for(char c : exp[3]){
        if(!isspace(c)){
            return false;
        }
    }

    return success;
}

#ifndef __PROGTEST__
int main () {

    ostringstream oss,oss2;
    //cout<<boolalpha<<report("test1_in.txt",oss);
    //cout<<oss.str();
    oss.str("");
    assert(report("test4_in.txt",oss2)== true);
    cout<<oss2.str()<<boolalpha<<report("test4_in.txt",oss2);
    assert(checkName("Knox-Marie"));
 //   assert(report("test3_in.txt",oss2)== true);
    /*assert (report("test0_in.txt", oss) == true);
    cout<<oss.str();
    assert (oss.str() ==
            "John Christescu 258452362\n"
            "John Harmson 861647702\n"
            "-> 2\n"
            "-> 0\n"
            "Josh Dakhov 264112084\n"
            "Dakhov Speechley 865216101\n"
            "-> 2\n"
            "John Harmson 861647702\n"
            "-> 1\n");
    oss.str("");
    assert (report("test1_in.txt", oss) == false);


    vector<string> working;
    working.emplace_back("John");
    working.emplace_back("Doe");
    working.emplace_back("505123456");
    working.emplace_back("");
    string s1 = "John Doe 505123456";
    assert(explode(s1) == working);

    vector<string> n1;
    n1.emplace_back("John");
    n1.emplace_back("Doe");
    n1.emplace_back("505123456");
    n1.emplace_back(".");
    assert(!validateRecordVector(n1));
    n1.pop_back();
    n1.emplace_back("   ");
    assert(validateRecordVector(n1));

    assert(!checkNumber("1"));
    assert(!checkNumber(""));
    assert(!checkNumber("019283942"));
    assert(!checkNumber("3928102930"));
    assert(!checkNumber("3232309a89"));
    assert(checkNumber("221323121"));
    assert(checkNumber("605776637"));*/
    return 0;
}
#endif /* __PROGTEST__ */
