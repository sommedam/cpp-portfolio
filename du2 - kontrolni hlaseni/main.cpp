#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <cmath>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <string>
#include <utility>
#include <vector>
#include <list>
#include <algorithm>
#include <memory>
#endif /* __PROGTEST__ */
using namespace std;
void assertv();

// todo: Tohle mozna zabira prilis mnoho pameti na stacku
string toLowercaseAscii(string caseSensitive){
    string caseInsensitive;
    for (unsigned char c :caseSensitive) {
        caseInsensitive+= tolower(c);
    }
    return caseInsensitive;
}

/*class Invoice{
public:
    unsigned int amount;
    explicit Invoice(unsigned int amount) : amount(amount) {}
};*/

class Company {
public:
    string name;
    // TODO: odstranit
    //vector<Invoice *> invoices;
    string address;
    unsigned int invoicesSum = 0;
    string id;

    virtual ~Company() {
        //invoices.clear();
    }

    Company(const string &name, const string &address, const string &id) : name(name), address(address), id(id) {}

    void addInvoice(unsigned int amount) {
        invoicesSum += amount;
    }
};

struct CompanyIdOrder
{
    bool operator() (Company * l, const string& r)const
    {
        if(l== nullptr){return false;}
        return l->id < r;
    }
};

struct InvoiceAmountOrder
{
    bool operator() (const unsigned int l, const unsigned int r)const
    {
        return l < r;
    }
};

struct CompanyNameAdressOrderCaseInsensitive
{
    bool operator() (Company * l, Company * r)const {
        if(l== nullptr||r== nullptr){return false;}
        if (toLowercaseAscii(l->name) == toLowercaseAscii(r->name)) {
            return toLowercaseAscii(l->address) < toLowercaseAscii(r->address);
        } else {
            return toLowercaseAscii(l->name) < toLowercaseAscii(r->name);
        }
    }
    bool operator() (Company * l, const Company& r)const {
        if (toLowercaseAscii(l->name) == toLowercaseAscii(r.name)) {
            return toLowercaseAscii(l->address) < toLowercaseAscii(r.address);
        } else {
            return toLowercaseAscii(l->name) < toLowercaseAscii(r.name);
        }
    }
};

class CVATRegister
{
    vector<Company*>companyIdsIndex;
    vector<Company*>companyNameAddressIndex;
    vector<int>invoiceAmountIndex;

public:
    CVATRegister   ()= default;
    ~CVATRegister  (){
        for(auto & i : companyIdsIndex){
            delete i;
        }
        companyIdsIndex.clear();
        companyNameAddressIndex.clear();
        invoiceAmountIndex.clear();
    }
    bool          newCompany     ( const string    & name,
                                   const string    & addr,
                                   const string    & taxID ) {
        auto *tmp = new Company(name, addr, taxID);

        auto idIndexSearch = lower_bound(companyIdsIndex.begin(), companyIdsIndex.end(), taxID, CompanyIdOrder());
        auto nameAdressIndexSearch = lower_bound(companyNameAddressIndex.begin(), companyNameAddressIndex.end(), tmp, CompanyNameAdressOrderCaseInsensitive());

        // Existuje jiz jiny zaznam se stejnym ID. ID je unikatni
        if(idIndexSearch != companyIdsIndex.end() &&
        idIndexSearch.operator*()->id==taxID){
            delete tmp;
            return false;
        }
        // Existuje jiz jiny zaznam se stejnym jmenem a adresou
        if (nameAdressIndexSearch != companyNameAddressIndex.end() &&
                toLowercaseAscii(nameAdressIndexSearch.operator*()->name) == toLowercaseAscii(name) &&
                toLowercaseAscii(nameAdressIndexSearch.operator*()->address) == toLowercaseAscii(addr)) {
            delete tmp;
            return false;
        }
        companyIdsIndex.insert(idIndexSearch, tmp);
        companyNameAddressIndex.insert(nameAdressIndexSearch, tmp);
        return true;
    }

    bool cancelCompany  ( const string    & name,
                                   const string    & addr ){
        Company toCompare(name,addr,"");
        auto companyNameAdressItr = lower_bound(companyNameAddressIndex.begin(), companyNameAddressIndex.end(), toCompare, CompanyNameAdressOrderCaseInsensitive());
        if(companyNameAdressItr == companyNameAddressIndex.end() ||
                toLowercaseAscii(companyNameAdressItr.operator*()->name) != toLowercaseAscii(name) ||
                toLowercaseAscii(companyNameAdressItr.operator*()->address) != toLowercaseAscii(addr)){
            // Nebyla nalezena firma
            return false;
        }else{
            auto companyIdItr = lower_bound(companyIdsIndex.begin(), companyIdsIndex.end(),companyNameAdressItr.operator*()->id,CompanyIdOrder());
            // Vymazat firmu v pameti, smazat ji z vektorovych indexaci
            delete companyNameAdressItr.operator*();

            companyNameAddressIndex.erase(companyNameAdressItr);
            companyIdsIndex.erase(companyIdItr);
            return true;
        }
    }
    bool cancelCompany  ( const string    & taxID ){
        auto companyIdItr = lower_bound(companyIdsIndex.begin(), companyIdsIndex.end(), taxID, CompanyIdOrder());
        if(companyIdItr == companyIdsIndex.end() ||
           companyIdItr.operator*()->id != taxID){
            // Nebyla nalezena firma
            return false;
        }else{
            auto compareCompany = Company(companyIdItr.operator*()->name,companyIdItr.operator*()->address,"");
            auto companyNameAddressItr = lower_bound(companyNameAddressIndex.begin(), companyNameAddressIndex.end(), compareCompany, CompanyNameAdressOrderCaseInsensitive());
            // Vymazat firmu v pameti, smazat ji z vektorovych indexaci
            delete companyIdItr.operator*();

            companyNameAddressIndex.erase(companyNameAddressItr);
            companyIdsIndex.erase(companyIdItr);
            return true;
        }
    }

private:
    void addInvoice(Company * company,unsigned int & amount){
        auto invoiceAmountSearch = lower_bound(invoiceAmountIndex.begin(), invoiceAmountIndex.end(), amount, InvoiceAmountOrder());
        // U firmy chce znat jen soucet, bez dalsi Indexace
        company->addInvoice(amount);
        // U smluv se tridi podle hodnoty, protoze je potreba znat median
        invoiceAmountIndex.insert(invoiceAmountSearch, amount);
    }
// mozna segfault byl zpusobeny tim ze v jednom vektoru bylo vice zaznamu nez v druhem a nestastne se mazali oba
public:
    bool          invoice        ( const string    & taxID,
                                   unsigned int      amount ){
        // Najit firmu, ke ktere patri smlouva
        auto company = lower_bound(companyIdsIndex.begin(), companyIdsIndex.end(), taxID, CompanyIdOrder());
        // Pokud neexistuje takovy podnik
        if(company == companyIdsIndex.end() ||
            company.operator*()->id!=taxID){
            return false;
        }else{
            addInvoice(company.operator*(),amount);
            return true;
        }
    }
    bool          invoice        ( const string    & name,
                                   const string    & addr,
                                   unsigned int      amount ){
        // Najit firmu, ke ktere patri smlouva
        Company compareCompany(name,addr,"");
        auto company = lower_bound(companyNameAddressIndex.begin(), companyNameAddressIndex.end(), compareCompany, CompanyNameAdressOrderCaseInsensitive());
        // Pokud neexistuje takovy podnik
        if(company == companyNameAddressIndex.end() ||
           (toLowercaseAscii(company.operator*()->name)!= toLowercaseAscii(name)||
                   toLowercaseAscii(company.operator*()->address)!= toLowercaseAscii(addr))){
            return false;
        }else{
            addInvoice(company.operator*(),amount);
            return true;
        }
    }
    bool          audit          ( const string    & nameid,
                                   const string    & addr,
                                   unsigned int    & sumIncome ) const{
        Company compareCompany(nameid,addr,"");
        auto company = lower_bound(companyNameAddressIndex.begin(), companyNameAddressIndex.end(), compareCompany, CompanyNameAdressOrderCaseInsensitive());
        if(company==companyNameAddressIndex.end()||
                toLowercaseAscii(company.operator*()->name)!= toLowercaseAscii(nameid)||
                toLowercaseAscii(company.operator*()->address)!= toLowercaseAscii(addr)){
            return false;
        }else{
            sumIncome=company.operator*()->invoicesSum;
            return true;
        }
    }
    bool          audit          ( const string    & taxID,
                                   unsigned int    & sumIncome ) const{
        auto company = lower_bound(companyIdsIndex.begin(), companyIdsIndex.end(), taxID, CompanyIdOrder());
        if(company==companyNameAddressIndex.end() ||
           company.operator*()->id!=taxID){
            return false;
        }else{
            sumIncome=company.operator*()->invoicesSum;
            return true;
        }
    }

    bool          firstCompany   ( string          & name,
                                   string          & addr ) const {
        if(companyNameAddressIndex.empty()){return false;
        }else{
            name=companyNameAddressIndex.at(0)->name;
            addr=companyNameAddressIndex.at(0)->address;
            return true;
        }
    }
    bool          nextCompany    ( string          & name,
                                   string          & addr ) const{
        Company compareCompany(name,addr,"");
        auto company = lower_bound(companyNameAddressIndex.begin(), companyNameAddressIndex.end(), compareCompany, CompanyNameAdressOrderCaseInsensitive());
        // Pokud neexistuje takovy podnik
        if(company == companyNameAddressIndex.end() ||
                toLowercaseAscii(company.operator*()->name)!= toLowercaseAscii(name)||
                toLowercaseAscii(company.operator*()->address)!= toLowercaseAscii(addr)){
            // I v pripade, kdyby neexistovala
            return false;
        }else{
            // Pokud nasledujici neexistuje
            if(++company==companyNameAddressIndex.end()) {
                return false;
            }
            name=company.operator*()->name;
            addr=company.operator*()->address;
            return true;
        }
    }

    unsigned int  medianInvoice  ( ) const {
        if (invoiceAmountIndex.empty()) { return 0; }
        uint32_t middleIndex=median(invoiceAmountIndex.size());
        // Nemuze se nikdy stat
        if(middleIndex-1<0||middleIndex-1>= invoiceAmountIndex.size()){return false;}
        return invoiceAmountIndex.at(middleIndex-1);
    }

    static unsigned int median(uint32_t length){
        if (length == 1) { return 1; }
        if ((length % 2)==0) {
            return (length / 2)+1;
        } else {
            return ceil((double )length / 2);
        }
    }
};

#ifndef __PROGTEST__
int               main           ( void )
{
    cout<<"compiled"<<endl;
  //assertv();

    string name, addr;
    unsigned int sumIncome;

    CVATRegister b1;
    assert ( b1 . newCompany ( "ACME", "Thakurova", "666/666" ) );
    assert ( b1 . newCompany ( "ACME", "Kolejni", "666/666/666" ) );
    assert ( b1 . newCompany ( "Dummy", "Thakurova", "123456" ) );
    assert ( b1 . invoice ( "666/666", 2000 ) );
    assert ( b1 . medianInvoice () == 2000 );
    assert ( b1 . invoice ( "666/666/666", 3000 ) );
    assert ( b1 . medianInvoice () == 3000 );
    assert ( b1 . invoice ( "123456", 4000 ) );
    assert ( b1 . medianInvoice () == 3000 );
    assert ( b1 . invoice ( "aCmE", "Kolejni", 5000 ) );
    assert ( b1 . medianInvoice () == 4000 );
    assert ( b1 . audit ( "ACME", "Kolejni", sumIncome ) && sumIncome == 8000 );
    assert ( b1 . audit ( "123456", sumIncome ) && sumIncome == 4000 );
    assert ( b1 . firstCompany ( name, addr ) && name == "ACME" && addr == "Kolejni" );
    assert ( b1 . nextCompany ( name, addr ) && name == "ACME" && addr == "Thakurova" );
    assert ( b1 . nextCompany ( name, addr ) && name == "Dummy" && addr == "Thakurova" );
    assert ( ! b1 . nextCompany ( name, addr ) );
    assert ( b1 . cancelCompany ( "ACME", "KoLeJnI" ) );
    assert ( b1 . medianInvoice () == 4000 );
    assert ( b1 . cancelCompany ( "666/666" ) );
    assert ( b1 . medianInvoice () == 4000 );
    assert ( b1 . invoice ( "123456", 100 ) );
    assert ( b1 . medianInvoice () == 3000 );
    assert ( b1 . invoice ( "123456", 300 ) );
    assert ( b1 . medianInvoice () == 3000 );
    assert ( b1 . invoice ( "123456", 200 ) );
    assert ( b1 . medianInvoice () == 2000 );
    assert ( b1 . invoice ( "123456", 230 ) );
    assert ( b1 . medianInvoice () == 2000 );
    assert ( b1 . invoice ( "123456", 830 ) );
    assert ( b1 . medianInvoice () == 830 );
    assert ( b1 . invoice ( "123456", 1830 ) );
    assert ( b1 . medianInvoice () == 1830 );
    assert ( b1 . invoice ( "123456", 2830 ) );
    assert ( b1 . medianInvoice () == 1830 );
    assert ( b1 . invoice ( "123456", 2830 ) );
    assert ( b1 . medianInvoice () == 2000 );
    assert ( b1 . invoice ( "123456", 3200 ) );
    assert ( b1 . medianInvoice () == 2000 );
    assert ( b1 . firstCompany ( name, addr ) && name == "Dummy" && addr == "Thakurova" );
    assert ( ! b1 . nextCompany ( name, addr ) );
    assert ( b1 . cancelCompany ( "123456" ) );
    assert ( ! b1 . firstCompany ( name, addr ) );

    CVATRegister b2;
    assert ( b2 . newCompany ( "ACME", "Kolejni", "abcdef" ) );
    assert ( b2 . newCompany ( "Dummy", "Kolejni", "123456" ) );
    assert ( ! b2 . newCompany ( "AcMe", "kOlEjNi", "1234" ) );
    assert ( b2 . newCompany ( "Dummy", "Thakurova", "ABCDEF" ) );
    assert ( b2 . medianInvoice () == 0 );
    assert ( b2 . invoice ( "ABCDEF", 1000 ) );
    assert ( b2 . medianInvoice () == 1000 );
    assert ( b2 . invoice ( "abcdef", 2000 ) );
    assert ( b2 . medianInvoice () == 2000 );
    assert ( b2 . invoice ( "aCMe", "kOlEjNi", 3000 ) );
    assert ( b2 . medianInvoice () == 2000 );
    assert ( ! b2 . invoice ( "1234567", 100 ) );
    assert ( ! b2 . invoice ( "ACE", "Kolejni", 100 ) );
    assert ( ! b2 . invoice ( "ACME", "Thakurova", 100 ) );
    assert ( ! b2 . audit ( "1234567", sumIncome ) );
    assert ( ! b2 . audit ( "ACE", "Kolejni", sumIncome ) );
    assert ( ! b2 . audit ( "ACME", "Thakurova", sumIncome ) );
    assert ( ! b2 . cancelCompany ( "1234567" ) );
    assert ( ! b2 . cancelCompany ( "ACE", "Kolejni" ) );
    assert ( ! b2 . cancelCompany ( "ACME", "Thakurova" ) );
    assert ( b2 . cancelCompany ( "abcdef" ) );
    assert ( b2 . medianInvoice () == 2000 );
    assert ( ! b2 . cancelCompany ( "abcdef" ) );
    assert ( b2 . newCompany ( "ACME", "Kolejni", "abcdef" ) );
    assert ( b2 . cancelCompany ( "ACME", "Kolejni" ) );
    assert ( ! b2 . cancelCompany ( "ACME", "Kolejni" ) );
    return EXIT_SUCCESS;
}

bool testNameOrder(Company*l,Company*r) {
    if (l->name == r->name) {
        return l->address < r->address;
    } else {
        return l->name < r->name;
    }
}

bool testIdOrder(Company*l,string id) {
    return l->id < id;
}

void assertv() {
    assert((string)"0"<(string)"a");
    assert((string)"0"<(string)"A");
    assert(toLowercaseAscii("Ahoj")=="ahoj");
    assert(toLowercaseAscii("PoPoCaTePETL")=="popocatepetl");

    auto *a = new Company("Adam", "Xiaova", "abrakadabra21121kakar");
    auto *a2 = new Company("Adam", "Andeova", "abrakadabra21121kakaa");
    auto *a3 = new Company("Adam", "Andeova", "abrakadabra21121kakar");
    auto *a4 = new Company("Adam", "Xiaova", "1");
    auto *a5 = new Company("Adam", "Xiaov", "2");
    auto *b = new Company("Ben", "Jaburkove", "3");

    CVATRegister t1;
    assert(t1.newCompany(a->name, a->address, a->id));
    assert(t1.newCompany(b->name, b->address, b->id));
    // Identicky
    assert(!t1.newCompany(a->name, a->address, a->id));
    // Stejne ID
    assert(!t1.newCompany(a3->name, a3->address, a3->id));
    // Rozdilne ID, ale stejne jmeno a adresa
    assert(!t1.newCompany(a4->name, a4->address, a4->id));
    assert(t1.newCompany(a5->name, a5->address, a5->id));
    // Ruzna jmena
    assert(testNameOrder(a, b));
    assert(!testNameOrder(b, a));
    // Stejne zaznamy
    assert(!testNameOrder(a, a));
    // Strejna jmena, jina adresa
    assert(!testNameOrder(a, a2));
    assert(testNameOrder(a2, a));
    assert(!testIdOrder(a, a2->id));

    // Id
    assert(t1.invoice(a->id, 100));
    assert(t1.invoice(a->id, 300));
    assert(t1.invoice(a->id, 200));
    // Neexistujici
    assert(!t1.invoice("Velmi dlouhy text, ktery bude umisten na konec", 9999));
    // Neexistujici
    assert(!t1.invoice("Kratky text", 9999));
    // Adresa, jmeno
    assert(t1.invoice(a->name, a->address, 150));
    assert(t1.invoice(a->name, a->address, 150));
    assert(t1.invoice(a->name, a->address, 700));
    assert(!t1.invoice(a->name, "9999", 400));

    unsigned int median = t1.medianInvoice();
    assert(median==200);
    assert(t1.cancelCompany(a->name,a->address));
    assert(t1.medianInvoice()==median);

    assert(t1.median(1)==1);
    assert(t1.median(2)==2);
    assert(t1.median(3)==2);
    assert(t1.median(4)==3);
    assert(t1.median(5)==3);

    string nn;
    string aa;
    string & name=nn;
    string & addr=aa;
    t1.nextCompany(name,addr);

    cout<<endl;
}
#endif /* __PROGTEST__ */