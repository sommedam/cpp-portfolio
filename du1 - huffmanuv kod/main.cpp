#ifndef __PROGTEST__
#include <cstdint>
#include <iostream>
#include <fstream>
#include <vector>
#include <stdexcept>
#include <cassert>

using namespace std;

#endif /* __PROGTEST__ */

void assertv();

/**
     * Rozdeli int na vektor Bytu.
     *
     * @param i cislo k rozdeleni
     * @return vektor velikosti 4. [0] - prvni byte [3] - posledni byte cisla (big-endian)
*/
vector<unsigned char> int32toChar(uint32_t i){
    vector<unsigned char> r;
    if((i>>24)!=0)
        r.push_back((i>>24));
    if(((i<<8)>>24)!=0)
        r.push_back(((i<<8)>>24));
    if(((i<<16)>>24)!=0)
        r.push_back(((i<<16)>>24));
    r.push_back(((i<<24)>>24));
    return r;
}

/**
     * Zjisti zda 4 Bytovy UTF8 splnuje dodatek standardu RFC3629.
     * Predpoklada se ze obsluhuje jinak validni UTF8 cislo.
     * Maximalni codepoint je 0x10FFFF
     * @param utf8fourbyte 4B utf8 slovo ulozene do int
*/
bool isthisvalidutf8fourbytecode(uint32_t utf8fourbyte){
    vector<unsigned char> cs = int32toChar(utf8fourbyte);
    unsigned char c1=cs[0];
    unsigned char c2=cs[1];
    unsigned char c3=cs[2];
    unsigned char c4=cs[3];
    // 11110xxx
    c1=c1<<5;
    c1=c1>>5;
    // 10xxxxxx
    // Lze bezesporu resit forcyklem. Ciste z vlastniho presvedceni se utvrzuji v nazoru ze takto je to prehlednejsi. (pozn. red)
    // Prirazeni hodnoty, ktera je pouze v obsaditelnych bitech.
    c2=c2<<1;
    c2=c2>>1;
    c3=c3<<1;
    c3=c3>>1;
    c4=c4<<1;
    c4=c4>>1;
    // Codepoint je posloupnost bitu, ktere jsou skrze Byty obsaditelne.
    // Posun o 6 bitu odpovida 6 pozicim, ktere jsou v 1.-3.B volne.
    uint32_t codepoint=(((((((c1)<<6)+c2)<<6)+c3)<<6)+c4);
    if(codepoint<=0x10FFFF){
        return true;
    }else{
        return false;
    }
}

/**
    * Navraci Nty bit z Byte. Poradi je indexovane od MSB do LSB, pro lepsi praci se zpracovanim fronty bitu.
    * @param c Byte, ktery uchovava hodnotu
    * @param n offset v Byte
 */
 unsigned char getNthBit(unsigned char c,int n){
    c=c << n;
    c=c >> 7;
    return c;
}


class StreamReader {
    ifstream stream;
    // Hodnota prave cteneho Byte
    unsigned char byteVal = 0;
    // Indikator pozice nasledujiciho Byte v prave stenem streamu
    uint32_t g;
    // Kolikaty bit z Byte se cte
    int byteOffset = 0;
public:
    explicit StreamReader(const string &inputFile) {
        stream = ifstream(inputFile, std::ios::binary);
        byteVal = stream.get();
        g = stream.tellg();
    }

    void setByteOffset(int byteOffset) {
        this->byteOffset = byteOffset;
    }

    // Dostat nasledujici bit ve streamu
    // Po pouziti muze nastat EOF, ten volajici funkce musi zkontrolovat
    unsigned char getBit() {
        if (byteOffset == 8) {
            byteOffset = 0;
            byteVal = stream.get();
            g = stream.tellg();
        }
        unsigned char r = getNthBit(byteVal, byteOffset++);
        return r;
    }
    int getByteOffset() const {
        return byteOffset;
    }
    bool eof() {
        return stream.eof();
    }
    bool fail() {
        return stream.fail();
    }
    uint32_t tellg() const {
        return g;
    }
    ifstream &getStream() {
        return stream;
    }
};

 // Slouzi k testovani
/*void dumpStream(ifstream &ifstream1){
    cout<<endl;
    cout<<boolalpha<<"eof: "<<ifstream1.eof()<<endl;
    cout<<boolalpha<<"fail: "<<ifstream1.fail()<<endl;
    cout<<boolalpha<<"bad: "<<ifstream1.bad()<<endl;
}*/

class Node {
public:
    uint32_t utf8_character = 0;
    // Priznak zdali je Node prirazeno nejake pismeno (utf8_character)
    // Za behu se nestane, ze by se zmenilo na false.
    // utf8 = 0 je ridici kod NUL, ale pro stabilitu je informace drzena.
    bool assigned= false;

    void setUtf8Character(uint32_t utf8Character) {
        utf8_character = utf8Character;
        assigned= true;
    }

    Node *left = nullptr;
    Node *right = nullptr;

    string getCharacter() const{
        string r;
        for(unsigned char c: int32toChar(utf8_character)) {
            r.push_back((char) c);
        }
        return r;
    }
};

/**
 * Slouzi k vyhledavani zadaneho pismene ve stromu a vypsani cesty k nemu. Slouzi k testovani.
 * @param node korenovy vrchol
 * @param character hledane pismeno UTF8
 * @param path cesta, od korene
 */
void findInTree(Node& node, const string& character, const string& path){
        if(node.getCharacter()==character){
            cout<<path;
        }
        if(node.left!= nullptr) {
            string toSet = path+"0";
            findInTree(*node.left, character, toSet);
        }
        if(node.right!= nullptr){
            string toSet = path+"1";
            findInTree(*node.right,character,toSet);
        }
}

bool compressFile ( const char * inFileName, const char * outFileName ){return false;}

/**
 * Slouzi ke stavbe stromu pro dekodovani Huf.
 * @param node korenovy vrchnol
 * @param streamReader stream s ridicimi bitovymi sekvencemi a utf8 znaky
 * @param lastAcceptedCharacterBitPosition pozice posledniho uspesne precteneho znaku <offset v Byte, pozice ve streamu>
 * @param continueBuilding slouzi k preruseni stavby
 * @return pokud zdarne postaven, true. pokud nelze postavit nebo dojde k chybe pri cteni, false
 */
bool buildTreeRecursive(Node & node, StreamReader & streamReader,
                        pair<int,int>& lastAcceptedCharacterBitPosition,
                        bool & continueBuilding) {
    // Navratova hodnota, znaci jestli lze strom utvorit.
    // Pri chybe ve vstupu automaticky vraci false.
    bool r = true;
    cout << "jump / ";
    if (streamReader.getStream().fail()) {
        // Pokud je dalsi stream poruseny
        // prava vetev by dale nepokracovala, protoze nadrazeny vrchol ji v pripade false nepusti
        continueBuilding = false;
        return false;
    }
    if (streamReader.getStream().eof()) {
        // Pokud prijde rekurze do leve vetve a je eof, tak navraceni true zapricini projiti
        // prave vetve, ktera take navrati true a tvorba stromu tak skonci u posledniho uspesne
        // pridaneho pismene.
        continueBuilding = false;
        return true;
    }
    unsigned char c = streamReader.getBit();
    cout << (int) c << endl;

    // Bude se nacitat dalsi znak
    if (c == 1) {
        // UTF-8 znak
        // kolik pismen se zatim precetlo, pokud bude ve spravnem formatu, tak characters read je 8 az 32
        int charactersRead = 0;
        // podle prefixu, kolik se bude cist Byte z UTF8
        int utfbytes;
        // precist prvni Byte z pismene a urcit pocet Byte
        while (charactersRead != 8) {
            unsigned char nextchar = streamReader.getBit();
            // pokud se stane ze ctes posledni pismeno a vstup bude fail, tak return false, jinak pokud eof, tak vratit k poslednimu zapsanemu pismenu
            if (streamReader.getStream().eof()) {
                continueBuilding = false;
                return true;
            }
            if (streamReader.getStream().fail()) {
                continueBuilding = false;
                return false;
            }
            // pridat bit do existujiciho cisla (neni byte, utf8_character je int)
            node.setUtf8Character((node.utf8_character << 1) + nextchar);
            charactersRead++;
        }
        uint32_t firstBytePrefix = node.utf8_character;
        firstBytePrefix = firstBytePrefix >> 3;
        if (firstBytePrefix == 0b11110) {
            utfbytes = 4;
            cout << "4B ";
        } else if (firstBytePrefix >> 1 == 0b1110) {
            utfbytes = 3;
            cout << "3B ";
        } else if (firstBytePrefix >> 2 == 0b110) {
            utfbytes = 2;
            cout << "2B ";
        } else if (firstBytePrefix >> 4 == 0b0) {
            utfbytes = 1;
            cout << "1B ";
        } else {
            // Byte nezacina na zadny znamy prefix
            // UZAVRIT STROM, vyssi vrchol by pripadny navrat true vyhodnotil jako pokracovani tvorby stromu na vnejsim vrcholu.
            // stream se obnovi od bodu, kdy se pridalo posledni UTF8 znak. takze prectene nuly k tomuto vrcholu a posledni jednicka, uvozujici znak budou navraceny.
            continueBuilding = false;
            return true;
        }

        // Kolik bylo precteno znaku
        charactersRead = 0;
        // Podle prefixu urcit kolik se ma precist bitu
        while (charactersRead != ((utfbytes - 1) * 8)) {
            unsigned char nextchar = streamReader.getBit();
            if (streamReader.getStream().fail()) {
                continueBuilding = false;
                return false;
            }
            if (streamReader.getStream().eof()) {
                continueBuilding = false;
                return true;
            }

            // Nesplnen prefix sekundarnich Bytu UTF8.
            if ((charactersRead % 8 == 0 && nextchar != 1) ||
                (charactersRead % 8 == 1 && nextchar != 0)) {
                continueBuilding = false;
                return true;
            }

            // pridat bit do existujiciho cisla (neni byte, utf8_character je int)
            node.setUtf8Character((node.utf8_character << 1) + nextchar);
            charactersRead++;
        }
        if(utfbytes==4){
            if(!isthisvalidutf8fourbytecode(node.utf8_character)){
                return false;
            }
        }
        string str;
        str = node.getCharacter();
        cout << "save " << str << endl;
        // pokud se uspesne ulozi cely UTF znak, tak nastavit jako posledni pridany
        lastAcceptedCharacterBitPosition = pair<int, int>(streamReader.getByteOffset() - 1, streamReader.tellg());
    } else if (c == 0) {
        node.left = new Node();
        r = r && buildTreeRecursive(*node.left, streamReader, lastAcceptedCharacterBitPosition, continueBuilding);
        // Pokud se nestal fail a ani se nevraci stream na pozici, od ktere zacina dalsi cast programu
        if (r&&continueBuilding) {
            node.right = new Node();
            r = r &&
                buildTreeRecursive(*node.right, streamReader, lastAcceptedCharacterBitPosition, continueBuilding);
        } else {
            // Leva by byla corrupted stejne
            node.left = nullptr;
        }
    }
    return r;
}


bool decompressFile ( const char * inFileName, const char * outFileName ) {
    // Celkovy pocet prectenych pismen
    int charsRead =0;
    std::ofstream output(outFileName, std::ios::binary);
    if(output.fail()){
        return false;
    }

    Node root;
    bool continueBuilding = true;
    pair<int, int> lastBitPosition;
    StreamReader streamReader(inFileName);
    if(streamReader.eof()||streamReader.fail()){
        return false;
    }
    bool good = buildTreeRecursive(root, streamReader, lastBitPosition, continueBuilding);
    if (!good) {
        return false;
    }
    if(streamReader.getStream().eof()||streamReader.getStream().fail()){
        return false;
    }
    cout << "Konec cteni stromu v Byte: "<<hex << lastBitPosition.second <<dec<< " [" << lastBitPosition.first << "]" << endl;
    // Pocita se posledni pozice ze ktere se cetlo. Dale se cte o pozici dal.
    streamReader.setByteOffset(lastBitPosition.first+1);
    // Nastavit posledni precteny Byte
    streamReader.getStream().seekg(lastBitPosition.second);
    // Chunk s nastavitelnou delkou musi byt na konci
    bool readAdjustableChunk=false;
    // Pro kazdy chunk
    while(!streamReader.getStream().eof()) {
        // Nacist chunk
        int chunk = 0;
        int gotBit = streamReader.getBit();
        if (gotBit==1) {
            // Zacal dalsi Byte, ale predchozi chunky uz probehli. Neexistujici Byte = FF
            if (streamReader.eof()) {
                return true;
            }
            // Jiz byl precten chunk s nastavitelnou delkou
            if (readAdjustableChunk) {
                return false;
            }
            if (streamReader.fail()) {
                return false;
            }
            chunk = 0xFFF+1;
            cout<<chunk<<" CHUNK started at Byte "<<"["<<hex<< streamReader.tellg()<<"]";
        } else {

            // 12 bitu udava velikost
            uint32_t startedAtByte = streamReader.tellg();
            cout<<endl<<"CUSTOM CHUNK started at byte: "<<hex<< streamReader.getStream().tellg()<<"["<<streamReader.getByteOffset()-1<<"]"<<endl;
            // Pokud by koncilo presne tak lastReadByte by cetl stary lastReadByte
            uint32_t lastReadByte=streamReader.tellg();
            for (int i = 0; i < 12; i++) {
                unsigned char b = streamReader.getBit();
                if (streamReader.eof()) {
                    // Pokud se cetly same nuly, tak je uz konec vstupu
                    // Musi vystup musi ale skoncit hned za poslednimi platnymi bity
                    // rozdil tak musi byt jen jedna, protoze eof bude hned na nasledujicim
                    if (chunk == 0 &&
                        (startedAtByte == lastReadByte)) {
                        return true;
                    } else {
                        // Pokud se precetla nejaka jednicka a je s ni neco v neporadku, tak chyba
                        return false;
                    }
                }
                if (streamReader.fail()) {
                    return false;
                }
                chunk = (chunk << 1) + b;
                lastReadByte = streamReader.tellg();
            }
            // Nikdy se nebude cist custom chunk dvakrat
            //if(readAdjustableChunk){return false;}
            readAdjustableChunk = true;
            cout<<"- of letter length "<<dec<<(int)chunk<<endl;
        }
        string word;
        int characterCount =0;
        // Pro kazde pismeno v chunk
        while (chunk != 0) {
            // Promenne k testovani a vypisu
            // --
            string huf;
            Node ptr = root;
            // ----
            if(streamReader.eof()||streamReader.fail()){return false;}
            cout<<"["<<hex<<streamReader.getStream().tellg()<<"] ";
            while (true) {
                if (ptr.assigned) {
                    word+=ptr.getCharacter();
                    cout << ptr.getCharacter()<<"/ "<< huf <<" / "<<endl;
                    for (unsigned char c: int32toChar(ptr.utf8_character)) {
                        if (output.fail()) {
                            return false;
                        }
                        output.put((char) c);
                        charsRead++;
                    }
                    if(ptr.getCharacter()==" "){
                        cout<<word<<endl;
                        word="";
                    }
                    characterCount++;
                    break;
                }
                int branch = streamReader.getBit();
                if (branch == 0) {
                    huf+="0";
                    if (ptr.left == nullptr) {
                        return false;
                    } // Predpoklada se ze pokud ve strome nenajde slovo, tak spatne.
                    ptr = *ptr.left;
                } else {
                    huf+="1";
                    if (ptr.right == nullptr) {
                        return false;
                    }
                    ptr = *ptr.right;
                }
            }
            chunk--;
        }
        //cout<<"chunk "<<chunk<<endl;
        cout<<dec<<"END CHUNK read: ["<< characterCount<<"]" << endl;
    }
    streamReader.getStream().close();
    output.close();
    return true;
}

// Compare files
bool identicalFiles(const std::string& p1, const std::string& p2) {
    StreamReader s1(p1);
    StreamReader s2(p2);
    // Pouze k testovani. Neni ozkouseny
    while(!s1.eof()){
        if(s1.getBit()!=s2.getBit()){
            return false;
        }
    }
    if(!s2.eof()){return false;}
    return true;
}


#ifndef __PROGTEST__
int main() {
    assert(!isthisvalidutf8fourbytecode(0b11110100100100001000000010000000));
    assert(isthisvalidutf8fourbytecode(0b11110100100011111011111110111111));

    assert(decompressFile("testprogtest2.bin","tempfile"));
    assert(!decompressFile("testprogtest.bin","tempfile"));
    assert ( decompressFile ( "test0.huf", "tempfile" ) );
    assert ( identicalFiles ( "test0.orig", "tempfile" ) );

    assert ( decompressFile ( "test1.huf", "tempfile" ) );
    assert ( identicalFiles ( "test1.orig", "tempfile" ) );

    assert ( decompressFile ( "test2.huf", "tempfile" ) );
    assert ( identicalFiles ( "test2.orig", "tempfile" ) );

    assert ( decompressFile ( "test3.huf","tempfile" ));
    assert ( identicalFiles ( "test3.orig", "tempfile" ));

    assert ( decompressFile ( "test4.huf", "tempfile" ) );
    assert ( identicalFiles ( "test4.orig", "tempfile" ) );

    assert ( ! decompressFile ( "test5.huf", "tempfile" ) );


    assert ( decompressFile ( "extra0.huf", "tempfile" ) );
    assert ( identicalFiles ( "extra0.orig", "tempfile" ) );

    assert ( decompressFile ( "extra1.huf", "tempfile" ) );
    assert ( identicalFiles ( "extra1.orig", "tempfile" ) );

    assert ( decompressFile ( "extra2.huf", "tempfile" ) );
    assert ( identicalFiles ( "extra2.orig", "tempfile" ) );

    assert ( decompressFile ( "extra3.huf", "tempfile" ) );
    assert ( identicalFiles ( "extra3.orig", "tempfile" ) );

    assert ( decompressFile ( "extra4.huf", "tempfile" ) );
    assert ( identicalFiles ( "extra4.orig", "tempfile" ) );

    assert ( decompressFile ( "extra5.huf", "tempfile" ) );
    assert ( identicalFiles ( "extra5.orig", "tempfile" ) );

    assert ( decompressFile ( "extra6.huf", "tempfile" ) );
    assert ( identicalFiles ( "extra6.orig", "tempfile" ) );

    assert ( decompressFile ( "extra7.huf", "tempfile" ) );
    assert ( identicalFiles ( "extra7.orig", "tempfile" ) );

    assert ( decompressFile ( "extra8.huf", "tempfile" ) );
    assert ( identicalFiles ( "extra8.orig", "tempfile" ) );

    assert ( decompressFile ( "extra9.huf", "tempfile" ) );
    assert ( identicalFiles ( "extra9.orig", "tempfile" ) );

    assertv();
    return 0;
}

void assertv() {
    unsigned char assertChar = 0b01101010;
    assert(getNthBit(assertChar, 0) == 0);
    assert(getNthBit(assertChar, 1) == 1);
    assert(getNthBit(assertChar, 2) == 1);
    assert(getNthBit(assertChar, 3) == 0);
    assert(getNthBit(assertChar, 4) == 1);
    assert(getNthBit(assertChar, 5) == 0);
    assert(getNthBit(assertChar, 6) == 1);
    assert(getNthBit(assertChar, 7) == 0);

    Node assertUTFNode;
    assertUTFNode.utf8_character = 0b11110000100111111000100010000000;
    assert(assertUTFNode.getCharacter() == "🈀");
    assertUTFNode.utf8_character = 0b111000011010100010010011;
    assert(assertUTFNode.getCharacter() == "ᨓ");
    assertUTFNode.utf8_character = 0b1100010010000010;
    assert(assertUTFNode.getCharacter() == "Ă");
    assertUTFNode.utf8_character = 0xC599;
    assert(assertUTFNode.getCharacter() == "ř");
    assertUTFNode.utf8_character = 0b01010111;
    assert(assertUTFNode.getCharacter() == "W");
}

#endif /* __PROGTEST__ */