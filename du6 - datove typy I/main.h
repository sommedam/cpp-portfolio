#ifndef __PROGTEST__
#include <cstring>
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <cctype>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <stdexcept>
#include <algorithm>
#include <typeinfo>
#include <unordered_map>
#include <unordered_set>
#include <memory>
using namespace std;
#endif /* __PROGTEST__ */

enum TYPE{cINT,cDOUBLE,cENUM,cSTRUCT};
string TYPE_NAME(int index){
    switch (index) {
        case cINT:return "int";
        case cDOUBLE:return "double";
        case cENUM:return "enum";
        case cSTRUCT:return "struct";
        default:return "NOT VALID TYPE_NAME";
    }
}

class DataType {
protected:
    size_t byteSize;
    char type;

    virtual std::ostream &print(std::ostream &os) const {
        os << TYPE_NAME(type);
        return os;
    }
public:
    friend std::ostream &operator<<(std::ostream &os, const DataType &d){
        ostream &pf(d.print(os));
        return pf;
    };

    DataType(size_t size, char type) : byteSize(size), type(type) {}

    char getType() const {
        return type;
    }

    virtual size_t getSize() const {
        return byteSize;
    };

    virtual bool operator==(const DataType &rhs) const {
        return type == rhs.type;
    };

    bool operator!=(const DataType &rhs) const {
        return !(*this == rhs);
    };

    virtual ~DataType() = default;

    virtual DataType *clone() const = 0;
};

class CDataTypeEnum:public DataType
{
    set<string> keySet;
    vector<string> keyOrderSorted;
public:
    CDataTypeEnum() : DataType(4,cENUM) {}

    CDataTypeEnum& add(const char * key){
        string sKey = string (key);
        return add(sKey);
    }
    CDataTypeEnum& add(const string& key){
        auto itr = keySet.find(key);
        if(itr==keySet.end()){
            keySet.insert(key);
            keyOrderSorted.push_back(key);
            return *this;
        }else{
            throw invalid_argument("Duplicate enum value: "+key);
        }
    }

    bool operator==(const DataType &rhs) const override {
        if (rhs.getType() != getType()) {
            return false;
        } else {
            auto &rhsCasted = (CDataTypeEnum &) rhs;
            if (rhsCasted.keyOrderedSortedSize() == keyOrderSorted.size()) {
                for(long unsigned int i = 0;i<keyOrderSorted.size();i++)
                    if(keyOrderSorted.at(i)!=rhsCasted.keyAtPos(i)){
                        return false;
                    }
                return true;
            } else {
                return false;
            }
        }
    }

    ~CDataTypeEnum() override = default;
protected:
    DataType* clone() const override
    {
        auto* out = new CDataTypeEnum(*this);
        return out;
    }

    ostream &print(ostream &os) const override {
        os<<TYPE_NAME(type)<<endl<<"{"<<endl;
        for(long unsigned int i=0; i<keyOrderSorted.size();i++){
            os<<"  "<<keyOrderSorted.at(i);
            if(i!=keyOrderSorted.size()-1){os<<",";}
            os<<endl;
        }
        os<<"}";
        return os;
    }

protected:
    size_t keyOrderedSortedSize()const{
        return keyOrderSorted.size();
    }
    string keyAtPos(size_t pos)const{
        return keyOrderSorted.at(pos);
    }
};

class CDataTypeStruct:public DataType {
    // Mnozina vsech nazvu promennych
    set<string> *names;
    // Vektor vsech clenu structu. Serazeno podle posloupnosti, v jakem byli pridany.
    vector<pair<string, DataType *> *> *valuesVector;

public:
    // Defaultni konstruktor
    CDataTypeStruct() : DataType(0, cSTRUCT) {
        names = new set<string>;
        valuesVector = new vector<pair<string, DataType *> *>;
    }

    // Kopirujici konstruktor - ma vyssi rezii. Vse se kopiruje jako nove
    CDataTypeStruct(const CDataTypeStruct &p) : DataType(0, cSTRUCT) {
        names = new set<string>;
        valuesVector = new vector<pair<string, DataType *> *>;
        // Prekopirovat kazdy nazev promenny
        for (const auto &name: *p.names) {
            names->insert(name);
        }

        // Pro kazdy clen zkopirovat nazev a naklonovat datovy typ
        for (auto &value: *p.valuesVector) {
            valuesVector->push_back(new pair<string, DataType *>(value->first, value->second->clone()));
        }
    }

    // Presunujici konstruktor - ma nizsi rezii
    CDataTypeStruct(CDataTypeStruct &&src) noexcept: DataType(0, cSTRUCT) {
        names = src.names;
        valuesVector = src.valuesVector;
        src.names = nullptr; // takto se uz ve zdroji neuvolni
        src.valuesVector = nullptr; // takto se uz ve zdroji neuvolni
    }

    size_t getSize() const override {
        size_t aggr = 0;
        for (auto p: *valuesVector) {
            aggr += p->second->getSize();
        }
        return aggr;
    }

    ~CDataTypeStruct() override {
        // Uvolnit mnozinu nazvu.
        delete names;
        for (auto &p: *valuesVector) {
            // Uvolnit datovy typ
            delete p->second;
            // Uvolnit clen structu
            delete p;
        }
        delete valuesVector;
    }

    CDataTypeStruct &addField(const char *name, const DataType &dt) {
        string sName = string(name);
        return addField(sName, dt);
    }

    CDataTypeStruct &addField(const string &name, const DataType &dt) {
        if (names->find(name) == names->end()) {
            valuesVector->push_back(new pair<string, DataType *>(name, dt.clone()));
            names->insert(name);
            return *this;
        } else {
            throw invalid_argument("Duplicate field: " + name);
        }
    }

    CDataTypeStruct &addField(const char *name, DataType &&dt) {
        string sName = string(name);
        return addField(sName, dt);
    }

    CDataTypeStruct &addField(const string &name, DataType &&dt) {
        if (names->find(name) == names->end()) {
            valuesVector->push_back(new pair<string, DataType *>(name, dt.clone()));
            names->insert(name);
            return *this;
        } else {
            throw invalid_argument("Duplicate field: " + name);
        }
    }

    const DataType &field(const char *name)const {
        const string& sName = string(name);
        return field(sName);
    }

    const DataType &field(const string& name)const {
        for (const auto &value: *valuesVector) {
            if (value->first == name) {
                return *(value->second);
            }
        }
        throw invalid_argument("Unknown field: " + name);
    }

    bool operator==(const DataType &rhs) const override {
        // Neni to stejny typ
        if (rhs.getType() != cSTRUCT) {
            return false;
        }
        // Dynamic cast
        const CDataTypeStruct &rhsCast = (CDataTypeStruct &&) rhs;
        if (getNumberOfValues() != rhsCast.getNumberOfValues()) {
            return false;
        }
        for (long unsigned int i = 0; i < valuesVector->size(); i++) {
            // Zdali se rovna kazda slozka na kazde pozici
            // Muze byt rekurzivni, ale nemel by to byt problem
            // Todo operator* mozna zahodi virtualni cast
            auto thisTypeAtPos = valuesVector->at(i)->second;
            auto rhsTypeAtPos = rhsCast.valueAtPosition(i);
            if (*thisTypeAtPos != *rhsTypeAtPos) {
                return false;
            }
        }
        return true;
    }


private:
    size_t getNumberOfValues() const {
        return valuesVector->size();
    }

    DataType *valueAtPosition(size_t pos) const {
        return valuesVector->at(pos)->second;
    }

protected:
    DataType *clone() const override {
        auto *out = new CDataTypeStruct(*this);
        return out;
    }

    ostream &print(ostream &os) const override {
        os << "struct" << endl;
        os << "{" << endl;
        for (const auto &p: *valuesVector) {
            os << "  " << *(p->second) << " ";
            os << p->first << ";" << endl;
        }
        os << "}";
        return os;
    }
};

class CDataTypeInt:public DataType
{
public:
    CDataTypeInt() : DataType(4,cINT) {}

    bool operator==(const DataType &rhs) const override {
        // Pro pohodlnejsi debugovani rozepsano
        if(rhs.getType()!=cINT){
            return false;
        }else{
            return true;
        }
    }
protected:
    DataType *clone() const override {
        auto *out = new CDataTypeInt(*this);
        return out;
    }
};

class CDataTypeDouble : public DataType
{
public:
    CDataTypeDouble() : DataType(8, cDOUBLE) {}
protected:
    DataType *clone() const override {
        auto *out = new CDataTypeDouble(*this);
        return out;
    }
};

#ifndef __PROGTEST__
static bool        whitespaceMatch                         ( const string    & a,
                                                             const string    & b )
{
    // todo
    return true;
}
template <typename T_>
static bool        whitespaceMatch                         ( const T_        & x,
                                                             const string    & ref )
{
    ostringstream oss;
    oss << x;
    return whitespaceMatch ( oss . str (), ref );
}
int main ( void ){
    CDataTypeStruct  a = CDataTypeStruct () .
            addField ( "m_Length", CDataTypeInt () ) .
            addField ( "m_Status", CDataTypeEnum () .
            add ( "NEW" ) .
            add ( "FIXED" ) .
            add ( "BROKEN" ) .
            add ( "DEAD" ) ).
            addField ( "m_Ratio", CDataTypeDouble () );

    CDataTypeStruct b = CDataTypeStruct () .
            addField ( "m_Length", CDataTypeInt () ) .
            addField ( "m_Status", CDataTypeEnum () .
            add ( "NEW" ) .
            add ( "FIXED" ) .
            add ( "BROKEN" ) .
            add ( "READY" ) ).
            addField ( "m_Ratio", CDataTypeDouble () );

    CDataTypeStruct c =  CDataTypeStruct () .
            addField ( "m_First", CDataTypeInt () ) .
            addField ( "m_Second", CDataTypeEnum () .
            add ( "NEW" ) .
            add ( "FIXED" ) .
            add ( "BROKEN" ) .
            add ( "DEAD" ) ).
            addField ( "m_Third", CDataTypeDouble () );

    CDataTypeStruct  d = CDataTypeStruct () .
            addField ( "m_Length", CDataTypeInt () ) .
            addField ( "m_Status", CDataTypeEnum () .
            add ( "NEW" ) .
            add ( "FIXED" ) .
            add ( "BROKEN" ) .
            add ( "DEAD" ) ).
            addField ( "m_Ratio", CDataTypeInt () );

    assert(CDataTypeInt()==CDataTypeInt());
    assert ( a != b );
    assert ( a == c );
    assert ( a != d );
    assert ( a . field ( "m_Status" ) == CDataTypeEnum () . add ( "NEW" ) . add ( "FIXED" ) . add ( "BROKEN" ) . add ( "DEAD" ) );
    assert ( a . field ( "m_Status" ) != CDataTypeEnum () . add ( "NEW" ) . add ( "BROKEN" ) . add ( "FIXED" ) . add ( "DEAD" ) );
    assert ( a != CDataTypeInt() );

    CDataTypeStruct aOld = a;
    b . addField ( "m_Other", CDataTypeDouble ());
    a . addField ( "m_Sum", CDataTypeInt ());

    assert ( a != aOld );
    assert ( a != c );
    assert ( aOld == c );


    c . addField ( "m_Another", a . field ( "m_Status" ));
    d . addField ( "m_Another", a . field ( "m_Ratio" ));

    assert(a.getSize()==20);
    assert(b.getSize()==24);

    try
    {
        a . addField ( "m_Status", CDataTypeInt () );
        assert ( "addField: missing exception!" == nullptr );
    }
    catch ( const invalid_argument & e )
    {
        assert ( e . what () == "Duplicate field: m_Status"sv );
    }

    try
    {
        cout << a . field ( "m_Fail" ) << endl;
        assert ( "field: missing exception!" == nullptr );
    }
    catch ( const invalid_argument & e )
    {
        assert ( e . what () == "Unknown field: m_Fail"sv );
    }

    try
    {
        CDataTypeEnum en;
        en . add ( "FIRST" ) .
                add ( "SECOND" ) .
                add ( "FIRST" );
        assert ( "add: missing exception!" == nullptr );
    }
    catch ( const invalid_argument & e )
    {
        assert ( e . what () == "Duplicate enum value: FIRST"sv );
    }

    return EXIT_SUCCESS;
}
#endif /* __PROGTEST__ */