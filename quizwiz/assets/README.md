<h2>Vitejte v dokumentaci Quiz Wizard, kvízovém prohlížeči 🧙</h2>

Aplikace Quiz Wizard slouží pro tvorbu kvízů a jejich zodpovídání.

Pokyny pro ovládání jsou umístěné na spodní straně uživatelského okna konzole. Začněte třeba zmáčknutím klávesy 'z' pro tvorbu nového kvízu.</br>

<b>V adresáři spustitelného souboru musí být umístěna složka <i>saved_quizwiz</i>, tak jako je ve složce examples.</b>
<b>V rámci celé aplikace je zakázáno používat jiné než ASCII znaky.</b>

<i>Pro lepší zážitek z aplikace je v examples původní verze aplikace, před odstraněním nepovolené knihovny std::filesystem. Ve verzi, která je zkompilovatelná z dodaného zdrojového kódu není možnost jak zobrazit existující kvízy, proto je třeba na obrazovce výběru kvízu zadat přesný název kvízu, pod kterým byl uložen v tvůrci kvízů. </i>


