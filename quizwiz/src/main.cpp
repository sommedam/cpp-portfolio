#include <iostream>
#include <sys/stat.h>
#include "_tests/layout/LayoutTest.h"
#include "_tests/layout/drawable_holder/DrawableHolderTest.h"
#include "_tests/layout/drawable_holder/DrawableHolderPageableTest.h"
#include "_tests/quiz_containers/PageTest.h"
#include "_tests/answers/TextSetAnswerTest.h"
#include "_tests/answers/MultichoiceAnswerTest.h"
#include "_tests/drawables/TextDrawableTest.h"
#include "_tests/answers/TextAnswerTest.h"
#include "_tests/quiz_containers/PageIteratorTest.h"
#include "gui/navigation/fragment/project_fragments/MainMenuFragment.h"
#include "gui/navigation/fragment/project_fragments/CreatorFragment.h"
#include "gui/navigation/fragment/project_fragments/ChoosingQuizFragment.h"
#include "gui/navigation/fragment/FragmentHandler.h"
#include "_tests/answers/NumberIntervalAnswerTest.h"
#include "_tests/answers/TextMisspelledAnswerTest.h"

using namespace std;
struct stat info;
void manualTests();
void autoTests();

int main() {
    autoTests();
    //manualTests();

    // check if save folder exists
    if (stat("saved_quizwiz", &info) != 0) {
        DisplayConsole::displayMessage("Chyby slozka kvizu. Zalozte v korenovem adresari slozku 'saved_quizwiz'.");
        this_thread::sleep_for(chrono::seconds(5));
        return 0;
    }



    auto mainMenuFragment = make_shared<MainMenuFragment>();
    auto creatorFragment = make_shared<CreatorFragment>();
    auto choosingQuizFragment = make_shared<ChoosingQuizFragment>();
    auto quizFragment = make_shared<QuizFragment>();
    vector<shared_ptr<Fragment>> fragment{mainMenuFragment,creatorFragment,choosingQuizFragment,quizFragment};
    FragmentHandler fh(mainMenuFragment->NAME(),fragment);
    return 0;
}

void autoTests(){
    LayoutTest::autoTest();
    DrawableHolderTest::autoTest();
    DrawableHolderPageableTest::autoTest();
    PageTest::autoTest();
    PageIteratorTest::autoTest();
    NumberIntervalAnswerTest::autoTest();
    TextSetAnswerTest::autoTest();
    MultichoiceAnswerTest::autoTest();
    TextAnswerTest::autoTest();
    TextDrawableTest::autoTest();
    TextMisspelledAnswerTest::autoTest();

    cout<<"All auto tests passed"<<endl;
}

void manualTests(){
    //  DrawableHolderPageableTest::manualTest();
    //  TextMisspelledAnswerTest::manualTest();
    //  FragmentHandlerTest::manualTest();
    //  BitmapTest::manualTest();
    //  ScrollableLayoutTest::manualTest();
    //  PageableLayoutTest::manualTest();
    //  TextAnswerQuestionLayoutTest::manualTest();
    //  ChoiceAnswerQuestionLayoutTest::manualTest();
    //  NumberIntervalAnswerTest::manualTest();
    //  TextSetAnswerTest::manualTest();
    //  MultichoiceAnswerTest::manualTest();
}
