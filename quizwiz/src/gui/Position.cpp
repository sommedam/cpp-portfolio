//
// Created by voky on 28.03.22.
//

#include "Position.h"

int Position::getX() const {
    return x;
}

int Position::getY() const {
    return y;
}

bool Position::operator==(Position &second) const {
    if(x == second.getX() &&
       y == second.getY()){
        return true;
    }else{
        return false;
    }
}

Position Position::operator+(Position &b) const {
    return {x+b.getX(),y+b.getY()};
}
