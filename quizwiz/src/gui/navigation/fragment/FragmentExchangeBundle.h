//
// Created by Frix on 04.08.2021.
//

#ifndef SEMESTRALKA_EXCHANGEBUNDLE_H
#define SEMESTRALKA_EXCHANGEBUNDLE_H

#include <string>
#include <map>

using namespace std;

/**
 * Class for delivering extra payload between fragments.
 */
class FragmentExchangeBundle {
    map<string, string> mBundle;

public:
    FragmentExchangeBundle()= default;

    explicit FragmentExchangeBundle(const map<string, string> &mBundle) : mBundle(mBundle) {}

    [[nodiscard]] const map<string, string> &getBundle() const;

    string dummyString;

    /**
     * Returns element of given name
     * @attention methond does not throw exception, when element not found, it will return empty string instead.
     * @param elementName  name of element to return
     * @return string of payload in unspecified format
     */
    [[nodiscard]]const string &getBundleElement(const string& elementName)const;

    void setBundleElement(string  elementName, string  content);
};

#endif //SEMESTRALKA_EXCHANGEBUNDLE_H
