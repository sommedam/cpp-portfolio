//
// Created by Frix on 02.08.2021.
//

#ifndef SEMESTRALKA_FRAGMENTHANDLER_H
#define SEMESTRALKA_FRAGMENTHANDLER_H

#include <string>
#include <functional>
#include <memory>
#include <map>
#include <iostream>
#include <chrono>
#include <thread>
#include <termios.h>
#include <unistd.h>
#include "Fragment.h"
#include "NoFragmentOfNameException.h"

using namespace std;
/*
 * Class responsible for fragment switching and maintaining
 * Responsible for keybinds exectution outside of fragments. ex. exit program
 */
class FragmentHandler: public FragmentListener{
    map<string, shared_ptr<Fragment>> mFragments; /* map of all application Fragments, indexed by their NAME */
    shared_ptr<Fragment> currentFragment; /* fragment displaying to user at the moment. */
    map<unsigned char, function<void()>> mKeyMap; /* map of keys, bind to their set functionality */
    bool runInputLoop = false; /* if keyboard listening is active */
    
public:
    /**
     * @param defaultFragment name of default fragment
     * @param fragments reference to all fragments in application
     * @param defaultFragmentExchangeBundle payload to default fragment. typicaly main menu fragment
     */
    FragmentHandler(const std::string& defaultFragment,vector<shared_ptr<Fragment>>&fragments,const FragmentExchangeBundle& defaultFragmentExchangeBundle = FragmentExchangeBundle());


    /**
     * Service of keybind listening. Keybind is set in onResume and onCreate phase of Fragment.
     */
    void startInputLoop();
    void endInputLoop();

    /**
     * Change of keyMap
     * @param keyMap map of keybinds and their listeners
     */
    void onKeyMapChange(map<unsigned char, function<void()>>& keyMap) override;

    /**
     * Switch active fragment to another.
     * Active Fragment at the moment will stop refreshing console and the right will be passed to next Fragment
     * @param fragmentName name of next Fragment
     * @param exchangeBundle extra payload for nextFragment
     */
    void onFragmentChange(std::string& fragmentName, const FragmentExchangeBundle&exchangeBundle) override;

    set<string> alreadyCreatedFragments;

    [[nodiscard]] shared_ptr<Fragment> getCurrentFragment() const;

    void overrideKeyListening() override;

    void stopOverrideKeyListening() override;
};



#endif //SEMESTRALKA_FRAGMENTHANDLER_H
