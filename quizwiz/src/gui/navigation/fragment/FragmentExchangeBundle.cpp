//
// Created by voky on 31.5.22.
//

#include "FragmentExchangeBundle.h"

const map<string, string> &FragmentExchangeBundle::getBundle() const {
    return mBundle;
}

void FragmentExchangeBundle::setBundleElement(string elementName, string content) {
    mBundle[move(elementName)]=move(content);
}

const string &FragmentExchangeBundle::getBundleElement(const string &elementName) const {
    auto r = mBundle.find(elementName);
    if(!mBundle.empty()&& r!=mBundle.end()){
        return r->second;
    }else{
        return dummyString;
    }
}
