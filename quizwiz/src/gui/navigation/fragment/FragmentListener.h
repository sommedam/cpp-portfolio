//
// Created by Frix on 02.08.2021.
//

#ifndef SEMESTRALKA_FRAGMENTLISTENER_H
#define SEMESTRALKA_FRAGMENTLISTENER_H


#include <map>
#include <functional>
#include <memory>
#include "FragmentExchangeBundle.h"

/**
 * Interface between @link FragmentHandler.h and @link Fragment.h
 */
class FragmentListener {

public:
    virtual void overrideKeyListening()=0;
    virtual void stopOverrideKeyListening()=0;
    virtual void onKeyMapChange(std::map<unsigned char, std::function<void()>>&keyMap)=0;
    virtual void onFragmentChange(std::string& fragmentName,const FragmentExchangeBundle & exchangeBundle)=0;
};


#endif //SEMESTRALKA_FRAGMENTLISTENER_H
