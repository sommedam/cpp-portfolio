//
// Created by voky on 19.5.22.
//

#include "FragmentHandler.h"

FragmentHandler::FragmentHandler(const string &defaultFragment, vector<shared_ptr<Fragment>> &fragments,
                                 const FragmentExchangeBundle &defaultFragmentExchangeBundle) {
    for(const auto& fragment:fragments){
        mFragments.insert(pair<string,shared_ptr<Fragment>>(fragment->NAME(),fragment));
    }

    string mDef = defaultFragment; /* switch to default fragment */
    FragmentHandler::onFragmentChange(mDef,defaultFragmentExchangeBundle);
    startInputLoop(); /* start keyboard listening */
}

void FragmentHandler::startInputLoop() {
    runInputLoop = true;
    while(runInputLoop)
    {
        string input;
        unsigned char ch; //= cin.get();
        std::getline(std::cin, input);
        ch= input[0];
        // reserved for testing
        if(ch=='k') {
            runInputLoop=false;
            cout<<"pressed 'k', ultimate application fatality";
        }else{
            // if mapped, execute
            if(!mKeyMap.empty()){
                auto foundRef = mKeyMap.find(ch);
                if(foundRef!=mKeyMap.end()&&runInputLoop){
                    (foundRef->second());
                }
            }
        }
        if(runInputLoop) {
            // STACKOVERFLOW
            using namespace this_thread; // sleep_for, sleep_until
            using namespace chrono; // nanoseconds, system_clock, seconds
            sleep_for(nanoseconds(10));
            // KONEC STACKOVERFLOW
        }else{
            // End of program.
        }
    }
}

void FragmentHandler::endInputLoop() {
    runInputLoop= false;
}

void FragmentHandler::onKeyMapChange(map<unsigned char, function<void()>> &keyMap) {
    FragmentHandler::mKeyMap = keyMap;
}

void FragmentHandler::onFragmentChange(string &fragmentName, const FragmentExchangeBundle &exchangeBundle) {
    auto foundRef = mFragments.find(fragmentName);
    if(foundRef==mFragments.end()) {
        throw NoFragmentOfNameException();
    } else{
        if(currentFragment){currentFragment->onPause();}

        currentFragment=foundRef->second;
        if(alreadyCreatedFragments.find((*foundRef).first)==alreadyCreatedFragments.end()) {
            currentFragment->onCreate(this, exchangeBundle);
            alreadyCreatedFragments.insert(foundRef->first);
        }else{
            currentFragment->onResume(exchangeBundle);
        }
    }
}

shared_ptr<Fragment> FragmentHandler::getCurrentFragment() const {
    return currentFragment;
}

void FragmentHandler::overrideKeyListening() {
    runInputLoop= false;
}

void FragmentHandler::stopOverrideKeyListening() {
    startInputLoop();
}
