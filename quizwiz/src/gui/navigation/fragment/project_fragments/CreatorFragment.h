//
// Created by voky on 12.04.22.
//

#ifndef QUIZWIZ_CREATORFRAGMENT_H
#define QUIZWIZ_CREATORFRAGMENT_H


#include <fstream>
#include "../Fragment.h"
#include "MainMenuFragment.h"
#include "../../../graphical/drawable/layout/Layout.h"
#include "../../../graphical/drawable/project_drawables/BottomKeysDrawable.h"
#include "../../../../quiz_containers/page/Page.h"
#include "../../../graphical/drawable/layout/ScrollableLayout.h"
#include "../../../../quiz_containers/answer/AnswerFactory.h"
#include "../../../../quiz_containers/page/page_policy/PagePolicyFactory.h"
#include "../../../../quiz_containers/Quiz.h"

/**
 * Fragment for quiz creating
 */
class CreatorFragment: public Fragment {
    AnswerFactory answerFactory;
    shared_ptr<BottomKeysDrawable> bottom;

    shared_ptr<ScrollableLayout> scrollableLayout;
    shared_ptr<Layout> outterLayout;

    shared_ptr<Page> currentPage;
    PagePolicyFactory pagePolicyFactory;
    FragmentListener *fragmentListenerPtr = nullptr;
    Quiz quiz;
    int numberOfAddedLines = 0;
    set<string> yes = {"ano", "a", "A", "ano", "Ano", "Y", "y", "yes", "Yes"}; /* user input accepted as true */
    //set<string> no = {"nikoliv", "nikolivek", "n", "N", "ne", "Ne", "No", "no"}; /* user input accepted as false */
    string name = "CreatorFragment";

private:
    /**
     * Tests if file exists
     * @param filename name of file to test
     * @return true if exists
     */
    static inline bool doFileExists(const std::string &filename);

    /**
     * Add next page to quiz
     */
    void addPage();

    /**
     * Add next question to quiz
     */
    void addQuestion();

    /**
     * Prida dalsi text do skrolovaciho layoutu, automaticky layout posune, pokud by informace presahovaly okno terminalu
     * @param innerText - text, ktery se ma vlozit
     */
    void addLineToScroll(const string &innerText);

    void changeKeyMapDefault();

    void saveQuiz();

    void goToMainMenuFragment();

    void badInputQuestionIndex();

    void setStartKeyMap();
public:
    CreatorFragment() = default;

    void onCreate(FragmentListener *fragmentHandler, const FragmentExchangeBundle &bundle) override;

    void onResume(const FragmentExchangeBundle &exchangeBundle) override;

    string NAME() override;
};


#endif //QUIZWIZ_CREATORFRAGMENT_H
