//
// Created by voky on 23.03.22.
//

#ifndef QUIZWIZ_QUIZFRAGMENT_H
#define QUIZWIZ_QUIZFRAGMENT_H

#include "../Fragment.h"
#include "../../../../quiz_containers/Quiz.h"
#include "../../../graphical/drawable/layout/PageableLayout.h"
#include "../../../graphical/drawable/layout/ScrollableLayout.h"
#include "../../../graphical/drawable/project_drawables/BottomKeysDrawable.h"
#include "../../../graphical/drawable/TextDrawable.h"

using namespace std;

/**
 * Fragment with quiz solving
 */
class QuizFragment: public Fragment {
    Quiz quiz;
    shared_ptr<PageableLayout> pageableLayout;
    shared_ptr<ScrollableLayout> scrollableLayout;

    shared_ptr<Layout> outterLayout;
    vector<shared_ptr<Drawable>>::iterator beginOfCurrentPageQuizLayoutsItr;
    vector<shared_ptr<Drawable>>::iterator currentPageQuizLayoutsItr;
    vector<shared_ptr<Drawable>>::iterator endOfCurrentPageQuizLayoutsItr;
    PageIterator pageIterator;
    int focusedOffsetY = 3;

    PagePolicyBuilder pagePolicyBuilder;

    shared_ptr<BottomKeysDrawable> bottomKeysDrawable;

    shared_ptr<TextDrawable> quizPageHeadlineText;
    shared_ptr<TextDrawable> focusedPointerText;
public:
    explicit QuizFragment();

    void onCreate(FragmentListener *fragmentHandler, const FragmentExchangeBundle &bundle) override;

    void onResume(const FragmentExchangeBundle &bundle) override;

    string NAME() override;

private:
    void setDefaultKeyMap();

    void setResultKeyMap();

    void scrollResultDown();

    void scrollResultUp();

    void goToMainMenuFragment() const;

    FragmentListener *fragmentListenerPtr{};

    void setQuizFromBundle(const FragmentExchangeBundle &bundle);

    void initAllPages();

    void interactFocused();

    void nextPage();

    void generateResultPage(const string &pageName);

    void focusNext();

    void focusPrevious();

};


#endif //QUIZWIZ_QUIZFRAGMENT_H
