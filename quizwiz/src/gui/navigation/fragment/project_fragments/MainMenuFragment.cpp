//
// Created by voky on 23.03.22.
//

#include "MainMenuFragment.h"
#include "../../../graphical/drawable/TextDrawable.h"

void MainMenuFragment::onCreate(FragmentListener *fragmentListener, const FragmentExchangeBundle &bundle) {
    fragmentListenerPtr=fragmentListener;
    initUi();
    setDefaultKeyMap();

    vector<string> lines={" ####   #    #     #    ######  #    #     #    ######",
                          "#    #  #    #     #        #   #    #     #        #",
                          "#    #  #    #     #       #    #    #     #       #",
                          "#  # #  #    #     #      #     # ## #     #      #",
                          "#   #   #    #     #     #      ##  ##     #     #",
                          " ### #   ####      #    ######  #    #     #    ######"};
    for (size_t i = 0; i < lines.size(); i++) {
        auto l = make_shared<TextDrawable>("deco_" + to_string(i), Position(2, i),
                                           [this]() { drawableRefreshCallback(); });
        auto d = dynamic_pointer_cast<Drawable>(l);
        baseLayout->addDrawable(d);
        l->setText(lines[i]);
    }
}

void MainMenuFragment::setDefaultKeyMap() {
    map<unsigned char, function<void()>> keyMap;
    keyMap['a']=([this]() {
        string fragmentName = "ChoosingQuizFragment";
        fragmentListenerPtr->onFragmentChange(fragmentName, FragmentExchangeBundle());
    });
    keyMap['z']=([this]() {
        string fragmentName = "CreatorFragment";
        fragmentListenerPtr->onFragmentChange(fragmentName, FragmentExchangeBundle());
    });
    fragmentListenerPtr->onKeyMapChange(keyMap);
}

string MainMenuFragment::NAME() {
    return "MainMenuFragment";
}

void MainMenuFragment::onResume(const FragmentExchangeBundle &bundle) {
    setDefaultKeyMap();
    drawableRefreshCallback();
}

void MainMenuFragment::initUi() {
    const function<void()> observer = [this](){
        drawableRefreshCallback();
    };
    baseLayout= make_shared<Layout>(NAME(),Position(0,0),observer);
    auto quizOption = make_shared<TextDrawable>("quiz",Position(4,8),observer);
    string quizText = "[a] Prejit do kvizu";
    quizOption->setText(quizText);

    auto quizCreatorOption = make_shared<TextDrawable>("creator",Position(4,10),observer);
    string creatorText = "[z] Tvorba kvizu";
    quizCreatorOption->setText(creatorText);

    auto versionText = make_shared<TextDrawable>("version",Position(79,DisplayConsole::bottomPosition()),observer);
    string versionTextInner = "quiz wizard v1.0";
    versionText->setText(versionTextInner);

    shared_ptr<Drawable> d1 = std::static_pointer_cast<Drawable>(quizOption);
    shared_ptr<Drawable> d2 = std::static_pointer_cast<Drawable>(quizCreatorOption);
    shared_ptr<Drawable> d3 = std::static_pointer_cast<Drawable>(versionText);


    baseLayout->addDrawable(d1);
    baseLayout->addDrawable(d2);
    baseLayout->addDrawable(d3);
}
