//
// Created by voky on 23.03.22.
//

#include "QuizFragment.h"
#include "../../../graphical/drawable/layout/project_layouts/QuestionLayout.h"
#include "MainMenuFragment.h"
#include "../../../../quiz_containers/import_export/WizardInputOutput.h"
#include "../../../../quiz_containers/answer/exceptions/AnswerUserInputException.h"

QuizFragment::QuizFragment() {
    pagePolicyBuilder.MessageFlagListener([this](const string& message){
        DisplayConsole::displayMessage(message);
        std::this_thread::sleep_for(std::chrono::seconds (3));
        goToMainMenuFragment();
    });
    pagePolicyBuilder.BlockFlagListener([this](int i){
        DisplayConsole::displayMessage("Vsechny otazky nejsou zodpovezeny spravne.");
        std::this_thread::sleep_for(std::chrono::seconds (2));
        drawableRefreshCallback();
    });

    outterLayout = make_shared<Layout>("outter", Position(4, 3), [this]() {
        drawableRefreshCallback();
    });
    scrollableLayout = make_shared<ScrollableLayout>("scrollable",Position(0,0),[this](){
        drawableRefreshCallback();
    });
    pageableLayout = make_shared<PageableLayout>("pageable", Position(0, 0), [this]() {
        drawableRefreshCallback();
    });
    outterLayout->setMaxHeight(13);
    auto lpageable = dynamic_pointer_cast<Layout>(pageableLayout);
    auto lscrollable = dynamic_pointer_cast<Layout>(scrollableLayout);

    scrollableLayout->addInnerLayout(lpageable);
    outterLayout->addInnerLayout(lscrollable);

    Fragment::baseLayout = make_shared<Layout>(QuizFragment::NAME(), Position(0, 0), [this]() {
        drawableRefreshCallback();
    });
    Fragment::baseLayout->addInnerLayout(outterLayout);

    quizPageHeadlineText = make_shared<TextDrawable>("page_name",Position(3,1),[this](){drawableRefreshCallback();});
    auto d = dynamic_pointer_cast<Drawable>(quizPageHeadlineText);
    baseLayout->addDrawable(d);

    focusedPointerText = make_shared<TextDrawable>("pointer",Position(2,4),[this](){drawableRefreshCallback();});
    auto focusedPointerDrawable = dynamic_pointer_cast<Drawable>(focusedPointerText);

    baseLayout->addDrawable(focusedPointerDrawable);

    bottomKeysDrawable = make_shared<BottomKeysDrawable>("bottom_keys",Position(0,DisplayConsole::bottomPosition()),[this](){drawableRefreshCallback();});
    auto d4 = dynamic_pointer_cast<Drawable>(bottomKeysDrawable);
    baseLayout->addDrawable(d4);
}

void QuizFragment::setDefaultKeyMap() {
    map<unsigned char, function<void()>> keyMap;
    keyMap['f']=[this](){interactFocused();};
    keyMap['w']=[this](){focusPrevious();};
    keyMap['s']=[this](){focusNext();};
    keyMap['n']=[this](){nextPage();};
    keyMap['x']=[this](){goToMainMenuFragment();};
    bottomKeysDrawable->removeAllKeys();
    bottomKeysDrawable->addKey('f',"Odpovedet");
    bottomKeysDrawable->addKey('w',"Predchozi");
    bottomKeysDrawable->addKey('s',"Nasledujici");
    bottomKeysDrawable->addKey('n',"Dalsi strana");
    bottomKeysDrawable->addKey('x',"Hl.menu");
    fragmentListenerPtr->onKeyMapChange(keyMap);
}

void QuizFragment::setResultKeyMap() {
    bottomKeysDrawable->removeAllKeys();
    map<unsigned char, function<void()>> keyMap;
    keyMap['w']=[this](){scrollResultUp();};
    keyMap['s']=[this](){scrollResultDown();};
    keyMap['x']=[this](){goToMainMenuFragment();};
    bottomKeysDrawable->addKey('w',"Vzhuru");
    bottomKeysDrawable->addKey('s',"Dolu");
    bottomKeysDrawable->addKey('x',"Hl.menu");
    fragmentListenerPtr->onKeyMapChange(keyMap);
}

void QuizFragment::scrollResultDown() {
    scrollableLayout->setScroll(scrollableLayout->getScrollOffset()+3);
}

void QuizFragment::scrollResultUp() {
    scrollableLayout->setScroll(max(scrollableLayout->getScrollOffset()-3,0));
}

void QuizFragment::goToMainMenuFragment() const {
    MainMenuFragment mmf;
    string mmfname=mmf.NAME();
    FragmentExchangeBundle bundle;
    fragmentListenerPtr->onFragmentChange(mmfname,bundle);
}

void QuizFragment::onCreate(FragmentListener *fragmentHandler, const FragmentExchangeBundle &bundle) {
    focusedPointerText->setText(">");
    fragmentListenerPtr=fragmentHandler;
    setDefaultKeyMap();

    setQuizFromBundle(bundle);
    initAllPages();

    auto p = (pageIterator.getCurrentPage());
    string firstPage = ((*p)->getPageName());
    pageableLayout->switchPage(firstPage);
    quizPageHeadlineText->setText("STRANA| "+(*p)->getPageName());

    beginOfCurrentPageQuizLayoutsItr=pageableLayout->getDrawableHolderPageable()->beginOfPageDrawables(firstPage);
    endOfCurrentPageQuizLayoutsItr=pageableLayout->getDrawableHolderPageable()->endOfPageDrawables(firstPage);
    currentPageQuizLayoutsItr=beginOfCurrentPageQuizLayoutsItr;
}

void QuizFragment::onResume(const FragmentExchangeBundle &bundle) {
    // Vsechny drawable specificky pro kviz jsou pageable. Tudiz pro vymazani predchoziho kvizu a nahrazeni novym
    // lze vymazat vsechny pageable a pak znova zavolat oncreate. Stale elementy jsou tvoreny v baselayout v konstruktoru
    pageableLayout->getDrawableHolderPageable()->removeAllPageableDrawables();
    focusedPointerText->setVisible(true);
    onCreate(fragmentListenerPtr,bundle);
}

string QuizFragment::NAME() {
    return "QuizFragment";
}

void QuizFragment::interactFocused() {
    try {
        dynamic_pointer_cast<QuestionLayout>(*currentPageQuizLayoutsItr)
                ->onInteract();
    } catch (AnswerUserInputException &e) {
        DisplayConsole::displayMessage("Odpoved zadana ve spatnem formatu.");
        std::this_thread::sleep_for(std::chrono::seconds(2));
        drawableRefreshCallback();
    }
}

void QuizFragment::nextPage() {
    scrollableLayout->setScroll(0);
    int nextPageResponse = pageIterator.nextPage();

    if(nextPageResponse==PageIterator::NEXT_PAGE_OK){
        string nextPageName = pageIterator.getCurrentPage().operator*()->getPageName();

        pageableLayout->switchPage(nextPageName);

        beginOfCurrentPageQuizLayoutsItr=pageableLayout->getDrawableHolderPageable()->beginOfPageDrawables(nextPageName);
        endOfCurrentPageQuizLayoutsItr=pageableLayout->getDrawableHolderPageable()->endOfPageDrawables(nextPageName);
        currentPageQuizLayoutsItr=beginOfCurrentPageQuizLayoutsItr;

        drawableRefreshCallback();

        quizPageHeadlineText->setText("STRANA| "+pageIterator.getCurrentPage().operator*()->getPageName());
    }else if(nextPageResponse==PageIterator::NEXT_PAGE_END){
        string resultPage="result page";
        generateResultPage(resultPage);
        pageableLayout->switchPage(resultPage);
        scrollableLayout->setScroll(0);
        focusedPointerText->setVisible(false);
        quizPageHeadlineText->setVisible(false);
        drawableRefreshCallback();
    }
    // todo tady byl overlook
}

void QuizFragment::generateResultPage(const string &pageName) {
    quizPageHeadlineText->setVisible(false);
    focusedPointerText->setVisible(false);
    setResultKeyMap();

    auto resultLayout = make_shared<ScrollableLayout>(pageName,Position(4,0),[this](){
        drawableRefreshCallback();
    });
    auto l = dynamic_pointer_cast<Layout>(resultLayout);

    auto headlineText = make_shared<TextDrawable>("headline",Position(0,0),[this](){drawableRefreshCallback();});
    headlineText->setText("Kviz hotov!");
    auto d = dynamic_pointer_cast<Drawable>(headlineText);
    resultLayout->addDrawable(d);

    auto resultText = make_shared<TextDrawable>("result text", Position(0,2),[this](){drawableRefreshCallback();});
    int correctAnswers=quiz.getCorrectCount();
    int questionsCount= quiz.getQuestionsCount();
    resultText->setText("Spravne vyreseno: " + to_string(correctAnswers)+" / "+ to_string(questionsCount));
    auto d2= dynamic_pointer_cast<Drawable>(resultText);
    resultLayout->addDrawable(d2);

    string answersOutput = quiz.failAnswersOutputAggregated();
    string answerLine;
    size_t answersOutputSize = answersOutput.size();
    size_t index=0;
    int lineCount=0;
    if(!answersOutput.empty())
        while(index!= answersOutputSize-1) {
            if (answersOutput[index] == '\n') {
                auto answerOutputTxt = make_shared<TextDrawable>(("output_" + to_string(index)), Position(0, 5 + lineCount),
                                                                 [this]() { drawableRefreshCallback(); });
                answerOutputTxt->setText(answerLine);
                auto answerOutputDrawable = dynamic_pointer_cast<Drawable>(answerOutputTxt);
                resultLayout->addDrawable(answerOutputDrawable);
                answerLine="";
                lineCount++;
            } else {
                answerLine += answersOutput.at(index);
            }
            index++;
        }

    scrollableLayout->setMaximumScroll(lineCount);
    QuizFragment::pageableLayout->addInnerLayout(pageName,l);
}

void QuizFragment::focusNext() {
    // Pokud je zameren posledni tak nic nedelat
    if(currentPageQuizLayoutsItr+1==endOfCurrentPageQuizLayoutsItr){
        return;
    }
    currentPageQuizLayoutsItr++;

    int questionRelativeY = (*currentPageQuizLayoutsItr)->getRelativePosition().getY();
    int questionAbsoluteY = questionRelativeY+
                            baseLayout->getRelativePosition().getY()+
                            outterLayout->getRelativePosition().getY()+
                            scrollableLayout->getRelativePosition().getY()+
                            pageableLayout->getRelativePosition().getY();

    scrollableLayout->setScroll(questionAbsoluteY-focusedOffsetY);
    Fragment::drawableRefreshCallback();
}

void QuizFragment::focusPrevious() {
    if(currentPageQuizLayoutsItr!=beginOfCurrentPageQuizLayoutsItr){
        currentPageQuizLayoutsItr--;
    }else{
        return;
    }

    int questionRelativeY = (*currentPageQuizLayoutsItr)->getRelativePosition().getY();
    int questionAbsoluteY = questionRelativeY+
                            baseLayout->getRelativePosition().getY()+
                            outterLayout->getRelativePosition().getY()+
                            scrollableLayout->getRelativePosition().getY()+
                            pageableLayout->getRelativePosition().getY();

    scrollableLayout->setScroll(questionAbsoluteY-focusedOffsetY);
    Fragment::drawableRefreshCallback();
}

void QuizFragment::setQuizFromBundle(const FragmentExchangeBundle &bundle) {
    string chosenQuiz = bundle.getBundleElement("chosen_quiz");
    quiz = WizardInputOutput::decodeFromFile(chosenQuiz);
    pageIterator = quiz.getPageIterator(pagePolicyBuilder);
}

void QuizFragment::initAllPages() {
    int positionY=0;
    int i = 0;

    // Foreach page
    for (const auto& page:quiz.getPages()) {
        for (const auto &question : page->getQuestions()) {
            shared_ptr<Layout> questionLayout = question.getAnswer()->
                    getSpecificQuestionLayout(to_string(i), Position(0, positionY), [this]() {
                    drawableRefreshCallback();
            });
            auto l = dynamic_pointer_cast<QuestionLayout>(questionLayout);
            l->setQuestion(question);
            pageableLayout->addInnerLayout(page->getPageName(),questionLayout);
            positionY+=questionLayout->getHeight()+1;
            i++;
        }
        positionY=0;
    }
}
