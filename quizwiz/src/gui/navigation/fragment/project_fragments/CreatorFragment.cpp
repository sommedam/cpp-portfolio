//
// Created by voky on 12.04.22.
//

#include "CreatorFragment.h"
#include "../../../../quiz_containers/answer/exceptions/WizardFormatInputException.h"

bool CreatorFragment::doFileExists(const string &filename) {
    ifstream f(filename);
    return f.good();
}

void CreatorFragment::addPage() {
    if(currentPage != nullptr){
        addLineToScroll("");
    }
    fragmentListenerPtr->overrideKeyListening(); /* addPage using user input, default fragment key bind has to be stopped till the end of method */
    // prevent creating new page, if previous havent any questions
    if (currentPage != nullptr && currentPage->getQuestions().empty()) {
        DisplayConsole::displayMessage("Strana musi mit alespon jednu otazku.");
        this_thread::sleep_for(std::chrono::seconds(2));
        drawableRefreshCallback();
        return;
    }
    string pageNameInput;
    cout << "Zadejte nazev strany kvizu: ";
    std::getline(std::cin, pageNameInput);
    if(pageNameInput.empty()){
        DisplayConsole::displayMessage("Strana mit nazev alespon delky 1.");
        this_thread::sleep_for(std::chrono::seconds(2));
        drawableRefreshCallback();
        fragmentListenerPtr->stopOverrideKeyListening();
    }

    string enablePolicyInput;
    cout << "Pridat dodatecne pravidlo pro stranu [a/n]? ";
    std::getline(std::cin, enablePolicyInput);

    currentPage = make_shared<Page>(pageNameInput);
    string choosenPolicyTag;
    // if user wants to add policy. any other input is accepted as false.
    if(yes.find(enablePolicyInput) != yes.end()){
        cout << pagePolicyFactory.ALL_PAGE_FLAGS_LIST();
        string choosenPolicyInput;
        cout<<"# ";
        std::getline(std::cin, choosenPolicyInput);
        int choosenPolicyIndex;
        try {
            choosenPolicyIndex = stoi(choosenPolicyInput);
            auto policyCreator = pagePolicyFactory.PAGE_POLICY_OF_INDEX(choosenPolicyIndex)();
            choosenPolicyTag=policyCreator->TAG();
            currentPage->setPolicy(policyCreator);
        }
        catch (const invalid_argument& e) {
            DisplayConsole::displayMessage("Zadali jste neplatne cislo.");
            this_thread::sleep_for(chrono::seconds(2));
        }catch (const out_of_range& e){
            DisplayConsole::displayMessage("Zadali jste neplatne cislo.");
            this_thread::sleep_for(chrono::seconds(2));
        }
    }

    quiz.addPage(currentPage);
    string choosenPolicyPostfix;
    if(!choosenPolicyTag.empty()) {
        choosenPolicyPostfix = " [" + choosenPolicyTag + "]";
    }
    addLineToScroll("STRANA| "+ pageNameInput + choosenPolicyPostfix);
    addLineToScroll("-------------------------------------------------------------------");
    changeKeyMapDefault();
    drawableRefreshCallback();

    fragmentListenerPtr->stopOverrideKeyListening();
}

void CreatorFragment::addLineToScroll(const string &innerText) {
    auto text = make_shared<TextDrawable>("scroll_"+ to_string(numberOfAddedLines),Position(0,numberOfAddedLines),[this](){drawableRefreshCallback();});
    auto d = dynamic_pointer_cast<Drawable>(text);
    text->setText(innerText);
    scrollableLayout->addDrawable(d);
    Fragment::drawableRefreshCallback();
    numberOfAddedLines++;
    if(numberOfAddedLines>12){
        scrollableLayout->scrollDown();
    }
}

void CreatorFragment::changeKeyMapDefault() {
    shared_ptr<Drawable> startMessage;
    baseLayout->findDrawableRecursive("start_message", startMessage);
    startMessage->setVisible(false);

    bottom->removeAllKeys();
    bottom->addKey('q', "Pridat otazku");
    bottom->addKey('a', "Pridat stranku");
    bottom->addKey('s', "Ulozit kviz");
    bottom->addKey('x', "Opustit tvoric");

    std::map<unsigned char, std::function<void()>> keyMap;
    keyMap['q'] = [this]() { addQuestion(); };
    keyMap['a'] = [this]() { addPage(); };
    keyMap['s'] = [this]() { saveQuiz(); };
    keyMap['x'] = [this]() { goToMainMenuFragment(); };
    fragmentListenerPtr->onKeyMapChange(keyMap);
}

void CreatorFragment::saveQuiz() {
    if(currentPage->getQuestions().empty()){
        DisplayConsole::displayMessage("Strana musi mit alespon jednu otazku.");
        this_thread::sleep_for(std::chrono::seconds(2));
        drawableRefreshCallback();
        return;
    }

    fragmentListenerPtr->overrideKeyListening();
    cout<<"Ulozit kviz, zadejte nazev kvizu [bez mezer a diakritiky]: ";
    string filename;
    getline(cin,filename);
    filename="saved_quizwiz/"+filename;
    if(doFileExists(filename)){
        cout<<"Zadane jmeno kvizu jiz existuje..\n\n";
        saveQuiz();
    }else{
        std::ofstream outfile (filename);
        outfile << quiz;
        outfile.close();
    }
    goToMainMenuFragment(); /* after saving, return back to MainMenuFragment */
    fragmentListenerPtr->stopOverrideKeyListening();
}

void CreatorFragment::goToMainMenuFragment() {
    MainMenuFragment mmf;
    string mmfname=mmf.NAME();
    fragmentListenerPtr->onFragmentChange(mmfname, FragmentExchangeBundle());
}

void CreatorFragment::badInputQuestionIndex() {
    DisplayConsole::displayMessage("Zadali jste neplatne cislo.");
    this_thread::sleep_for(chrono::seconds(2));
    drawableRefreshCallback();
}

void CreatorFragment::addQuestion() {
    fragmentListenerPtr->overrideKeyListening(); /* creator of answer using cin, stop default fragment listening to prevent two input observers */

    cout<<"Zadejte zneni otazky: ";
    string questionHeadline;
    std::getline(std::cin, questionHeadline);

    cout<<"Vyberte typ odpovedi: "<<endl;
    cout<< answerFactory.ALL_ANSWERS_LIST(); /* menu answers to choose from */
    cout<<"# ";
    string choosenAnswerInput;
    getline(std::cin,choosenAnswerInput);
    shared_ptr<Answer> answer;
    try {
        int choosenAnswer;
        choosenAnswer = stoi(choosenAnswerInput);
        try {
            answer = answerFactory.ANSWER_CREATOR_OF_INDEX(choosenAnswer)();
        } catch (const WizardFormatInputException &e) {
            // Wizard failure
            DisplayConsole::displayMessage("Zadali jste neplatny vstup.");
            this_thread::sleep_for(chrono::seconds(2));
            drawableRefreshCallback();
            fragmentListenerPtr->stopOverrideKeyListening();
            return;
        }
    }
    /* not an index */
    catch (const invalid_argument& e) {
        badInputQuestionIndex();
        fragmentListenerPtr->stopOverrideKeyListening();
        return;
    }catch (const out_of_range&e) {
    /* wrong index of answer in list */
        badInputQuestionIndex();
        fragmentListenerPtr->stopOverrideKeyListening();
        return;
    }

    Question question(questionHeadline, answer);
    currentPage->addQuestion(question);
    addLineToScroll(questionHeadline);
    fragmentListenerPtr->stopOverrideKeyListening();
}

void CreatorFragment::onCreate(FragmentListener *fragmentHandler, const FragmentExchangeBundle &bundle) {
    fragmentListenerPtr = fragmentHandler;

    baseLayout = make_shared<Layout>("base", Position(0, 0), [this]() { drawableRefreshCallback(); });
    outterLayout = make_shared<Layout>("outter", Position(5, 2), [this]() { drawableRefreshCallback(); });
    scrollableLayout = make_shared<ScrollableLayout>("scrollable", Position(0, 0),
                                                     [this]() { drawableRefreshCallback(); });
    auto l = dynamic_pointer_cast<Layout>(scrollableLayout);

    outterLayout->addInnerLayout(l);
    baseLayout->addInnerLayout(outterLayout);

    bottom = make_shared<BottomKeysDrawable>("bottom keys", Position(0, DisplayConsole::bottomPosition()),
                                             [this]() { drawableRefreshCallback(); });
    auto d = dynamic_pointer_cast<Drawable>(bottom);
    baseLayout->addDrawable(d);

    setStartKeyMap();

    auto text = make_shared<TextDrawable>("start_message",Position(16,5),[this](){drawableRefreshCallback();});
    auto d2 = dynamic_pointer_cast<Drawable>(text);
    baseLayout->addDrawable(d2);
    text->setText("Vitejte v kvizovem tvorici. Zacenete pridanim nove stranky [a].");
}

void CreatorFragment::setStartKeyMap() {
    std::map<unsigned char, std::function<void()>> keyMap;
    bottom->removeAllKeys();
    bottom->addKey('a', "Pridat stranku");
    bottom->addKey('x', "Opustit tvorbu kvizu");
    keyMap['a'] = [this]() { addPage(); };
    keyMap['x'] = [this]() { goToMainMenuFragment(); };
    fragmentListenerPtr->onKeyMapChange(keyMap);
}

void CreatorFragment::onResume(const FragmentExchangeBundle &exchangeBundle) {
    currentPage= nullptr;
    numberOfAddedLines=0;
    quiz=Quiz();
    setStartKeyMap();
    scrollableLayout->setScroll(0);
    shared_ptr<Drawable> startMessage;
    baseLayout->findDrawableRecursive("start_message",startMessage);
    scrollableLayout->getDrawableHolder()->removeAllDrawablesInLayout();
    startMessage->setVisible(true);
    drawableRefreshCallback();
}

string CreatorFragment::NAME() {
    return name;
}
