//
// Created by voky on 23.03.22.
//

#ifndef QUIZWIZ_MAINMENUFRAGMENT_H
#define QUIZWIZ_MAINMENUFRAGMENT_H

#include "../Fragment.h"

/**
 * Main menu fragment, user can choose from quiz creator or quiz solving
 */
class MainMenuFragment : public Fragment {
public:
    FragmentListener *fragmentListenerPtr;
    void onCreate(FragmentListener * fragmentListener, const FragmentExchangeBundle & bundle) override;

    void setDefaultKeyMap();

    string NAME() override;

    void onResume(const FragmentExchangeBundle &bundle) override;

    void initUi();
};


#endif //QUIZWIZ_MAINMENUFRAGMENT_H
