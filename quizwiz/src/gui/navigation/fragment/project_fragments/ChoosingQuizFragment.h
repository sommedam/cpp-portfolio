//
// Created by voky on 03.04.22.
//

#ifndef QUIZWIZ_CHOOSINGQUIZFRAGMENT_H
#define QUIZWIZ_CHOOSINGQUIZFRAGMENT_H

#include <filesystem>
#include "../Fragment.h"
#include "MainMenuFragment.h"
#include "QuizFragment.h"
#include "../../../graphical/drawable/project_drawables/BottomKeysDrawable.h"
#include "../../../graphical/drawable/layout/ScrollableLayout.h"
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <filesystem>
#include <chrono>

using std::cout; using std::cin;
using std::endl; using std::string;
// Disabled for Progtest reasons
// using std::filesystem::directory_iterator;

/**
 * Fragment of quiz choosing screen.
 */
class ChoosingQuizFragment : public Fragment {
    FragmentListener *fragmentListenerPtr;
    shared_ptr<BottomKeysDrawable> bottomKeys;

public:
    void onCreate(FragmentListener *fragmentHandler, const FragmentExchangeBundle &bundle) override;

    void onResume(const FragmentExchangeBundle &exchangeBundle) override;

    string NAME() override;


private:
    void setDefaultKetMap();

    void initUi();

    shared_ptr<ScrollableLayout> scrollLayout;

    void inputQuizName();

    set<string> filesSet;

    void goToMainMenuFragment();

    bool doQuizSaveFileExists(const string &filename);
};


#endif //QUIZWIZ_CHOOSINGQUIZFRAGMENT_H
