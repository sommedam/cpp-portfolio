//
// Created by voky on 03.04.22.
//

#include "ChoosingQuizFragment.h"

void ChoosingQuizFragment::onCreate(FragmentListener *fragmentHandler, const FragmentExchangeBundle &bundle) {
    fragmentListenerPtr = fragmentHandler;
    initUi();
}

void ChoosingQuizFragment::onResume(const FragmentExchangeBundle &exchangeBundle) {
    setDefaultKetMap();
    drawableRefreshCallback();
}

void ChoosingQuizFragment::setDefaultKetMap() {
    map<unsigned char, function<void()>>map;
    bottomKeys->addKey('f',"zadat nazev kvizu");
    bottomKeys->addKey('s',"dolu");
    bottomKeys->addKey('x',"zpet do hlavni nabidky");
    map['s']=[this](){scrollLayout->setScroll(scrollLayout->getScrollOffset()+4);};
    map['f']=[this](){inputQuizName();};
    map['x']=[this](){goToMainMenuFragment();};
    fragmentListenerPtr->onKeyMapChange(map);
}

void ChoosingQuizFragment::inputQuizName() {
    DisplayConsole::displayBitmap(baseLayout->getBitmap(), "Zadejte nazev kvizu: ");
    string file;
    getline(cin,file);
    //if (filesSet.find(file) == filesSet.end()) {
    if(!doQuizSaveFileExists(file)){
        DisplayConsole::displayMessage("Takovy kviz neexistuje."); /* non-existent file of given name */
        this_thread::sleep_for(std::chrono::seconds(2));
        drawableRefreshCallback();
        return;
    }

    FragmentExchangeBundle fragmentBundle; /* file exist, make fragment transaction to QuizFragment and send name of quiz as FragmentBundle */
    fragmentBundle.setBundleElement("chosen_quiz", file);
    string qfname = "QuizFragment";
    fragmentListenerPtr->onFragmentChange(qfname, fragmentBundle);
}

string ChoosingQuizFragment::NAME() {
    return "ChoosingQuizFragment";
}

void ChoosingQuizFragment::initUi() {
    const function<void()> observer = [this]() {
        drawableRefreshCallback();
    };

    // Layout initialization
    baseLayout = make_shared<Layout>("base", Position(0, 0), observer);
    auto outterLayout = make_shared<Layout>("outter", Position(36, 6), observer);
    scrollLayout = make_shared<ScrollableLayout>("scroll", Position(0, 0), observer);
    auto l = dynamic_pointer_cast<Layout>(scrollLayout);
    outterLayout->addInnerLayout(l);
    baseLayout->addInnerLayout(outterLayout);
    outterLayout->setMaxHeight(9);

    // Key map initialization
    bottomKeys = make_shared<BottomKeysDrawable>("bottom_keys",Position(0,DisplayConsole::bottomPosition()),observer);
    setDefaultKetMap();
    auto d = dynamic_pointer_cast<Drawable>(bottomKeys);
    baseLayout->addDrawable(d);

    // Text intitialization
    auto headline = make_shared<TextDrawable>("headline",Position(9,3),observer);
    string textInner="Zmackente 'f' a zadejte nazev kvizu, ktery chcete otevrit. Napriklad: 'example'";
    headline->setText(textInner);
    auto hd = static_pointer_cast<Drawable>(headline);
    baseLayout->addDrawable(hd);

    // Quiz files list initialization
    string path = "saved_quizwiz";
    int i = 0;
    // List all available Quizes. Disabled in Progtest.
    /*for (const auto &file: directory_iterator(path)) {
        string quizName;
        std::istringstream pathStream(file.path());
        getline(pathStream,quizName,'/');
        getline(pathStream,quizName,'/');
        filesSet.insert(quizName);
        auto text = make_shared<TextDrawable>(file.path(), Position(11-(quizName.size()/2), i * 2),observer);
        string innerText = file.path();
        text->setText(quizName);
        auto d1 = dynamic_pointer_cast<Drawable>(text);
        scrollLayout->addDrawable(d1);
        i++;
    }*/
    drawableRefreshCallback();
    // Maximum scroll is number of lines quiz is long minus upper offset
    scrollLayout->setMaximumScroll((i*2)-6);
}

void ChoosingQuizFragment::goToMainMenuFragment() {
    MainMenuFragment mmf;
    string mmfname=mmf.NAME();
    FragmentExchangeBundle bundle;
    fragmentListenerPtr->onFragmentChange(mmfname,bundle);
}

bool ChoosingQuizFragment::doQuizSaveFileExists(const string &filename) {
    ifstream f("saved_quizwiz/"+filename);
    return f.good();
}
