//
// Created by voky on 23.03.22.
//

#ifndef QUIZWIZ_NOFRAGMENTOFNAMEEXCEPTION_H
#define QUIZWIZ_NOFRAGMENTOFNAMEEXCEPTION_H

#include <stdexcept>

using namespace std;
class NoFragmentOfNameException : public invalid_argument{
public:
    NoFragmentOfNameException ( )
            : invalid_argument ( "Zadany fragment neexistuje." )
    {
    }
};


#endif //QUIZWIZ_NOFRAGMENTOFNAMEEXCEPTION_H
