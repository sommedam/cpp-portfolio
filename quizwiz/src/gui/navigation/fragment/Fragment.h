//
// Created by voky on 03.04.22.
//

#ifndef SEMESTRALKA_FRAGMENT_H
#define SEMESTRALKA_FRAGMENT_H


#include "FragmentListener.h"
#include "FragmentExchangeBundle.h"
#include "../../DisplayConsole.h"
#include "../../graphical/drawable/layout/Layout.h"

/**
 * Class for wrapping numerous graphical components to single screen (user window).
 * Active fragment is one at a time. Active fragment is only class to call @link DisplayConsole.h
 * */
class Fragment {
protected:
    shared_ptr<Layout> baseLayout;
public:
    void drawableRefreshCallback() const;

    // Fragment lifecycle methods
    /**
     * Calling this method will build given Fragment layout and switch to it.
     * @param fragmentListener reference to fragmentHandler, which is Fragment part of
     * @param bundle extra information from previous fragment
     */
    virtual void onCreate(FragmentListener *fragmentListener, const FragmentExchangeBundle &bundle) = 0;

    /**
     * Called when switched to another Fragment.
     * onPause will cancel all background tasks.
     */
    virtual void onPause() {};

    /**
     * Called when Fragment was previously created, switched out and switched in again.
     * @param bundle extra information from previous fragment
     */
    virtual void onResume(const FragmentExchangeBundle &bundle) {};

    /**
     * Name of the Fragment
     * @return unique name of Fragment, not displayed to user
     */
    virtual string NAME() = 0;
};


#endif //SEMESTRALKA_FRAGMENT_H
