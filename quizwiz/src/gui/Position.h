//
// Created by voky on 28.03.22.
//

#ifndef QUIZWIZ_POSITION_H
#define QUIZWIZ_POSITION_H

/**
 * Class for holding Y, X coordinates of Drawables.
 * Y - number of lines from top
 * X - number of columns from left
 */
class Position {
    int x{};
    int y{};

public:
    Position(int x, int y) : x(x), y(y) {}

    [[nodiscard]] int getX() const;

    [[nodiscard]] int getY() const;


    bool operator == (Position &second) const;

    Position operator +(Position&b) const;
};


#endif //QUIZWIZ_POSITION_H
