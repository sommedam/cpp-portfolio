//
// Created by voky on 28.03.22.
//

#ifndef QUIZWIZ_BITMAP_H
#define QUIZWIZ_BITMAP_H

#include <vector>

using namespace std;

/**
 * Class for holding char byteField, smallest displayable unit
 */
class Bitmap {
    vector<vector<char>> byteField;
    int height = 0;
    int width = 0;

    /**
     * Assign field with one null character to byteField
     */
    void initialize();


public:
    /**
     * Cut upper n byteField lines
     * @param btm bitmap to be cut
     * @param minus positive number of lines to be cut
     */
    static void cropTop(Bitmap& btm, int minus);

    void erase();

    char read(int X,int Y);

    /**
     * Assigns character to field
     * @param X horizontal position
     * @param Y vertical position
     * @param c character to write
     */
    void write(const int &X, const int &Y, const char &c);

    [[nodiscard]] int getHeight() const;

    [[nodiscard]] int getWidth() const;

    void setHeight(int height);
};


#endif //QUIZWIZ_BITMAP_H
