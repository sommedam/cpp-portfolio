//
// Created by voky on 30.03.22.
//

#ifndef QUIZWIZ_PAGEABLELAYOUT_H
#define QUIZWIZ_PAGEABLELAYOUT_H


#include "../Drawable.h"
#include "Layout.h"

/**
 * Layout with multiple sets of inner layuots and drawables, indexed by page name
 */
class PageableLayout : public Layout {
    shared_ptr<DrawableHolderPageable> drawableHolderPageable;

public:
    PageableLayout(const string &drawableId, const Position &relativePosition,const function<void()> &observer);

    Bitmap &getBitmap() override;

    /**
     * Add layout to page of name
     * @param page name of page
     * @param l layout to add
     */
    void addInnerLayout(const string& page,shared_ptr<Layout> &l);

    /**
     * Add Drawable to page of name
     * @param page name of page
     * @param d Drawable to add
     */
    void addDrawable(const string& page,shared_ptr<Drawable> &d);

    /**
     * Hides Drawables of all pages but the entered one
     * @param page name of page to be displayed
     */
    void switchPage(string page);

    [[nodiscard]] const shared_ptr<DrawableHolderPageable> &getDrawableHolderPageable() const;
};


#endif //QUIZWIZ_PAGEABLELAYOUT_H
