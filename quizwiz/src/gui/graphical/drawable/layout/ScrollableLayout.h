//
// Created by voky on 29.03.22.
//

#ifndef QUIZWIZ_SCROLLABLELAYOUT_H
#define QUIZWIZ_SCROLLABLELAYOUT_H


#include <cstdint>
#include "Layout.h"
#include "../Drawable.h"

/**
 * Layout with option of scrolling down or up in console view.
 * Using of Layout is recommended with otter wrapping layout, so this layout wont overflow its bounds.
 */
class ScrollableLayout: public Layout {
    int scrollOffset=0;
    int maximumScroll=INT16_MAX;

public:
    /**
     * Set absolute scroll of layout
     * @param n scroll in terminal lines
     */
    void setScroll(int n);

    void scrollDown();

    void scrollUp();

    ScrollableLayout(const string &drawableId, const Position &relativePosition, const function<void()> &observer);


    Bitmap rb;
    Bitmap &getBitmap() override;

    int getHeight() override;

    [[nodiscard]] Position getRelativePosition()const override;

    void setMaximumScroll(int ms);

    [[nodiscard]] int getScrollOffset() const;

    [[nodiscard]] int getMaximumScroll() const;
};


#endif //QUIZWIZ_SCROLLABLELAYOUT_H
