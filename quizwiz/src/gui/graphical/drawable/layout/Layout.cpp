//
// Created by voky on 24.03.22.
//

#include "Layout.h"

Bitmap &Layout::getBitmap() {
    r = drawableHolder->getAggregatedBitmap();
    if(maxHeightSet) {
        r.setHeight(min(maxHeight,r.getHeight()));
    }
    return r;
}

void Layout::addInnerLayout(shared_ptr<Layout> &l) {
    innerLayouts.push_back(l);
    shared_ptr<Drawable> d = std::static_pointer_cast<Drawable>(l);
    addDrawable(d);
}

void Layout::addDrawable(shared_ptr<Drawable> &d) {
    drawableHolder->addDrawable(d);
}

bool Layout::findDrawableRecursive(const string &drawableId, shared_ptr<Drawable> &in) {
    // If not found
    if(!drawableHolder->findDrawable(drawableId, in)){
        for(const auto& layout:innerLayouts){
            if(layout->findDrawableRecursive(drawableId, in)){
                return true;
            }
        }
    }else{
        // Drawable of Id was found
        // in assigned in Drawable find
        return true;
    }
    // Not found
    return false;
}

void Layout::setMaxHeight(int mh) {
    this->maxHeight = mh;
    maxHeightSet = true;
}

const shared_ptr<DrawableHolder> &Layout::getDrawableHolder() const {
    return drawableHolder;
}
