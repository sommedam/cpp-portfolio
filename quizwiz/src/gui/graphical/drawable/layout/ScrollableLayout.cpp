//
// Created by voky on 29.03.22.
//

#include "ScrollableLayout.h"

void ScrollableLayout::setScroll(int n) {
    scrollOffset = min(n,maximumScroll);
    Drawable::observer();
}

void ScrollableLayout::scrollDown() {
    if(scrollOffset<maximumScroll)
        scrollOffset++;
    Drawable::observer();
}

void ScrollableLayout::scrollUp() {
    scrollOffset--;
    Drawable::observer();
}

ScrollableLayout::ScrollableLayout(const string &drawableId, const Position &relativePosition,
                                   const function<void()> &observer)
        : Layout(drawableId, relativePosition, observer) {}

Bitmap &ScrollableLayout::getBitmap() {
    Bitmap initial = Layout::getBitmap();
    Position initialPosition = Drawable::getRelativePosition();
    // If position summed with scroll is bigger than zero, Drawable dont need to be cut
    // else it will be cut by overflow
    int cropPoints = abs(min(0, initialPosition.getY() - scrollOffset));
    Bitmap::cropTop(initial, cropPoints);
    rb = initial;
    return rb;
}

int ScrollableLayout::getHeight() {
    Position initial = Drawable::relativePosition;
    // If layout is overflown over zero, lower height by overflown points
    if(initial.getY()-scrollOffset<0){
        return max(0,Drawable::getHeight()-abs(initial.getY()-scrollOffset));
    }else{
        // super
        return Drawable::getHeight();
    }
}

void ScrollableLayout::setMaximumScroll(int ms) {
    ScrollableLayout::maximumScroll = ms;
}

Position ScrollableLayout::getRelativePosition() const {
    Position initial = Drawable::relativePosition;
    int ypsilonCalculated = initial.getY() - scrollOffset;
    return {initial.getX(),max(0, ypsilonCalculated)};
}

int ScrollableLayout::getScrollOffset() const {
    return scrollOffset;
}

int ScrollableLayout::getMaximumScroll() const {
    return maximumScroll;
}
