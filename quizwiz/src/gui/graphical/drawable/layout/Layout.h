//
// Created by voky on 24.03.22.
//

#ifndef QUIZWIZ_LAYOUT_H
#define QUIZWIZ_LAYOUT_H

#include <iostream>
#include "../Drawable.h"
#include "../../drawable_holder/DrawableHolder.h"
#include "../../drawable_holder/DrawableHolderPageable.h"

class Layout : public Drawable {
protected:
    vector<shared_ptr<Layout>> innerLayouts;
    shared_ptr<DrawableHolder> drawableHolder;
    bool maxHeightSet = false;
    int maxHeight = 0;

public:
    Layout(const string &drawableId, const Position &relativePosition, const function<void()> &observer,
           const shared_ptr<DrawableHolderPageable> &drawableHolderPageable) : Drawable(drawableId, relativePosition, observer){
        drawableHolder = std::static_pointer_cast<DrawableHolder>(drawableHolderPageable);
    }

    Layout(const string &drawableId, const Position &relativePosition, const function<void()> &observer) : Drawable(
            drawableId, relativePosition, observer) {
        drawableHolder= make_shared<DrawableHolder>();
    }

    Bitmap r;
    /**
     * Agreggated bitmap of all sublayouts and drawables
     * @return Bitmap of this layout dimensions
     */
    Bitmap &getBitmap() override;

    virtual void addInnerLayout(shared_ptr<Layout> &l);
    virtual void addDrawable(shared_ptr<Drawable> &d);

    /**
     * Search for Drawable of given ID. Returning first found in DrawwableHolder tree.
     * @param drawableId ID of Drawable to find
     * @param in empty instance, will be rewriten by found Drawable, or if not found, it will be left unchanged
     * @return boolean true if Drawable found, else false
     */
    bool findDrawableRecursive(const string &drawableId, shared_ptr<Drawable>&in);

    [[nodiscard]] const shared_ptr<DrawableHolder> &getDrawableHolder() const;

    /**
     * Set maximum Bitmap height of layout
     * @param mh maximum in lines of console
     */
    void setMaxHeight(int mh);
};


#endif //QUIZWIZ_LAYOUT_H
