//
// Created by voky on 13.04.22.
//

#include "ChoiceAnswerQuestionLayout.h"

void ChoiceAnswerQuestionLayout::setQuestion(const Question &question) {
    QuestionLayout::setQuestion(question);
    ChoiceAnswerQuestionLayout::generateAnswerSpecificStructure();
}

void ChoiceAnswerQuestionLayout::onInteract() {
    string answer;
    cout << question.getAnswer()->INPUT_FORMAT();
    getline(cin,answer);
    auto choiceAnswer = dynamic_pointer_cast<ChoiceAnswer>(question.getAnswer());
    choiceAnswer->setInput(answer);

    // disable option arrows
    for (const auto &option: choiceAnswer->getKeyMappedOptions()) {
        shared_ptr<Drawable> r;
        Layout::findDrawableRecursive(focusablePrefix + option.first, r);
        dynamic_pointer_cast<TextDrawable>(r)->setText(" ");
    }

    // for user chosen options enable arrow
    for (auto option: choiceAnswer->getChosenOptions()) {
        shared_ptr<Drawable> r;
        Layout::findDrawableRecursive(focusablePrefix + option, r);
        string sign;
        sign += chosenSign;
        dynamic_pointer_cast<TextDrawable>(r)->setText(sign);
    }
}

ChoiceAnswerQuestionLayout::ChoiceAnswerQuestionLayout(const string &drawableId, const Position &relativePosition,
                                                       const function<void()> &observer) : QuestionLayout(drawableId, relativePosition,
                                                                                                          observer) {}

void ChoiceAnswerQuestionLayout::generateAnswerSpecificStructure() {
    auto a = question.getAnswer();
    auto choiceAnswer = dynamic_pointer_cast<ChoiceAnswer>(a);
    auto map = choiceAnswer->generateKeyMappedOptions();

    int index = 0;
    // for each option
    for (const auto &option: map) {
        int linePositionY = index + QuestionLayout::headerBottomY();
        // focusable is option arrow
        // option.first je znak na ktery se vola
        auto focusable = make_shared<TextDrawable>(focusablePrefix + option.first, Position(1, linePositionY),
                                                   observer);
        auto answerText = make_shared<TextDrawable>(textPrefix + option.first, Position(2, linePositionY),
                                                    observer);
        string answerString = "[";
        answerString += option.first;
        answerString += "] " + option.second.second;
        answerText->setText(answerString);

        auto d1 = dynamic_pointer_cast<Drawable>(focusable);
        auto d2 = dynamic_pointer_cast<Drawable>(answerText);

        Layout::addDrawable(d1);
        Layout::addDrawable(d2);
        index++;
    }
}
