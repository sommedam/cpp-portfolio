//
// Created by voky on 04.04.22.
//

#include "TextAnswerQuestionLayout.h"

void TextAnswerQuestionLayout::setQuestion(const Question &question) {
    QuestionLayout::setQuestion(question);
    TextAnswerQuestionLayout::generateAnswerSpecificStructure();
}

void TextAnswerQuestionLayout::onInteract() {
    string answer;
    cout << "Zadejte odpoved: ";
    getline(cin,answer);
    auto textAnswer= dynamic_pointer_cast<Answer>(question.getAnswer());
    textAnswer->setInput(answer);

    shared_ptr<Drawable> r;
    Layout::findDrawableRecursive(ANSWER_TEXT_DRAWABLE_ID, r);
    dynamic_pointer_cast<TextDrawable>(r)->setText(answer);

}

void TextAnswerQuestionLayout::generateAnswerSpecificStructure() {
    auto dummyText = make_shared<TextDrawable>("dummy_text", Position(2, headerBottomY()),
                                               observer);
    dummyText->setText("Vase odpoved: ");
    auto d = dynamic_pointer_cast<Drawable>(dummyText);
    Layout::addDrawable(d);
    auto answerText = make_shared<TextDrawable>(ANSWER_TEXT_DRAWABLE_ID, Position(16, headerBottomY()),
                                                observer);
    auto d2 = dynamic_pointer_cast<Drawable>(answerText);
    Layout::addDrawable(d2);
}
