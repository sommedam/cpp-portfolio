//
// Created by voky on 04.04.22.
//

#ifndef QUIZWIZ_TEXTANSWERQUESTIONLAYOUT_H
#define QUIZWIZ_TEXTANSWERQUESTIONLAYOUT_H


#include "QuestionLayout.h"
#include "../../../../DisplayConsole.h"
#include "../../../../../quiz_containers/answer/text_field_answer/TextFieldAnswer.h"

/*
 * Question layout with text user input
 */
class TextAnswerQuestionLayout: public QuestionLayout {
    const string ANSWER_TEXT_DRAWABLE_ID = "answerText";

public:
    TextAnswerQuestionLayout(const string &drawableId, const Position &relativePosition,
                             const function<void()> &observer) : QuestionLayout(drawableId, relativePosition,observer) {}

    void setQuestion(const Question &question) override;

    void onInteract() override;

protected:
    void generateAnswerSpecificStructure() override;
};


#endif //QUIZWIZ_TEXTANSWERQUESTIONLAYOUT_H
