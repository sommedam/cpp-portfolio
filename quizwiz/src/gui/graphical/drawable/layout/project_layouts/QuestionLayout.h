//
// Created by voky on 04.04.22.
//

#ifndef QUIZWIZ_QUESTIONLAYOUT_H
#define QUIZWIZ_QUESTIONLAYOUT_H


#include "../Layout.h"
#include "../../../../../quiz_containers/Question.h"
#include "../../TextDrawable.h"

/*
 * Class displays question specific layouts.
 * Contains interface for user input.
 */
class QuestionLayout : public Layout {
protected:
    Question question;

public:
    QuestionLayout(const string &drawableId, const Position &relativePosition,
                   const function<void()> &observer)
            : Layout(drawableId, relativePosition, observer) {}


    virtual void setQuestion(const Question &q) {
        QuestionLayout::question = q;
        generateCommonStructure();
    }

    /*
     * On user interaction
     */
    virtual void onInteract() = 0;


protected:
    /**
     * Y-offset for answer specific drawables
     */
    static int headerBottomY() {
        return 4;
    }

    virtual void generateAnswerSpecificStructure() = 0;

private:
    /**
     * Generates question head and other drawables, common to all answer types
     */
    void generateCommonStructure() {
        auto topDecoration = make_shared<TextDrawable>("topdeco", Position(0,0), observer);
        string decorationInner = "....................";
        topDecoration->setText(decorationInner);
        auto d1 = dynamic_pointer_cast<Drawable>(topDecoration);
        Layout::addDrawable(d1);

        auto topLine = make_shared<TextDrawable>("topline", Position(2, 3), observer);
        string lineInner = question.getAnswer()->DESCRIPTION();
        topLine->setText(lineInner);
        auto d2 = dynamic_pointer_cast<Drawable>(topLine);
        Layout::addDrawable(d2);

        auto headlineText = make_shared<TextDrawable>("headline", Position(2, 1), observer);
        headlineText->setText(question.getHeader());
        auto d3 = dynamic_pointer_cast<Drawable>(headlineText);
        Layout::addDrawable(d3);
    }
};


#endif //QUIZWIZ_QUESTIONLAYOUT_H
