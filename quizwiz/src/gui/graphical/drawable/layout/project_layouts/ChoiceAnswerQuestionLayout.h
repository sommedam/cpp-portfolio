//
// Created by voky on 13.04.22.
//

#ifndef QUIZWIZ_CHOICEANSWERQUESTIONLAYOUT_H
#define QUIZWIZ_CHOICEANSWERQUESTIONLAYOUT_H


#include "QuestionLayout.h"
#include "../../../../../quiz_containers/Question.h"
#include "../../../../../quiz_containers/answer/choice_answer/ChoiceAnswer.h"
#include "../../TextDrawable.h"

/*
 * question layout with multiple options to choose from in user input
 */
class ChoiceAnswerQuestionLayout: public QuestionLayout {
private:
    string focusablePrefix = "focusable_";
    string textPrefix = "text_";
    char chosenSign = '#';

public:
    ChoiceAnswerQuestionLayout(const string &drawableId, const Position &relativePosition,
    const function<void()> &observer);

    void setQuestion(const Question &question) override;

    void onInteract() override;
protected:
    void generateAnswerSpecificStructure() override;
};


#endif //QUIZWIZ_CHOICEANSWERQUESTIONLAYOUT_H
