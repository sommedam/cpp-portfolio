//
// Created by voky on 30.03.22.
//

#include "PageableLayout.h"


PageableLayout::PageableLayout(const string &drawableId, const Position &relativePosition,
                               const function<void()> &observer) :
        Layout(drawableId, relativePosition, observer, make_shared<DrawableHolderPageable>()){
    drawableHolderPageable = std::static_pointer_cast<DrawableHolderPageable>(Layout::drawableHolder);
}

Bitmap &PageableLayout::getBitmap() {
    return drawableHolderPageable->getAggregatedBitmap();
}

void PageableLayout::addInnerLayout(const string &page, shared_ptr<Layout> &l) {
    Layout::innerLayouts.push_back(l);
    shared_ptr<Drawable> d = std::static_pointer_cast<Drawable>(l);
    addDrawable(page,d);
}

void PageableLayout::addDrawable(const string &page, shared_ptr<Drawable> &d) {
    drawableHolderPageable->addDrawable(page,d);
}

void PageableLayout::switchPage(string page) {
    drawableHolderPageable->setPageActive(page);
}

const shared_ptr<DrawableHolderPageable> &PageableLayout::getDrawableHolderPageable() const {
    return drawableHolderPageable;
}
