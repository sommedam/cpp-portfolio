//
// Created by voky on 24.03.22.
//

#include "Drawable.h"

#include <utility>


Position Drawable::getRelativePosition() const {
    return relativePosition;
}

const string &Drawable::getDrawableId() const {
    return drawableId;
}

Drawable::Drawable(string drawableId, const Position &relativePosition, function<void()> observer) : drawableId(std::move(
        drawableId)), relativePosition(relativePosition), observer(std::move(observer)) {}

int Drawable::getHeight() {
    return getBitmap().getHeight();
}

void Drawable::setVisible(bool v) {
    Drawable::visible = v;
}

int Drawable::getWidth() {
    return getBitmap().getWidth();
}

bool Drawable::isVisible() const {
    return visible;
}
