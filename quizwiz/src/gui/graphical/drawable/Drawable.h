//
// Created by voky on 24.03.22.
//

#ifndef QUIZWIZ_DRAWABLE_H
#define QUIZWIZ_DRAWABLE_H

#include <utility>
#include <vector>
#include <string>
#include <functional>
#include "../../Position.h"
#include "../Bitmap.h"

using namespace std;
/**
 * Abstract class for graphic elements, renderable to console.
 */
class Drawable {
    bool visible = true;
    string drawableId; /* ID unique in layout */
protected:
    Position relativePosition; /* position of Drawable in Layout */
    function<void()> observer; /* observer called, every time Drawable has changed */
public:
    Drawable(string drawableId, const Position &relativePosition, function<void()> observer);

    [[nodiscard]] const string &getDrawableId() const;

    [[nodiscard]] virtual Position getRelativePosition() const;

    virtual Bitmap &getBitmap() = 0;

    virtual int getHeight();

    int getWidth();

    void setVisible(bool v);

    [[nodiscard]] bool isVisible() const;
};
#endif //QUIZWIZ_DRAWABLE_H
