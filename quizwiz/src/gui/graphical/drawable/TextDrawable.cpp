//
// Created by voky on 28.03.22.
//

#include "TextDrawable.h"

TextDrawable::TextDrawable(const string &drawableId, const Position &relativePosition, function<void()> observer)
        : Drawable(drawableId, relativePosition, std::move(observer)){}

void TextDrawable::setText(const string &text) {
    for (size_t i = 0; i < text.size(); ++i) {
        bitmap.write( i,0,text[i]);
    }
    // Updatovat
    Drawable::observer.operator()();
}

Bitmap &TextDrawable::getBitmap() {
    return bitmap;
}
