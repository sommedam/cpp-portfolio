//
// Created by voky on 28.03.22.
//

#ifndef QUIZWIZ_TEXTDRAWABLE_H
#define QUIZWIZ_TEXTDRAWABLE_H


#include "Drawable.h"

#include <utility>
#include <memory>

/**
 * Single line of text drawable
 */
class TextDrawable: public Drawable{
    Bitmap bitmap;

public:
    TextDrawable(const string &drawableId, const Position &relativePosition, function<void()> observer);
    /**
     * Set text to be displayed, observer will be notified
     * @param text text to display
     */
    void setText(const string& text);

    Bitmap& getBitmap() override;
};


#endif //QUIZWIZ_TEXTDRAWABLE_H
