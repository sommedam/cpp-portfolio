//
// Created by voky on 12.04.22.
//

#include "BottomKeysDrawable.h"

BottomKeysDrawable::BottomKeysDrawable(const string &drawableId, const Position &relativePosition,
                                       const function<void()> &observer) : TextDrawable(drawableId, relativePosition,
                                                                                        observer) {}

void BottomKeysDrawable::removeAllKeys() {
    keyDescription.clear();
}

void BottomKeysDrawable::addKey(char key, const string& description) {
    keyDescription.emplace_back(pair<char,string>(key,description));
    string textInner = "   ";
    for(auto& p : keyDescription){
        char c = p.first;
        textInner+="[";
        textInner+=c;
        textInner+="] - " + p.second;
        textInner+="    ";
    }
    textInner+="                                                              ";
    TextDrawable::setText(textInner);
}
