//
// Created by voky on 12.04.22.
//

#ifndef QUIZWIZ_BOTTOMKEYSDRAWABLE_H
#define QUIZWIZ_BOTTOMKEYSDRAWABLE_H


#include "../TextDrawable.h"

/**
 * Bottom console display of letters and bind actions.
 * Only displaying, input logic is not included
 */
class BottomKeysDrawable : public TextDrawable{
vector<pair<char,string>> keyDescription;

public:
    BottomKeysDrawable(const string &drawableId, const Position &relativePosition, const function<void()> &observer);

    void removeAllKeys();

    void addKey(char key, const string& description);
};


#endif //QUIZWIZ_BOTTOMKEYSDRAWABLE_H
