//
// Created by voky on 28.03.22.
//

#ifndef QUIZWIZ_BITMAPWIDTHHEIGHTEXCEPTION_H
#define QUIZWIZ_BITMAPWIDTHHEIGHTEXCEPTION_H


#include <stdexcept>

class BitmapWidthHeightException: public std::invalid_argument {
public:
    BitmapWidthHeightException():
    invalid_argument ( "bitmap calculation error" ){}
};


#endif //QUIZWIZ_BITMAPWIDTHHEIGHTEXCEPTION_H
