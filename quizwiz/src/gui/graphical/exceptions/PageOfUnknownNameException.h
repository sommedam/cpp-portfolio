//
// Created by voky on 30.03.22.
//

#ifndef QUIZWIZ_PAGEOFUNKNOWNNAMEEXCEPTION_H
#define QUIZWIZ_PAGEOFUNKNOWNNAMEEXCEPTION_H


#include <stdexcept>

class PageOfUnknownNameException: public std::invalid_argument {
public:
    PageOfUnknownNameException():
            invalid_argument ( "activated page of unknown name" ){}

};

#endif //QUIZWIZ_PAGEOFUNKNOWNNAMEEXCEPTION_H
