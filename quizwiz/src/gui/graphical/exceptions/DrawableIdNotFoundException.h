//
// Created by voky on 28.03.22.
//

#ifndef QUIZWIZ_DRAWABLEIDNOTFOUNDEXCEPTION_H
#define QUIZWIZ_DRAWABLEIDNOTFOUNDEXCEPTION_H


#include <stdexcept>

class DrawableIdNotFoundException: public std::invalid_argument {
public:
    DrawableIdNotFoundException():
            std::invalid_argument ( "bitmap calculation error" ){}
};



#endif //QUIZWIZ_DRAWABLEIDNOTFOUNDEXCEPTION_H
