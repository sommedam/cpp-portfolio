//
// Created by voky on 28.03.22.
//

#ifndef QUIZWIZ_DUPLICATEOFDRAWABLEIDEXCEPTION_H
#define QUIZWIZ_DUPLICATEOFDRAWABLEIDEXCEPTION_H


#include <stdexcept>
class DuplicateOfDrawableIdException: public std::invalid_argument {
public:
    DuplicateOfDrawableIdException():
          invalid_argument ( "drawable id not unique in given layout" ){}

};


#endif //QUIZWIZ_DUPLICATEOFDRAWABLEIDEXCEPTION_H
