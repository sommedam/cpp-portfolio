//
// Created by voky on 24.03.22.
//
#ifndef QUIZWIZ_DRAWABLEHOLDER_H
#define QUIZWIZ_DRAWABLEHOLDER_H


#include <memory>
#include <set>
#include <map>
#include "../drawable/Drawable.h"

using namespace std;

/**
 * Holds list of all inserted Drawables.
 */
class DrawableHolder {
protected:
    vector<shared_ptr<Drawable>> prioritySortedDrawables;
    std::map<string, shared_ptr<Drawable>> nameIndexedDrawables;
    Bitmap agregatedBitmap;

public:
    /**
     * Find Drawable in holder
     * @param drawableId ID of Drawable to find
     * @param r in/out parameter, if found will be replaced by Drawable
     * @return if Drawable found true
     */
    bool findDrawable(const string &drawableId,shared_ptr<Drawable> &r);

    virtual Bitmap &getAggregatedBitmap();

    void addDrawable(const shared_ptr<Drawable> &d);

    /**
     * Will remove all Drawables from Holder
     */
    void removeAllDrawablesInLayout();

    void removeDrawablesOfIdStartingAs(const string& perex);
};



#endif //QUIZWIZ_DRAWABLEHOLDER_H
