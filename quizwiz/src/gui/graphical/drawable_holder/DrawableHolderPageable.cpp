//
// Created by voky on 30.03.22.
//

#include "DrawableHolderPageable.h"
#include "../exceptions/PageOfUnknownNameException.h"

void DrawableHolderPageable::addDrawable(const string &page, const shared_ptr<Drawable> &d) {
    if(pageIndexedDrawables.find(page)==pageIndexedDrawables.end()){
        vector<shared_ptr<Drawable>> inner;
        inner.push_back(d);
        pageIndexedDrawables.insert(pair<string, vector<shared_ptr<Drawable>>>(page,inner));
    }else {
        pageIndexedDrawables.find(page)->second.push_back(d);
    }
    // super
    DrawableHolder::addDrawable(d);
}

void DrawableHolderPageable::setPageActive(string &toPage) {
    // Lze aktivovat jen takovou stranku, na kterou se neco zapisovalo
    if(pageIndexedDrawables.find(toPage) == pageIndexedDrawables.end()){
        throw PageOfUnknownNameException();
    }else{
        // Inicializace
        if(!activePageId.empty()){
            for(auto& drawable:pageIndexedDrawables.find(activePageId)->second){
                drawable->setVisible(false);
            }
        }else{
            // Napoprve deaktivovat vsechny drawable ve strankach az na drawable ve zvolene strance
            for(auto& pageDrawable:pageIndexedDrawables){
                if(pageDrawable.first!=toPage){
                    for(const auto& drawable:pageDrawable.second){
                        drawable->setVisible(false);
                    }
                }
            }
        }
        for(auto& drawable:pageIndexedDrawables.find(toPage)->second){
            drawable->setVisible(true);
        }
    }
}

void DrawableHolderPageable::removeAllPageableDrawables() {
    for(const auto& page:pageIndexedDrawables){
        for (const auto& findDrawable:page.second) {
            int searchIndex=0;
            for(const auto& drawableByPriority:prioritySortedDrawables) {
                if (drawableByPriority->getDrawableId() == findDrawable->getDrawableId()){
                    prioritySortedDrawables.erase(prioritySortedDrawables.begin()+searchIndex);
                    break;
                }
                searchIndex++;
            }
            nameIndexedDrawables.erase(findDrawable->getDrawableId());
        }
    }
    pageIndexedDrawables.clear();
}

vector<shared_ptr<Drawable>>::iterator DrawableHolderPageable::beginOfPageDrawables(string &name) {
    return pageIndexedDrawables.find(name)->second.begin();
}

vector<shared_ptr<Drawable>>::iterator DrawableHolderPageable::endOfPageDrawables(string &name) {
    return pageIndexedDrawables.find(name)->second.end();
}
