//
// Created by voky on 24.03.22.
//

#include "DrawableHolder.h"
#include "../exceptions/DuplicateOfDrawableIdException.h"

bool DrawableHolder::findDrawable(const string &drawableId, shared_ptr<Drawable> &r) {
    if (nameIndexedDrawables.find(drawableId) == nameIndexedDrawables.end()) {
        return false;
    } else {
        r= nameIndexedDrawables.find(drawableId)->second;
        return true;
    }
}

Bitmap &DrawableHolder::getAggregatedBitmap() {
    // Movable Drawables (for examples ScrollableLayout) will leave its trail in Bitmap, so it has to be cleaned and refreshed
    agregatedBitmap.erase();
    for (const auto &pair: nameIndexedDrawables) {
        shared_ptr<Drawable> drawable = pair.second;
        Bitmap drawableBitmap = drawable->getBitmap();
        if(!drawable->isVisible()){
            continue;
        }

        int posX = drawable->getRelativePosition().getX();
        int posY = drawable->getRelativePosition().getY();

        for (int j = 0; j < drawableBitmap.getHeight(); j++) {
            for (int i = 0; i < drawableBitmap.getWidth(); i++) {
                // Dont overwrite by not set fields, make layout transparent
                if(drawableBitmap.read(i, j)!=0) {
                    agregatedBitmap.write(posX + i, posY + j, drawableBitmap.read(i, j));
                }
            }
        }
    }
    return agregatedBitmap;
}

void DrawableHolder::addDrawable(const shared_ptr<Drawable> &d) {
    if (nameIndexedDrawables.find(d->getDrawableId()) != nameIndexedDrawables.end()) {
        throw DuplicateOfDrawableIdException();
    }
    prioritySortedDrawables.push_back(d);
    nameIndexedDrawables.insert(pair<string, shared_ptr<Drawable>>(d->getDrawableId(), d));
}

void DrawableHolder::removeAllDrawablesInLayout() {
    prioritySortedDrawables.clear();
    nameIndexedDrawables.clear();
}

void DrawableHolder::removeDrawablesOfIdStartingAs(const string &perex) {
    for(size_t i=0;i<prioritySortedDrawables.size();i++){
        if(prioritySortedDrawables[i]->getDrawableId().substr(0,perex.size())==perex){
            prioritySortedDrawables.erase(prioritySortedDrawables.begin()+i);
        }
    }
    for(const auto& d : nameIndexedDrawables){
        if(d.second->getDrawableId().substr(0,perex.size())==perex) {
            nameIndexedDrawables.erase(d.second->getDrawableId());
        }
    }
}
