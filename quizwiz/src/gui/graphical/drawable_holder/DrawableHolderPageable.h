//
// Created by voky on 30.03.22.
//

#ifndef QUIZWIZ_DRAWABLEHOLDERPAGEABLE_H
#define QUIZWIZ_DRAWABLEHOLDERPAGEABLE_H


#include <iostream>
#include "DrawableHolder.h"

/**
 * Holds Drawables grouped in pages. Could be switched between pages to de/active whole page of Drawables.
 */
class DrawableHolderPageable: public DrawableHolder {
    string activePageId;
    map<string,vector<shared_ptr<Drawable>>> pageIndexedDrawables;

public:
    /**
     * Adds Drawable to Holder, indexed by page.
     * @attention Does not override not pageable addDrawable, its just overload. Without @param page set, holder will add Drawable in default way and Drawable wont belong to any page.
     * @param page name of page Drawable belong to
     * @param d shared pointer to Drawable
     */
    void addDrawable(const string& page,const shared_ptr<Drawable> &d);

    /**
     * Set all Drawables of given page visible. Other, indexed by Page will set invisible.
     * @param toPage page name to set active
     */
    void setPageActive(string& toPage);

    void removeAllPageableDrawables();

    vector<shared_ptr<Drawable>>::iterator beginOfPageDrawables(string& name);
    vector<shared_ptr<Drawable>>::iterator endOfPageDrawables(string& name);
};


#endif //QUIZWIZ_DRAWABLEHOLDERPAGEABLE_H
