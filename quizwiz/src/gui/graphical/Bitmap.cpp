//
// Created by voky on 28.03.22.
//

#include "Bitmap.h"
#include "exceptions/BitmapWidthHeightException.h"

void Bitmap::initialize() {
    vector<char> line;
    line.push_back(0);
    byteField.push_back(line);
    height=1;
    width=1;
}

void Bitmap::cropTop(Bitmap &btm, int minus) {
    if(btm.height<minus){
        // Nulova bitmapa
        btm = Bitmap();
    }else{
        btm.byteField.erase(btm.byteField.begin(), btm.byteField.begin() + minus);
        btm.height-=minus;
    }
}

void Bitmap::erase() {
    for(int i =0;i<height;i++){
        for(int j=0;j<width;j++){
            byteField[i][j]=0;
        }
    }
}

char Bitmap::read(int X, int Y) {
    if(X>=width || Y>=height){
        throw BitmapWidthHeightException();
    } else{
        return byteField[Y][X];
    }
}

void Bitmap::write(const int &X, const int &Y, const char &c) {
    if(height==0||width==0){
        initialize();
    }

    if (X > width - 1) {
        for (int i = 0; i < height; i++) {
            byteField[i].resize(X + 1);
        }
        width = X + 1;

    }
    if (Y > height - 1) {
        byteField.resize(Y + 1);
        for (int i = height; i < Y + 1; i++) {
            vector<char> line;
            line.resize(width);
            byteField[i]=(line);
        }

        height = Y + 1;
    }
    byteField[Y][X] = c;
}

int Bitmap::getHeight() const {
    return height;
}

int Bitmap::getWidth() const {
    return width;
}

void Bitmap::setHeight(int h) {
    Bitmap::height = h;
}
