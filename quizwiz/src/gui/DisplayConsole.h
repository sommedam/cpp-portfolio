//
// Created by voky on 29.03.22.
//

#ifndef QUIZWIZ_DISPLAYCONSOLE_H
#define QUIZWIZ_DISPLAYCONSOLE_H

#include <iostream>
#include <cmath>
#include <chrono>
#include <thread>
#include "graphical/Bitmap.h"

using namespace std;

/**
 * Class managing writing of Bitmaps to console
 * @attention only single object at a time should access this class, to prevent unexpected console behaviour
 */
class DisplayConsole {
    static const int width=100; /* width of application window in console */
    static const int height=20; /* height of application window in console */
    static const int terminalHeight=100; /* number of newlines to clear the terminal */

    static const char horizontal='_';
    static const char vertical='|';
    static const char eter=' ';

    static void clear() {
        std::cout << string(terminalHeight, '\n');
    }

public:
    /*
     * @return y position of last line displayable in Bitmap
     */
    static int bottomPosition();
    static void displayMessage(const string& message);
    static void displayBitmap(Bitmap &b, string additionalMessage);
    static void displayBitmap(Bitmap &b);
    static void displayBitmapBlank();
};


#endif //QUIZWIZ_DISPLAYCONSOLE_H
