//
// Created by voky on 29.03.22.
//

#include "DisplayConsole.h"

void DisplayConsole::displayMessage(const string &message) {
    Bitmap blank;
    int startingX = (width/2)-(message.size()/2);
    for(auto character : message){
        blank.write(startingX++,(height-4)/2,character);
    }

    displayBitmap(blank);
}

void DisplayConsole::displayBitmap(Bitmap &b, string additionalMessage) {
    displayBitmap(b);
    cout << endl << additionalMessage;
}

void DisplayConsole::displayBitmap(Bitmap &b) {
    clear();
    std::cout << string(width, horizontal);
    cout << endl;
    for (int i = 0; i < height - 2; i++) {
        cout << vertical;
        if (i < b.getHeight()) {
            for (int j = 0; j < min(width - 2, b.getWidth()); j++) {
                if(b.read(j, i)!=0){
                    cout << b.read(j, i);
                }else{
                    cout<<eter;
                }
            };
            // Doplnit chybejici sirku
            if (b.getWidth() < width - 2)
                std::cout << string((width - 2) - b.getWidth(), eter);
        } else {
            std::cout << string(width - 2, eter);
        }
        cout << vertical<<endl;
    }
    std::cout << string(width, horizontal)<<endl;
}

void DisplayConsole::displayBitmapBlank() {
    clear();
    std::cout << string(width, horizontal);
    cout << endl;
    for (int i = 0; i < height - 2; i++) {
        cout << vertical;
        std::cout << string(width - 2, eter);
        cout << vertical << endl;
    }
    std::cout << string(width, horizontal)<<endl;
}

int DisplayConsole::bottomPosition() {return height-3;}
