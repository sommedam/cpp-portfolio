//
// Created by voky on 23.5.22.
//

#include "TextDrawableTest.h"

void TextDrawableTest::autoTest() {
    int callbackCount = 0;
    Position pos1(11, 22);
    std::string innerText1 = "Ahoj svete";
    TextDrawable text1("text1", pos1, ([&callbackCount]() {
        callbackCount = callbackCount + 1;
    }));
    text1.setText(innerText1);
    assert(abs(text1.getWidth()) == innerText1.length());
    assert(callbackCount == 1);
    assert(text1.getRelativePosition().getX()==11);
    assert(text1.getRelativePosition().getY()==22);
    shared_ptr<TextDrawable> sharedPtr = make_shared<TextDrawable>(text1);
    assert(sharedPtr->getRelativePosition().getX()==11);
    assert(sharedPtr->getRelativePosition().getY()==22);
    assert(abs(sharedPtr->getWidth()) == innerText1.length());
}
