//
// Created by voky on 23.5.22.
//

#ifndef QUIZWIZ_TEXTDRAWABLETEST_H
#define QUIZWIZ_TEXTDRAWABLETEST_H


#include <cassert>
#include "../../gui/Position.h"
#include "../../gui/graphical/drawable/TextDrawable.h"

class TextDrawableTest {
public:
        static void autoTest();
};


#endif //QUIZWIZ_TEXTDRAWABLETEST_H
