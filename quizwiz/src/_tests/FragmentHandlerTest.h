//
// Created by voky on 24.5.22.
//

#ifndef QUIZWIZ_FRAGMENTHANDLERTEST_H
#define QUIZWIZ_FRAGMENTHANDLERTEST_H


#include <cassert>
#include "../gui/navigation/fragment/project_fragments/MainMenuFragment.h"
#include "../gui/navigation/fragment/FragmentHandler.h"

class FragmentHandlerTest {
public:
    static void manualTest();
};


#endif //QUIZWIZ_FRAGMENTHANDLERTEST_H
