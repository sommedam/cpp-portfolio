//
// Created by voky on 24.5.22.
//

#include "FragmentHandlerTest.h"

void FragmentHandlerTest::manualTest() {
    shared_ptr<MainMenuFragment> mainMenuFragment = make_shared<MainMenuFragment>();

    //shared_ptr<QuizFragment> quizFragment = make_shared<QuizFragment>();
    vector<shared_ptr<Fragment>> fragments;
    //fragment.push_back(quizFragment);
    fragments.push_back(mainMenuFragment);
    string defaultFragment = mainMenuFragment->NAME();
    FragmentHandler fh(defaultFragment,fragments);

    assert(fh.getCurrentFragment()->NAME()==defaultFragment);
    //string quizFragmentName =quizFragment->NAME();
    FragmentExchangeBundle quizExchange;
    //fh.onFragmentChange(quizFragmentName,quizExchange);
    //assert(fh.getCurrentFragment()->NAME()==quizFragmentName);
    // Nefunguje
    fh.endInputLoop();

}
