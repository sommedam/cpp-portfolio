//
// Created by voky on 23.03.22.
//

#ifndef QUIZWIZ_TEXTANSWERTEST_H
#define QUIZWIZ_TEXTANSWERTEST_H


#include <iostream>
#include <cassert>
#include <memory>
#include <vector>
#include <set>
#include "../../quiz_containers/answer/text_field_answer/TextFieldAnswer.h"
#include "../../quiz_containers/answer/text_field_answer/TextStrictAnswer.h"
#include "../../quiz_containers/answer/number_field_answer/NumberIntervalAnswer.h"
#include "../../quiz_containers/answer/text_field_answer/TextSetAnswer.h"

class TextAnswerTest {
public:
    static void autoTest();
};


#endif //QUIZWIZ_TEXTANSWERTEST_H
