//
// Created by voky on 1.6.22.
//

#ifndef QUIZWIZ_TEXTMISSPELLEDANSWERTEST_H
#define QUIZWIZ_TEXTMISSPELLEDANSWERTEST_H


class TextMisspelledAnswerTest {
    public:
        static void autoTest();

        static void manualTest();

    private:
        static void inputStreamTest();

        static void userInput();

        static void answerCreatorTest();
};


#endif //QUIZWIZ_TEXTMISSPELLEDANSWERTEST_H
