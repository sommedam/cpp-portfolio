//
// Created by voky on 13.04.22.
//

#include "MultichoiceAnswerTest.h"

void MultichoiceAnswerTest::autoTest() {
    inputStreamTest();
    userInput();
}

void MultichoiceAnswerTest::manualTest() {
    answerCreatorTest();
}

void MultichoiceAnswerTest::inputStreamTest() {
    MultichoiceAnswer multichoiceAnswer;
    try {
        string correctAnswer =
                R"(true{" spravne1 "," spravne2 "}false{" nespravne1 "," nespravne2 "})";
        istringstream iss(correctAnswer);
        string correctAnswer2 =
                R"(false{}true{" spravne1 "," spravne2 "})";
        istringstream iss2(correctAnswer2);
        iss >> multichoiceAnswer;
    }catch (AnswerInputStreamException & exception){
        assert(0);
    }
}

void MultichoiceAnswerTest::userInput() {
    vector<pair<bool, string>> options;
    options.emplace_back(true, "Lachtan");
    options.emplace_back(false, "Anakonda");
    options.emplace_back(true, "Wombat");
    MultichoiceAnswer multichoiceAnswer(options);

    auto keys = multichoiceAnswer.generateKeyMappedOptions();
    string correctInput;
    for (const auto &k: keys) {
        // Pokud je odpoved spravna tak jeji klic dat do stringu spravne odpovedi
        if (k.second.first) {
            char keyChar = k.first;
            correctInput += keyChar;
            correctInput += ", ";
        }
    }
    multichoiceAnswer.setInput(correctInput);
    assert(multichoiceAnswer.correct());
    // Nebyly oznaceny spravne vsechny
    string incorrectIncomplete = "a";
    multichoiceAnswer.setInput(incorrectIncomplete);
    assert(!multichoiceAnswer.correct());
    // Byly oznaceny i nespravne odpovedi
    string incorrectMoreCharacters = "a,b,c";
    multichoiceAnswer.setInput(incorrectMoreCharacters);
    assert(!multichoiceAnswer.correct());
}

void MultichoiceAnswerTest::answerCreatorTest() {
    MultichoiceAnswer multichoiceAnswer;
    auto a = multichoiceAnswer.answerCreatorWizard();
    cout<<a;
}
