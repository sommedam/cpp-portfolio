//
// Created by voky on 23.5.22.
//

#ifndef QUIZWIZ_NUMBERINTERVALANSWERTEST_H
#define QUIZWIZ_NUMBERINTERVALANSWERTEST_H

#include <cassert>
#include "../../quiz_containers/answer/number_field_answer/NumberIntervalAnswer.h"
#include "../../quiz_containers/answer/exceptions/AnswerInputStreamException.h"

using namespace std;
class NumberIntervalAnswerTest {
public:
    static void autoTest();

    static void manualTest();

private:
    static void inputStreamTest();

    static void userInput();

    static void answerCreatorTest();
};


#endif //QUIZWIZ_NUMBERINTERVALANSWERTEST_H
