//
// Created by voky on 23.03.22.
//

#include "TextAnswerTest.h"

void TextAnswerTest::autoTest() {
    vector<shared_ptr<TextFieldAnswer>> correctAnswers;
    correctAnswers.push_back(make_shared<TextStrictAnswer>("correct"));
    correctAnswers[0]->setInput("correct");
    correctAnswers.push_back(make_shared<TextStrictAnswer>("correct"));
    correctAnswers[1]->setInput("correct");

    for(const auto& correct : correctAnswers){
        assert(correct->correct());
    }
}
