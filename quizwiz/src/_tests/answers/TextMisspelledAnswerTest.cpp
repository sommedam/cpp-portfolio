//
// Created by voky on 1.6.22.
//

#include <cassert>
#include <sstream>
#include "TextMisspelledAnswerTest.h"
#include "../../quiz_containers/answer/text_field_answer/TextMisspelledAnswer.h"
#include "../../quiz_containers/answer/exceptions/AnswerInputStreamException.h"

void TextMisspelledAnswerTest::autoTest() {
    inputStreamTest();
    userInput();
}

void TextMisspelledAnswerTest::manualTest() {
    answerCreatorTest();
}

void TextMisspelledAnswerTest::inputStreamTest() {
    TextMisspelledAnswer textMisspelledAnswer;
    try {
        string correctAnswer ="Odpoved s preklepem;5";
        istringstream iss(correctAnswer);
        iss >> textMisspelledAnswer;
        string correctAnswer2 ="Odpoved s preklepem2;3";
        istringstream iss2(correctAnswer2);
        iss2 >> textMisspelledAnswer;
    }catch (AnswerInputStreamException & exception){
        assert(0);
    }
}

void TextMisspelledAnswerTest::userInput() {
    string correctAnswer ="Mississippi;3";
    istringstream iss(correctAnswer);
    TextMisspelledAnswer textMisspelledAnswer;
    iss>>textMisspelledAnswer;
    textMisspelledAnswer.setInput("Mississippi");
    assert(textMisspelledAnswer.correct());

    textMisspelledAnswer.setInput("MississiOOO");
    assert(textMisspelledAnswer.correct());
    textMisspelledAnswer.setInput("Mississipp");
    assert(!textMisspelledAnswer.correct());

    textMisspelledAnswer.setInput("");
    assert(!textMisspelledAnswer.correct());
}

void TextMisspelledAnswerTest::answerCreatorTest() {
    TextMisspelledAnswer textMisspelledAnswer;
    auto a = textMisspelledAnswer.answerCreatorWizard();
    cout<<a;
}
