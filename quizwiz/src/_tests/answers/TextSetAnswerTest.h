//
// Created by voky on 23.5.22.
//

#ifndef QUIZWIZ_TEXTSETANSWERTEST_H
#define QUIZWIZ_TEXTSETANSWERTEST_H

#include <sstream>
#include <cassert>
#include "istream"
#include "../../quiz_containers/answer/text_field_answer/TextSetAnswer.h"
#include "../../quiz_containers/answer/exceptions/AnswerInputStreamException.h"

using namespace std;

class TextSetAnswerTest {
public:
    static void autoTest();

    static void manualTest();

private:
    static void inputStreamTest();

    static void userInput();

    static void answerCreatorTest();
};


#endif //QUIZWIZ_TEXTSETANSWERTEST_H
