//
// Created by voky on 13.04.22.
//

#ifndef QUIZWIZ_MULTICHOICEANSWERTEST_H
#define QUIZWIZ_MULTICHOICEANSWERTEST_H


#include <istream>
#include <sstream>
#include <string>
#include <cassert>
#include "../../quiz_containers/answer/choice_answer/MultichoiceAnswer.h"
#include "../../quiz_containers/answer/exceptions/AnswerInputStreamException.h"

using namespace std;


class MultichoiceAnswerTest {
public:
    static void autoTest();

    static void manualTest();

private:
    static void inputStreamTest();

    static void userInput();

    static void answerCreatorTest();
};


#endif //QUIZWIZ_MULTICHOICEANSWERTEST_H
