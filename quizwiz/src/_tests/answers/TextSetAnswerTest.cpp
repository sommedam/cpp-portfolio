//
// Created by voky on 23.5.22.
//

#include "TextSetAnswerTest.h"

void TextSetAnswerTest::autoTest() {
    inputStreamTest();
    userInput();
}

void TextSetAnswerTest::manualTest() {
    answerCreatorTest();
}

void TextSetAnswerTest::inputStreamTest() {
    TextSetAnswer textSetAnswer;
    try {
        string correctAnswer ="spravna odpoved 1;spravna odpoved 2; spravna odpoved 3;";
        istringstream iss(correctAnswer);
        iss >> textSetAnswer;
        string correctAnswer2 ="spravna odpoved 1";
        istringstream iss2(correctAnswer2);
        iss >> textSetAnswer;
    }catch (AnswerInputStreamException & exception){
        assert(0);
    }
}

void TextSetAnswerTest::userInput() {
    TextSetAnswer textSetAnswer;
    textSetAnswer.addToOptions("spravne 1");
    textSetAnswer.addToOptions("spravne 2");

    textSetAnswer.setInput("spravne 1");
    assert(textSetAnswer.correct());
    textSetAnswer.setInput("spravne 2");
    assert(textSetAnswer.correct());

    textSetAnswer.setInput("");
    assert(!textSetAnswer.correct());
    textSetAnswer.setInput("nespravne");
    assert(!textSetAnswer.correct());
}

void TextSetAnswerTest::answerCreatorTest() {
    TextSetAnswer textSetAnswer;
    auto a = textSetAnswer.answerCreatorWizard();
    cout<<a;
}
