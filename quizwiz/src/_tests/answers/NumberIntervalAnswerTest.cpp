//
// Created by voky on 23.5.22.
//

#include "NumberIntervalAnswerTest.h"

void NumberIntervalAnswerTest::autoTest() {
    inputStreamTest();
    userInput();
}

void NumberIntervalAnswerTest::manualTest() {
    answerCreatorTest();
}

void NumberIntervalAnswerTest::inputStreamTest() {
    NumberIntervalAnswer textIntervalAnswer;
    try {
        string correctAnswer ="10;20";
        istringstream iss1(correctAnswer);
        iss1 >> textIntervalAnswer;
        string correctAnswer2 ="-10;5";
        istringstream iss2(correctAnswer2);
        iss2 >> textIntervalAnswer;
    }catch (AnswerInputStreamException & exception){
        assert(0);
    }
    try {
        string correctAnswer ="1;-1";
        istringstream iss3(correctAnswer);
        iss3 >> textIntervalAnswer;
    }catch (AnswerInputStreamException & exception){
        // Incorrect interval
    }
}

void NumberIntervalAnswerTest::userInput() {
    NumberIntervalAnswer textIntervalAnswer;
    textIntervalAnswer.setInterval(pair<int,int>(-10,10));

    textIntervalAnswer.setInput("0");
    assert(textIntervalAnswer.correct());
    textIntervalAnswer.setInput("-10");
    assert(textIntervalAnswer.correct());
    textIntervalAnswer.setInput("10");
    assert(textIntervalAnswer.correct());
    textIntervalAnswer.setInput("999");
    assert(!textIntervalAnswer.correct());

    try {
        textIntervalAnswer.setInput("");
        assert(false);
    }catch (AnswerUserInputException & e){
    }
    try {
        textIntervalAnswer.setInput("a b");
        assert(false);
    }catch (AnswerUserInputException & e){
    }
}

void NumberIntervalAnswerTest::answerCreatorTest() {
    NumberIntervalAnswer textIntervalAnswer;
    auto a = textIntervalAnswer.answerCreatorWizard();
    cout<<a;
}
