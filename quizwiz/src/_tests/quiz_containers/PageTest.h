//
// Created by voky on 23.5.22.
//

#ifndef QUIZWIZ_PAGETEST_H
#define QUIZWIZ_PAGETEST_H


#include <cassert>
#include "../../quiz_containers/page/Page.h"
#include "../../quiz_containers/answer/text_field_answer/TextStrictAnswer.h"

class PageTest {
public:
    static void autoTest();

    static void pageCorrectTest();
};


#endif //QUIZWIZ_PAGETEST_H
