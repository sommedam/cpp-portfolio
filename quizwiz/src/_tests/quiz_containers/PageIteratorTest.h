//
// Created by voky on 23.5.22.
//

#ifndef QUIZWIZ_PAGEITERATORTEST_H
#define QUIZWIZ_PAGEITERATORTEST_H


#include <cassert>
#include "../../quiz_containers/page/page_policy/PagePolicyBuilder.h"
#include "../../quiz_containers/answer/text_field_answer/TextStrictAnswer.h"
#include "../../quiz_containers/Question.h"
#include "../../quiz_containers/page/Page.h"
#include "../../quiz_containers/page/page_policy/next_page_policies/BlockPagePolicy.h"
#include "../../quiz_containers/page/page_policy/next_page_policies/MessagePagePolicy.h"
#include "../../quiz_containers/Quiz.h"

class PageIteratorTest {
public:
    static void autoTest();

    static void pagePoliciesTest();

    static void threeCorrectPages();

    static void correctIncorrectCorrectPages();
};


#endif //QUIZWIZ_PAGEITERATORTEST_H
