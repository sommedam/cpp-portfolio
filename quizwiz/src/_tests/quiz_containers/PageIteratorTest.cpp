//
// Created by voky on 23.5.22.
//

#include "PageIteratorTest.h"

void PageIteratorTest::autoTest() {
    pagePoliciesTest();
    threeCorrectPages();
    correctIncorrectCorrectPages();
}

void PageIteratorTest::pagePoliciesTest() {
    int blockCount=0; // block policy count
    int messageCount=0; // message policy count
    PagePolicyBuilder pagePolicyBuilder;
    pagePolicyBuilder.BlockFlagListener([&blockCount](int p) {
        blockCount++;
    });
    pagePolicyBuilder.MessageFlagListener([&messageCount](const string &p) {
        messageCount++;
        assert(p == "message"); /* check if bundle match */
    });

    // one answer per page
    auto blockPageAnswer1 = make_shared<TextStrictAnswer>("correct");
    auto messagePageAnswer2 = make_shared<TextStrictAnswer>("correct");
    auto correctAnswer3 = make_shared<TextStrictAnswer>("correct");
    // tests BlockPolicy
    blockPageAnswer1->setInput("_incorrect_");
    // tests MessagePolicy
    messagePageAnswer2->setInput("_incorrect_");
    // tests end flag of PageIterator
    correctAnswer3->setInput("correct");

    auto q1 = Question("question", blockPageAnswer1);
    auto q2 = Question("question2", messagePageAnswer2);
    auto q3 = Question("question3", correctAnswer3);

    auto p = make_shared<Page>("Strana 1");
    auto block = make_shared<BlockPagePolicy>();
    p->setPolicy(dynamic_pointer_cast<PagePolicyWrapper>(block));

    auto p2 = make_shared<Page>("Strana 2");
    auto message = make_shared<MessagePagePolicy>();
    message->setMessageToDisplay("message");
    p2->setPolicy(dynamic_pointer_cast<PagePolicyWrapper>(message));

    auto p3 = make_shared<Page>("Strana 3");

    vector<Question> questions1;
    vector<Question> questions2;
    vector<Question> questions3;

    questions1.push_back(q1);
    questions2.push_back(q2);
    questions3.push_back(q3);

    p->setQuestions(questions1);
    p2->setQuestions(questions2);
    p3->setQuestions(questions3);

    vector<shared_ptr<Page>> pages;
    pages.push_back(p);
    pages.push_back(p2);
    pages.push_back(p3);

    Quiz q(pages);

    PageIterator pageIterator = q.getPageIterator(pagePolicyBuilder);

    // 0 -> 1 ERROR
    assert(pageIterator.nextPage() == PageIterator::NEXT_PAGE_ABORT);
    assert(pageIterator.nextPage() == PageIterator::NEXT_PAGE_ABORT);
    assert(blockCount == 2);
    questions1.at(0).getAnswer()->setInput("correct");
    // 0 -> 1 OK
    assert(pageIterator.nextPage() == PageIterator::NEXT_PAGE_OK);
    // 1 -> 2 OK
    assert(pageIterator.nextPage() == PageIterator::NEXT_PAGE_ABORT);
    assert(messageCount == 1);
    questions2.at(0).getAnswer()->setInput("correct");
    // 2 -> END
    assert(pageIterator.getCurrentPage().operator*()->getPageName()=="Strana 2");
    assert(pageIterator.nextPage() == PageIterator::NEXT_PAGE_OK);
    assert(pageIterator.nextPage() == PageIterator::NEXT_PAGE_END);
}

void PageIteratorTest::threeCorrectPages() {
    auto correctAnswer1 = make_shared<TextStrictAnswer>("correct");
    auto correctAnswer2 = make_shared<TextStrictAnswer>("correct");
    auto correctAnswer3 = make_shared<TextStrictAnswer>("correct");
    correctAnswer1->setInput("correct");
    correctAnswer2->setInput("correct");
    correctAnswer3->setInput("correct");

    auto q1 = Question("question", correctAnswer1);
    auto q2 = Question("question2", correctAnswer2);
    auto q3 = Question("question3", correctAnswer3);
    auto p = make_shared<Page>("Strana 1");
    auto p2 = make_shared<Page>("Strana 2");
    auto p3 = make_shared<Page>("Strana 3");

    vector<Question> questions1;
    vector<Question> questions2;
    vector<Question> questions3;

    questions1.push_back(q1);
    questions2.push_back(q2);
    questions3.push_back(q3);
    p->setQuestions(questions1);
    p2->setQuestions(questions2);
    p3->setQuestions(questions3);

    vector<shared_ptr<Page>> pages;
    pages.push_back(p);
    pages.push_back(p2);
    pages.push_back(p3);

    Quiz q(pages);
    PageIterator pageIterator = q.getPageIterator();

    assert(pageIterator.nextPage()==PageIterator::NEXT_PAGE_OK);
    assert(pageIterator.nextPage()==PageIterator::NEXT_PAGE_OK);
    assert(pageIterator.nextPage()==PageIterator::NEXT_PAGE_END);
}

void PageIteratorTest::correctIncorrectCorrectPages() {
    auto correctAnswer1 = make_shared<TextStrictAnswer>("correct");
    auto incorrectAnswer2 = make_shared<TextStrictAnswer>("incorrect");
    auto correctAnswer3 = make_shared<TextStrictAnswer>("correct");
    correctAnswer1->setInput("correct");
    incorrectAnswer2->setInput("_incorrect_");
    correctAnswer3->setInput("correct");

    auto q1 = Question("question", correctAnswer1);
    auto q2 = Question("question2", incorrectAnswer2);
    auto q3 = Question("question3", correctAnswer3);
    auto p = make_shared<Page>("Strana 1");
    auto p2 = make_shared<Page>("Strana 2");
    auto p3 = make_shared<Page>("Strana 3");

    vector<Question> questions1;
    vector<Question> questions2;
    vector<Question> questions3;

    questions1.push_back(q1);
    questions2.push_back(q2);
    questions3.push_back(q3);
    p->setQuestions(questions1);
    p2->setQuestions(questions2);
    p3->setQuestions(questions3);

    vector<shared_ptr<Page>> pages;
    pages.push_back(p);
    pages.push_back(p2);
    pages.push_back(p3);

    Quiz q(pages);
    PageIterator pageIterator = q.getPageIterator();

    assert(pageIterator.nextPage()==PageIterator::NEXT_PAGE_OK);
    assert(pageIterator.nextPage()==PageIterator::NEXT_PAGE_OK);
    assert(pageIterator.nextPage()==PageIterator::NEXT_PAGE_END);
}
