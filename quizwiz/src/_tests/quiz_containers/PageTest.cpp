//
// Created by voky on 23.5.22.
//

#include "PageTest.h"

void PageTest::autoTest() {
    pageCorrectTest();
}

void PageTest::pageCorrectTest() {
    Page p("Strana 1");

    auto correctAnswer1 = make_shared<TextStrictAnswer>("correct");
    auto correctAnswer2 = make_shared<TextStrictAnswer>("correct");
    auto incorrectAnswer1 = make_shared<TextStrictAnswer>("incorrect");
    correctAnswer1->setInput("correct");
    correctAnswer2->setInput("correct");
    incorrectAnswer1->setInput("_incorrect_");
    assert(!incorrectAnswer1->correct()); // Nepatri do jednotkoveho testu
    auto q1 = Question("question",correctAnswer1);
    auto q2 = Question("question2",correctAnswer2);
    auto q3 = Question("question3", incorrectAnswer1);
    assert(!q3.correct());
    vector<Question> questions1;
    vector<Question> questions2;
    questions1.push_back(q1);
    questions1.push_back(q2);
    p.setQuestions(questions1);
    assert(p.correct());
    questions2.push_back(q3);
    p.setQuestions(questions2);
    assert(!p.correct());
}
