//
// Created by voky on 23.5.22.
//

#ifndef QUIZWIZ_SCROLLABLELAYOUTTEST_H
#define QUIZWIZ_SCROLLABLELAYOUTTEST_H


#include "../../gui/graphical/drawable/layout/Layout.h"
#include "../../gui/graphical/drawable/layout/ScrollableLayout.h"
#include "../../gui/graphical/drawable/TextDrawable.h"
#include "../../gui/DisplayConsole.h"
using namespace std;
class ScrollableLayoutTest {
public:
    static void manualTest();
};


#endif //QUIZWIZ_SCROLLABLELAYOUTTEST_H
