//
// Created by voky on 23.5.22.
//

#include "DrawableHolderTest.h"

void DrawableHolderTest::findReadDrawableTest() {
    DrawableHolder dh;
    string textId1 = "1";
    string textId2 = "2";
    string textInner1 = "Ahoj svete";
    string textInner2 = "Ahoj svete2";
    Position pos1(0, 0);
    Position pos2(1, 1);
    auto text1 = make_shared<TextDrawable>(textId1, pos1, [] {});
    auto text2 = make_shared<TextDrawable>(textId2, pos2, [] {});
    text1->setText(textInner1);
    text2->setText(textInner2);
    dh.addDrawable(text1);
    dh.addDrawable(text2);

    Bitmap agregated = dh.getAggregatedBitmap();
    assert(agregated.getHeight()==2);
    assert(agregated.getWidth()==12);
    for(size_t i =0; i<textInner1.size();i++){
        assert(agregated.read(i,0)==textInner1[i]);
    }
    for(size_t i =1; i<textInner2.size()+1;i++){
        assert(agregated.read(i,1)==textInner2[i-1]);
    }

    shared_ptr<Drawable> r;
    assert(dh.findDrawable("1",r)&&r->getDrawableId()=="1");
    assert(dh.findDrawable("2",r)&&r->getDrawableId()=="2");
}

void DrawableHolderTest::autoTest() {
    findReadDrawableTest();
}
