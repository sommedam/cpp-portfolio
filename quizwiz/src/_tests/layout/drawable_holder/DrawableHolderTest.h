//
// Created by voky on 23.5.22.
//

#ifndef QUIZWIZ_DRAWABLEHOLDERTEST_H
#define QUIZWIZ_DRAWABLEHOLDERTEST_H


#include <cassert>
#include "../../../gui/graphical/drawable_holder/DrawableHolder.h"
#include "../../../gui/graphical/drawable/TextDrawable.h"

class DrawableHolderTest {
public:
    static void autoTest();
    static void findReadDrawableTest();
};


#endif //QUIZWIZ_DRAWABLEHOLDERTEST_H
