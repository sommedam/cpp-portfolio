//
// Created by voky on 23.5.22.
//

#ifndef QUIZWIZ_DRAWABLEHOLDERPAGEABLETEST_H
#define QUIZWIZ_DRAWABLEHOLDERPAGEABLETEST_H


#include <cassert>
#include "../../../gui/graphical/drawable_holder/DrawableHolderPageable.h"
#include "../../../gui/graphical/drawable/TextDrawable.h"
#include "../../../gui/DisplayConsole.h"

class DrawableHolderPageableTest {
public:
    static void autoTest();
    static void manualTest();
private:
    static void setPageActiveTest();

   static void inheritanceUsageTest();
};


#endif //QUIZWIZ_DRAWABLEHOLDERPAGEABLETEST_H
