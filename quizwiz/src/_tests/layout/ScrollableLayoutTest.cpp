//
// Created by voky on 23.5.22.
//

#include "ScrollableLayoutTest.h"

void ScrollableLayoutTest::manualTest() {
    auto outterLayout = make_shared<Layout>("l1", Position(0, 0), []() {});
    auto scrollableLayout = make_shared<ScrollableLayout>("l2", Position(0, 10), []() {});
    shared_ptr<Layout> l = std::static_pointer_cast<Layout>(scrollableLayout);
    outterLayout->addInnerLayout(l);
    //outterLayout->addInnerLayout(scrollableLayout);

    string scrollableText = "UP DOWN";
    auto textInScrollable = make_shared<TextDrawable>(scrollableText, Position(0, 0), []() {});
    shared_ptr<Drawable> d = std::static_pointer_cast<Drawable>(textInScrollable);

    scrollableLayout->addDrawable(d);

    textInScrollable->setText(scrollableText);

    using namespace this_thread; // sleep_for, sleep_until
    using namespace chrono; // nanoseconds, system_clock, seconds
    // UP
    for (int i = 0; i < 12; i++) {
        DisplayConsole::displayBitmap(outterLayout->getBitmap());

        cout << "scrollable height: " << scrollableLayout->getHeight() << endl;
        cout << "scrollable position Y: " << scrollableLayout->getRelativePosition().getY() << endl;
        sleep_for(milliseconds (500));
        scrollableLayout->scrollDown();
    }
    // DOWN
    for (int i = 0; i < 22; i++) {
        DisplayConsole::displayBitmap(outterLayout->getBitmap());

        cout << "scrollable height: " << scrollableLayout->getHeight() << endl;
        cout << "scrollable position Y: " << scrollableLayout->getRelativePosition().getY() << endl;
        sleep_for(milliseconds (500));
        scrollableLayout->scrollUp();
    }
}
