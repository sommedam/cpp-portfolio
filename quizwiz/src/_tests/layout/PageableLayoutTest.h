
//
// Created by voky on 23.5.22.
//

#ifndef QUIZWIZ_PAGEABLELAYOUTTEST_H
    #define QUIZWIZ_PAGEABLELAYOUTTEST_H


#include "../../gui/DisplayConsole.h"
#include "../../gui/graphical/drawable/TextDrawable.h"
#include "../../gui/graphical/drawable/layout/Layout.h"
#include "../../gui/graphical/drawable/layout/PageableLayout.h"

class PageableLayoutTest {
public:
    static void manualTest();
};


#endif //QUIZWIZ_PAGEABLELAYOUTTEST_H
