//
// Created by voky on 23.5.22.
//

#ifndef QUIZWIZ_LAYOUTTEST_H
#define QUIZWIZ_LAYOUTTEST_H


#include <cassert>
#include "../../gui/graphical/drawable/layout/Layout.h"
#include "../../gui/DisplayConsole.h"
#include "../../gui/graphical/drawable/TextDrawable.h"

class LayoutTest {
public:
    static void autoTest();

    static void manualTest();
private:
    static void innerLayoutsPerformanceTest();

    static void inlinePerformanceTest();

    static void transparencyTest();

    static void findDrawableApplyDynamicCastTest();

    static void recursiveSearchTest();
};


#endif //QUIZWIZ_LAYOUTTEST_H
