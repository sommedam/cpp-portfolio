//
// Created by voky on 23.5.22.
//

#include "LayoutTest.h"

void LayoutTest::recursiveSearchTest() {
    auto l1 = make_shared<Layout>("l1", Position(0, 0), []() {});
    auto l2 = make_shared<Layout>("l2", Position(0, 0), []() {});
    auto l3 = make_shared<Layout>("l3", Position(0, 0), []() {});

    l1->addInnerLayout(l2);
    l2->addInnerLayout(l3);

    string drawableId = "text";
    auto text = make_shared<TextDrawable>(drawableId, Position(0, 0), [](){});
    shared_ptr<Drawable> d = std::static_pointer_cast<Drawable>(text);
    l3->addDrawable(d);

    shared_ptr<Drawable> found;
    assert(l1->findDrawableRecursive(drawableId, found));
    assert(found->getDrawableId()==drawableId);
}

void LayoutTest::findDrawableApplyDynamicCastTest() {
    auto l1 = make_shared<Layout>("l1", Position(0, 0), []() {});
    string drawableId = "text";
    string textInner = "dummy";
    auto text = make_shared<TextDrawable>(drawableId, Position(8, 10), [](){});
    shared_ptr<Drawable> d = std::static_pointer_cast<Drawable>(text);
    l1->addDrawable(d);
    text->setText(textInner);
    shared_ptr<Drawable> found;
    l1->findDrawableRecursive(drawableId, found);
    std::shared_ptr<TextDrawable> derived =
            std::dynamic_pointer_cast<TextDrawable> (found);
    assert(found->getDrawableId()==drawableId);
    assert(abs(derived->getBitmap().getWidth())==textInner.size());

}

void LayoutTest::transparencyTest() {
    // l2 - vyska je 2, bez pruhlednosti by prepsala l1.
    // test dopadne vypsanim TOP, BOTTOM pod sebou
    auto l1 = make_shared<Layout>("l1", Position(0, 0), []() {});
    auto l2 = make_shared<Layout>("l2", Position(0, 0), []() {});
    auto l3 = make_shared<Layout>("l3", Position(0, 0), []() {});
    l1->addInnerLayout(l2);
    l1->addInnerLayout(l3);

    string drawableTopId="TOP";
    string drawableBottomId="BOTTOM";
    auto textTop = make_shared<TextDrawable>(drawableTopId, Position(0, 0), [](){});
    auto textBottom = make_shared<TextDrawable>(drawableBottomId, Position(0, 1), [](){});

    shared_ptr<Drawable> dTop = std::static_pointer_cast<Drawable>(textTop);
    shared_ptr<Drawable> dBottom = std::static_pointer_cast<Drawable>(textBottom);

    l1->addDrawable(dTop);
    l2->addDrawable(dBottom);

    textTop->setText(drawableTopId);
    textBottom->setText(drawableBottomId);

    DisplayConsole::displayBitmap(l1->getBitmap());
}

void LayoutTest::inlinePerformanceTest() {
    // Get starting timepoint
    shared_ptr<Layout> layout = make_shared<Layout>("layout", Position(0, 0), []() {});
    const function<void()> observer = [layout]() {
        layout->getBitmap();
        //DisplayConsole::displayBitmap(layout->getBitmap());
    };
    auto start = std::chrono::high_resolution_clock::now();

    int id = 0;
    for (int i = 0; i < 99; i++) {

        for (int j = 0; j < 40; ++j) {
            id++;


            auto text = make_shared<TextDrawable>(to_string(id), Position(i, j), observer);
            shared_ptr<Drawable> d = dynamic_pointer_cast<Drawable>(text);
            layout->addDrawable(d);
            string textInner = "performance test";
            text->setText(textInner);
        }
    }

    // Get ending timepoint
    auto stop = chrono::high_resolution_clock::now();

    // Get duration. Substart timepoints to
    // get duration. To cast it to proper unit
    // use duration cast method
    auto duration = std::chrono::duration_cast<chrono::milliseconds>(stop - start);
    cout << "Inline, 4000 text drawable, rendered in: " << duration.count() << "ms" << endl;
}

void LayoutTest::innerLayoutsPerformanceTest() {
    int testDepth = 4;
    int observerCount;
    shared_ptr<Layout> layout = make_shared<Layout>("layout", Position(0, 0), []() {});
    const function<void()> observer = [&observerCount,layout]() {
        observerCount++;
        //DisplayConsole::displayBitmap(layout->getBitmap());
    };

    auto start = std::chrono::high_resolution_clock::now();
    shared_ptr<Layout> lowestInnerLayout;
    for(int i = 0;i<testDepth;i++){
        auto newInner = make_shared<Layout>(to_string(i), Position(0, 0), observer);
        if(i!=0) {
            lowestInnerLayout->addInnerLayout(newInner);
        }else{
            layout->addInnerLayout(newInner);
        }
        lowestInnerLayout=newInner;
    }
    auto text = make_shared<TextDrawable>("text",Position(10,10),observer);
    string innerText="Depth of "+ to_string(testDepth);
    auto d = dynamic_pointer_cast<Drawable>(text);
    lowestInnerLayout->addDrawable(d);
    text->setText(innerText);

    // Get ending timepoint
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<chrono::milliseconds>(stop - start);
    cout << "Inner layouts, depth of "<< to_string(testDepth)<<" and one TextDrawable, rendered in: " << duration.count() << "ms" << endl;
}

void LayoutTest::autoTest() {
    findDrawableApplyDynamicCastTest();
    recursiveSearchTest();
}

void LayoutTest::manualTest() {
    innerLayoutsPerformanceTest();
    inlinePerformanceTest();
    transparencyTest();
}
