//
// Created by voky on 23.5.22.
//

#include "ChoiceAnswerQuestionLayoutTest.h"

void ChoiceAnswerQuestionLayoutTest::manualTest() {
    auto questionLayout = make_shared<ChoiceAnswerQuestionLayout>("choiceAnswer", Position(0, 0), []() {});

    vector<pair<bool, string>> options;
    options.emplace_back(false, "Ano");
    options.emplace_back(true, "Panslavismus je hloupa idea");
    options.emplace_back(false, "Ne");

    auto multi = make_shared<MultichoiceAnswer>(options);
    Question q("Citim se jako slovan", multi);
    questionLayout->setQuestion(q);

    DisplayConsole::displayBitmap(questionLayout->getBitmap());
    questionLayout->onInteract();

    DisplayConsole::displayBitmap(questionLayout->getBitmap());
    questionLayout->onInteract();

    DisplayConsole::displayBitmap(questionLayout->getBitmap());
    // Tady je treba zadat spravnou odpoved, zejo
    assert(q.correct());
}
