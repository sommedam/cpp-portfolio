//
// Created by voky on 23.5.22.
//

#include "TextAnswerQuestionLayoutTest.h"

void TextAnswerQuestionLayoutTest::manualTest() {
    auto textAnswer = make_shared<TextStrictAnswer>("correct");
    auto a = dynamic_pointer_cast<Answer>(textAnswer);

    Question q("Question1",a);

    shared_ptr<Layout> ql = textAnswer->getSpecificQuestionLayout("layout",Position(0,0),[](){});
    auto l = dynamic_pointer_cast<QuestionLayout>(ql);
    l->setQuestion(q);
    l->onInteract();
    DisplayConsole::displayBitmap(ql->getBitmap());
}
