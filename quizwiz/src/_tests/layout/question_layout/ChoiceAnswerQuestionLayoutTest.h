//
// Created by voky on 23.5.22.
//

#ifndef QUIZWIZ_CHOICEANSWERQUESTIONLAYOUTTEST_H
#define QUIZWIZ_CHOICEANSWERQUESTIONLAYOUTTEST_H


#include <cassert>
#include "../../../gui/DisplayConsole.h"
#include "../../../quiz_containers/Question.h"
#include "../../../quiz_containers/answer/choice_answer/MultichoiceAnswer.h"
#include "../../../gui/graphical/drawable/layout/project_layouts/ChoiceAnswerQuestionLayout.h"

class ChoiceAnswerQuestionLayoutTest {
public:
    static void manualTest();
};


#endif //QUIZWIZ_CHOICEANSWERQUESTIONLAYOUTTEST_H
