//
// Created by voky on 23.5.22.
//

#include "PageableLayoutTest.h"

void PageableLayoutTest::manualTest() {
    auto baseLayout = make_shared<PageableLayout>("pageableLayout",Position(0,0),[](){});
    auto l1 = make_shared<Layout>("l1",Position(0,0),[](){});
    auto l2 = make_shared<Layout>("l2",Position(0,1),[](){});

    auto textL1 = make_shared<TextDrawable>("text1",Position(0,0),[](){});
    auto textL2 = make_shared<TextDrawable>("text2",Position(0,0),[](){});
    string textInner1="layout 1";
    string textInner2="layout 2";
    textL1->setText(textInner1);
    textL2->setText(textInner2);

    shared_ptr<Drawable> d1 = std::static_pointer_cast<Drawable>(textL1);
    shared_ptr<Drawable> d2 = std::static_pointer_cast<Drawable>(textL2);

    l1->addDrawable(d1);
    l2->addDrawable(d2);
    baseLayout->addInnerLayout("page1",l1);
    baseLayout->addInnerLayout("page2",l2);
    baseLayout->switchPage("page2");

    DisplayConsole::displayBitmap(baseLayout->getBitmap());
    baseLayout->getBitmap();
}
