//
// Created by voky on 23.5.22.
//

#include "BitmapTest.h"

void BitmapTest::manualTest() {
    Bitmap bitmap;
    bitmap.write(0, 0, 'A');
    assert(bitmap.read(0, 0) == 'A');
    assert(bitmap.getHeight() == 1);
    assert(bitmap.getWidth() == 1);

    bitmap.write(1, 0, 'B');
    assert(bitmap.read(1, 0) == 'B');
    assert(bitmap.getWidth() == 2);

    bitmap.write(3, 4, '/');
    assert(bitmap.read(3, 4) == '/');
    assert(bitmap.getWidth() == 4);
    assert(bitmap.getHeight() == 5);
    assert(bitmap.read(0, 4) == 0);
    assert(bitmap.read(3, 0) == 0);
}
