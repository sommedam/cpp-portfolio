//
// Created by voky on 23.03.22.
//

#ifndef QUIZWIZ_QUESTION_H
#define QUIZWIZ_QUESTION_H

#include <string>
#include <memory>
#include <utility>
#include "answer/Answer.h"

using namespace std;

/**
 * Class encapslating @link Answer.h and adding common elements of questions.
 */
class Question {
    string header; /* question text displayed to user */
    shared_ptr<Answer> answer; /* answer to question */

public:
    Question(string head, const shared_ptr<Answer>& answer) : header(std::move(head)), answer(answer) {}
    explicit Question()=default;

    void setHeader(const string &header);

    void setAnswer(const shared_ptr<Answer> &answer);

    /**
     * Checks if answer is correct
     * @return true if Answer correct
     */
    bool correct();

    [[nodiscard]] const string &getHeader() const;

    [[nodiscard]] const shared_ptr<Answer> &getAnswer() const;

    /**
     * Output stream, exporting to file. Exports Question->Answer
     * @param os Question output will be attached to
     * @return stream of export
     */
    friend ostream &operator<<(ostream &os,Question &q);
};


#endif //QUIZWIZ_QUESTION_H
