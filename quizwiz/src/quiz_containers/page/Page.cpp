//
// Created by voky on 23.03.22.
//

#include "Page.h"

#include <utility>

Page::Page(string  pageName) : pageName(std::move(pageName)) {
}

string Page::getPageName() const {
    return pageName;
}

bool Page::correct() {
    for (auto &question: questions) {
        if (!(question.correct()))
            return false;
    }
    return true;
}

std::ostream &operator<<(ostream &os, const shared_ptr<Page> &p) {
    os<<"page:";
    os<<R"("name"=')";
    os<<p->pageName;
    os<<"\'";
    os<<R"(,"policy"=')";
    // if set
    if(p->pagePolicy!= nullptr){
        os<<p->pagePolicy;
    }
    os<<"\'";
    os<<"\n";
    for(auto& q:p->questions){
        os<<q;
    }
    return os;
}

const vector<Question> &Page::getQuestions() const {
    return questions;
}



void Page::setQuestions(const vector<Question> &q) {
    Page::questions = q;
}

void Page::addQuestion(Question &q) {
    questions.emplace_back(q);
}

void Page::setPolicy(shared_ptr<PagePolicyWrapper> p) {
    pagePolicy=std::move(p);
}

void Page::setPageName(const string &pn) {
    Page::pageName = pn;
}

const shared_ptr<PagePolicyWrapper> &Page::getPagePolicy() const {
    return pagePolicy;
}

