//
// Created by voky on 23.03.22.
//
#ifndef QUIZWIZ_PAGE_H
#define QUIZWIZ_PAGE_H
#include <string>
#include <utility>
#include <vector>
#include <map>
#include <set>
#include <memory>
#include "../Question.h"
#include "page_policy/PagePolicy.h"
#include <sstream>
#include <iostream>

using namespace std;

/**
 * Class containing all questions on single page.
 * Used to determine if all questions on page are correct and exporting.
 */
class Page {
    shared_ptr<PagePolicyWrapper> pagePolicy; /* PagePolicy can be null */
    vector<Question> questions; /* list of all questions on page */
    string pageName;

public:
    explicit Page(string  pageName);

    /**
     * Assign Question to Page.
     * @param q question to assign
     */
    void addQuestion(Question &q);

    void setQuestions(const vector<Question> &q);

    void setPolicy(shared_ptr<PagePolicyWrapper> p);

    /**
     * Output of Page->Question->Answer
     */
    friend std::ostream& operator<<(std::ostream &os, const shared_ptr<Page>& p);

    /**
     * Checking if all Answers on Page are correct.
     * @return bool true, if yes
     */
    bool correct();

    [[nodiscard]] const vector<Question> &getQuestions() const;

    [[nodiscard]] string getPageName() const;

    void setPageName(const string &pageName);

    [[nodiscard]] const shared_ptr<PagePolicyWrapper> &getPagePolicy() const;
};


#endif //QUIZWIZ_PAGE_H
