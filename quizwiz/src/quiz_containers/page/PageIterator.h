//
// Created by voky on 23.03.22.
//

#ifndef QUIZWIZ_PAGEITERATOR_H
#define QUIZWIZ_PAGEITERATOR_H

#include "Page.h"
#include "page_policy/PagePolicy.h"

using namespace std;
/**
 * Class for sequential switching trough pages.
 */
class PageIterator {
    vector<shared_ptr<Page>>::iterator beginPage, endingPage, currentPage;
    PagePolicyBuilder builder;
public:
    const static int NEXT_PAGE_END = 0; /* flag indicating end of the quiz, current page wont change, if thrown */
    const static int NEXT_PAGE_OK = 1; /* next page is okay to proceed */
    const static int NEXT_PAGE_ABORT = 2; /* next page aborted, due to several reasons, exception will be served by listener of some kind (ex. PagePolicy) */


    PageIterator() = default;

    PageIterator(const vector<shared_ptr<Page>>::iterator &beginPage,
                 const vector<shared_ptr<Page>>::iterator &endingPage);

    PageIterator(const vector<shared_ptr<Page>>::iterator &beginPage,
                 const vector<shared_ptr<Page>>::iterator &endingPage, PagePolicyBuilder &builder);

    void set(vector<shared_ptr<Page>>::iterator begin,
             vector<shared_ptr<Page>>::iterator end);

    /**
     * Sets current iterator to next page
     * @return one of NEXT_PAGE_flag
     */
    int nextPage();

    /**
     * Initialization of iterator
     */
    void begin();

    vector<shared_ptr<Page>>::iterator &getCurrentPage();
};


#endif //QUIZWIZ_PAGEITERATOR_H
