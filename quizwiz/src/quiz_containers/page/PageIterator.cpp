//
// Created by voky on 23.03.22.
//

#include "PageIterator.h"

vector<shared_ptr<Page>>::iterator &PageIterator::getCurrentPage() {
    return currentPage;
}

void PageIterator::begin() {
    currentPage= beginPage;
}

int PageIterator::nextPage() {
    auto currentPagePolicy = currentPage.operator*()->getPagePolicy();
    int policyFlag = PagePolicyWrapper::NEXT_PAGE_CONTINUE; /* default value, if not interrupted */
    if(currentPagePolicy!= nullptr) {
        currentPagePolicy->assignListenerFromBuilder(builder); /* set listener builder to flag */
        policyFlag = currentPagePolicy->onNextPage(currentPage.operator*()->getQuestions()); /* override this nextPage() */
    }
    // if policy flag aborted nextPage(), it wont be executed
    if (policyFlag == PagePolicyWrapper::NEXT_PAGE_ABORT) {
        return PageIterator::NEXT_PAGE_ABORT;
    } else {
        // policy before even asking if this is last page
        if (currentPage + 1 == endingPage) {
            return NEXT_PAGE_END;
        }
        currentPage++;
        return NEXT_PAGE_OK;
    }
}

void PageIterator::set(vector<shared_ptr<Page>>::iterator begin, vector<shared_ptr<Page>>::iterator end) {
    this->beginPage=begin;
    this->endingPage=end;
    this->begin();
}


PageIterator::PageIterator(const vector<shared_ptr<Page>>::iterator &beginPage,
                           const vector<shared_ptr<Page>>::iterator &endingPage) : beginPage(beginPage),
                                                                                   endingPage(endingPage) {
    this->begin();
}

PageIterator::PageIterator(const vector<shared_ptr<Page>>::iterator &beginPage,
                           const vector<shared_ptr<Page>>::iterator &endingPage, PagePolicyBuilder &builder)
        : beginPage(beginPage), endingPage(endingPage), builder(builder) {
    this->begin();
}


