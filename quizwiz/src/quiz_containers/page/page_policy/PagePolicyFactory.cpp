//
// Created by voky on 16.5.22.
//

#include "PagePolicyFactory.h"


PagePolicyFactory::PagePolicyFactory() {
    for (auto pagePolicie : pagePolicies) {
        tagMap.insert(pair<string,PagePolicyWrapper*>(pagePolicie->TAG(),pagePolicie));
    }
}

shared_ptr<PagePolicyWrapper> PagePolicyFactory::NEW_PAGE_BY_TAG(string &tag) {
    return tagMap.at(tag)->newBlankPolicy();
}

string PagePolicyFactory::ALL_PAGE_FLAGS_LIST() {
    string r;
    int index = 0;
    for (const auto &a: pagePolicies) {
        r += "[" + to_string(index) + "] " + a->DESCRIPTION() + "\n";
        index++;
    }
    return r;
}

function<shared_ptr<PagePolicyWrapper>()> PagePolicyFactory::PAGE_POLICY_OF_INDEX(size_t index) const {
    function<shared_ptr<PagePolicyWrapper>()> r = [this, index]() {
        return pagePolicies.at(index)->pagePolicyCreatorWizard();
    };
    return r;
}
