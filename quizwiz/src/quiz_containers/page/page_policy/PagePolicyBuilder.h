//
// Created by voky on 12.5.22.
//
#ifndef QUIZWIZ_PAGEPOLICYBUILDER_H
#define QUIZWIZ_PAGEPOLICYBUILDER_H


#include <string>
#include <functional>

using namespace std;
class PagePolicyBuilder {
    function<void(string s)> messageFlagListener=[](const string& s){};
    function<void(int i)> blockFlagListener=[](int i){};
public:
    PagePolicyBuilder();

    explicit PagePolicyBuilder(const tuple<function<void(string)> &, function<void(int)> &> &listenersPack);

    tuple<function<void(string s)>&, function<void(int i)>&> listenersPack{
            messageFlagListener, blockFlagListener};

    PagePolicyBuilder& MessageFlagListener(const function<void(string)> &messageFlagListener);
    PagePolicyBuilder& BlockFlagListener(const function<void(int)> &blockFlagListner);

};


#endif //QUIZWIZ_PAGEPOLICYBUILDER_H
