//
// Created by voky on 16.5.22.
//

#ifndef QUIZWIZ_PAGEPOLICYFACTORY_H
#define QUIZWIZ_PAGEPOLICYFACTORY_H


#include "PagePolicy.h"
#include "next_page_policies/MessagePagePolicy.h"
#include "next_page_policies/BlockPagePolicy.h"

class PagePolicyFactory {
public:
    PagePolicyFactory();

private:

    map<string,shared_ptr<PagePolicyWrapper>> tagMap;
    
    const vector<PagePolicyWrapper *> pagePolicies = {
            new MessagePagePolicy(nullptr),
            new BlockPagePolicy(nullptr)
    };

public:
    shared_ptr<PagePolicyWrapper> NEW_PAGE_BY_TAG(string& tag);
    string ALL_PAGE_FLAGS_LIST();
    [[nodiscard]] function<shared_ptr<PagePolicyWrapper>()> PAGE_POLICY_OF_INDEX(size_t index) const;

};


#endif //QUIZWIZ_PAGEPOLICYFACTORY_H
