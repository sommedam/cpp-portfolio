//
// Created by voky on 11.5.22.
//
#ifndef QUIZWIZ_PAGEPOLICY_H
#define QUIZWIZ_PAGEPOLICY_H

#include <ostream>
#include <memory>
#include "../../Question.h"
#include "PagePolicyWrapper.h"


using namespace std;

/**
 * Class for overriding default behaviour of nextPage mathod in @link PageIterator.h
 * @tparam RETURN_TYPE type of extra payload from @link PahePolicy.h to @link Fragment.h, listening to @link PageIterator.h
 */
template <typename /*...*/ RETURN_TYPE=int >
class PagePolicy: public PagePolicyWrapper{
protected:
    function<void (RETURN_TYPE)> nextPageListener;

public:
    PagePolicy()=default;
    explicit PagePolicy(const function<void(RETURN_TYPE)> &listener): nextPageListener(listener) {}
};


#endif //QUIZWIZ_PAGEPOLICY_H
