//
// Created by voky on 11.5.22.
//

#include "PagePolicyWrapper.h"

std::ostream &operator<<(ostream &str, const PagePolicyWrapper &a) {
    a.outputStream(str);
    return str;
}

std::ostream &operator<<(ostream &str, const shared_ptr<PagePolicyWrapper> &a) {
    a->outputStream(str);
    return str;
}

std::istream &operator>>(istream &istr, PagePolicyWrapper &a) {
    a.inputStream(istr);
    return istr;
}

std::istream &operator>>(istream &istr, shared_ptr<PagePolicyWrapper> &a) {
    a->inputStream(istr);
    return istr;
}