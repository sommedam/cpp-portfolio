//
// Created by voky on 11.5.22.
//

#include "MessagePagePolicy.h"
#include "../PagePolicyBuilder.h"

int MessagePagePolicy::onNextPage(const vector<Question> &q) const {
    bool foundIncorrectAnswer = false;
    for(auto question:q){
        if(!question.correct()){
            foundIncorrectAnswer= true;
            break;
        }
    }
    if(!foundIncorrectAnswer){
        return NEXT_PAGE_CONTINUE;
    }else {
        if (nextPageListener != nullptr) {
            nextPageListener(messageToDisplay);
        }
        return PagePolicyWrapper::NEXT_PAGE_ABORT;
    }
}

void MessagePagePolicy::assignListenerFromBuilder(const PagePolicyBuilder &builder) {
    nextPageListener=get<0>(builder.listenersPack);
}

void MessagePagePolicy::inputStream(istream &in) {
    getline(in,messageToDisplay);
}

void MessagePagePolicy::outputStream(ostream &os) const {
    os<<TAG()<<";"<<messageToDisplay;
}

shared_ptr<PagePolicyWrapper> MessagePagePolicy::pagePolicyCreatorWizard() const {
    std::cout<<"Zadejte zpravu, pro neuspesneho resitele: ";
    // todo nullptr
    auto message = make_shared<MessagePagePolicy>(nullptr);
    string messageTextInput;
    getline(std::cin,messageTextInput);
    message->setMessageToDisplay(messageTextInput);
    return dynamic_pointer_cast<PagePolicyWrapper>(message);
}

string MessagePagePolicy::TAG() const {
    return "message";
}

void MessagePagePolicy::setMessageToDisplay(const string &messageToDisplay) {
    MessagePagePolicy::messageToDisplay = messageToDisplay;
}

MessagePagePolicy::MessagePagePolicy(const function<void(string)> &listener) : PagePolicy(listener) {}

string MessagePagePolicy::DESCRIPTION() const {
    return "Pri chybne vyplnene strane zobrazit zpravu a ukoncit kviz.";
}

shared_ptr<PagePolicyWrapper> MessagePagePolicy::newBlankPolicy() const {
    auto shared = make_shared<MessagePagePolicy>(nullptr);
    return dynamic_pointer_cast<PagePolicyWrapper>(shared);
}

MessagePagePolicy::MessagePagePolicy() {}
