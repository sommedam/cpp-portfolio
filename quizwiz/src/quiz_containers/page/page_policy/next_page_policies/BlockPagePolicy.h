//
// Created by voky on 11.5.22.
//

#ifndef QUIZWIZ_BLOCKPAGEPOLICY_H
#define QUIZWIZ_BLOCKPAGEPOLICY_H

/**
 * Policy blocking going to next page, if all answers not correct
 */
class BlockPagePolicy: public PagePolicy<int>{
public:
    explicit BlockPagePolicy(const function<void(int)> &listener);

    BlockPagePolicy();

private:
    [[nodiscard]] int onNextPage(const vector<Question>& q)const override;

    [[nodiscard]] shared_ptr<PagePolicyWrapper> pagePolicyCreatorWizard() const override;

    [[nodiscard]] string TAG() const override;

    [[nodiscard]] string DESCRIPTION() const override;

public:
    [[nodiscard]] shared_ptr<PagePolicyWrapper> newBlankPolicy() const override;

    void assignListenerFromBuilder(const PagePolicyBuilder &builder) override;
};


#endif //QUIZWIZ_BLOCKPAGEPOLICY_H
