//
// Created by voky on 11.5.22.
//

#include "../../Page.h"
#include "BlockPagePolicy.h"
#include "../PagePolicyBuilder.h"


int BlockPagePolicy::onNextPage(const vector<Question> &q) const {
    bool foundIncorrectAnswer = false;
    for(auto question:q){
        if(!question.correct()){
            foundIncorrectAnswer= true;
            break;
        }
    }
    if(!foundIncorrectAnswer){
        return NEXT_PAGE_CONTINUE;
    }else{
        if (nextPageListener != nullptr) {
            nextPageListener(0);
        }
        return PagePolicyWrapper::NEXT_PAGE_ABORT;
    }
}

shared_ptr<PagePolicyWrapper> BlockPagePolicy::pagePolicyCreatorWizard() const {
    return dynamic_pointer_cast<PagePolicyWrapper>(make_shared<BlockPagePolicy>(nullptr));
}

BlockPagePolicy::BlockPagePolicy(const function<void(int)> &listener) : PagePolicy(listener) {

}

string BlockPagePolicy::TAG() const {
    return "block";
}


string BlockPagePolicy::DESCRIPTION() const {
    return "Znemoznit preskoceni strany, pokud je chybne vyplnena.";
}

shared_ptr<PagePolicyWrapper> BlockPagePolicy::newBlankPolicy() const {
    auto shared = make_shared<BlockPagePolicy>(nullptr);
    return dynamic_pointer_cast<PagePolicyWrapper>(shared);
}

void BlockPagePolicy::assignListenerFromBuilder(const PagePolicyBuilder &builder) {
    nextPageListener= std::get<1>(builder.listenersPack);
}

BlockPagePolicy::BlockPagePolicy() = default;


//BlockPagePolicy::BlockPagePolicy() = default;


