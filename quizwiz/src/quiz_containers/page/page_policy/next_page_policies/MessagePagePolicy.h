//
// Created by voky on 11.5.22.
//

#ifndef QUIZWIZ_MESSAGEPAGEPOLICY_H
#define QUIZWIZ_MESSAGEPAGEPOLICY_H


#include "../PagePolicy.h"
#include "../PagePolicyBuilder.h"

/**
 * Policy displaying set message to user, if failed to solve page.
 * End quiz if failed to answer all questions on page.
 */
class MessagePagePolicy: public PagePolicy<string>{
    string messageToDisplay;
public:

    explicit MessagePagePolicy(const function<void(string)> &listener);

    MessagePagePolicy();

    [[nodiscard]] int onNextPage(const vector<Question> &q) const override;

    [[nodiscard]] shared_ptr<PagePolicyWrapper> pagePolicyCreatorWizard() const override;

protected:
    void inputStream(istream &in) override;

    void outputStream(ostream &os) const override;

private:
    [[nodiscard]] string TAG() const override;

    [[nodiscard]] string DESCRIPTION() const override;

public:
    [[nodiscard]] shared_ptr<PagePolicyWrapper> newBlankPolicy() const override;

public:
    void setMessageToDisplay(const string &messageToDisplay);

    void assignListenerFromBuilder(const PagePolicyBuilder &builder) override;
};


#endif //QUIZWIZ_MESSAGEPAGEPOLICY_H
