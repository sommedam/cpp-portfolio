//
// Created by voky on 12.5.22.
//

#include "PagePolicyBuilder.h"

PagePolicyBuilder& PagePolicyBuilder::MessageFlagListener(const function<void(string)> &messageFlagListener) {
    PagePolicyBuilder::messageFlagListener = messageFlagListener;
    return *this;
}

PagePolicyBuilder& PagePolicyBuilder::BlockFlagListener(const function<void(int)> &blockFlagListner) {
    PagePolicyBuilder::blockFlagListener = blockFlagListner;
    return *this;
}

PagePolicyBuilder::PagePolicyBuilder(const tuple<function<void(string)> &, function<void(int)> &> &listenersPack)
        : listenersPack(listenersPack) {}

PagePolicyBuilder::PagePolicyBuilder() = default;
