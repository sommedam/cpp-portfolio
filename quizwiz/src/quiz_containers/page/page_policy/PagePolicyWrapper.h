//
// Created by voky on 11.5.22.
//

#ifndef QUIZWIZ_PAGEPOLICYWRAPPER_H
#define QUIZWIZ_PAGEPOLICYWRAPPER_H

#include <memory>
#include "../../Question.h"
#include "PagePolicyBuilder.h"

using namespace std;
/*
 * Wrapper of generic @link PagePolicy.h class.
 * Used to bulk managing several Policies and emplacing them in data containers
 */
class PagePolicyWrapper {
    /**
     * File output
     * @param str empty stream
     * @return output in generally unspecified, PagePolicy specific format, that can be reversibly read by >>
     */
    friend std::ostream &operator<<(std::ostream &str, PagePolicyWrapper const &a);

    /**
     * File output
     * @param str empty stream
     * @return output in generally unspecified, PagePolicy specific format, that can be reversibly read by >>
     */
    friend std::ostream &operator<<(std::ostream &str, shared_ptr<PagePolicyWrapper> const &a);

    /**
     * File input
     * @param str stream in @link PagePolicy.h specific format, in which is previously exported
     * @return empty stream
     */
    friend std::istream &operator>>(std::istream &istr, PagePolicyWrapper &a);
    /**
     * File input
     * @param str stream in @link PagePolicy.h specific format, in which is previously exported
     * @return empty stream
     */
    friend std::istream &operator>>(std::istream &istr, shared_ptr<PagePolicyWrapper> &a);

protected:
    /**
     * substitude for >> operator
     */
    virtual void inputStream(std::istream &in) {
        /* default PagePolicy dont have any special bundle */
    };

    /**
     * substitude for << operator
     */
    virtual void outputStream(std::ostream &os) const {
        os << TAG() << ";"; /* Payload in format, 'TAG;specific payload' */
    };

public:
    /**
    * Starts sequence of steps to create specific flag
    * @attention method could use stdin, before calling stop any input listeners
    * @return shared pointer to created PagePolicy
    */
    [[nodiscard]] virtual shared_ptr<PagePolicyWrapper> pagePolicyCreatorWizard() const = 0;

    [[nodiscard]] virtual shared_ptr<PagePolicyWrapper> newBlankPolicy() const = 0;

    /**
     * Unique tag of policy, invisible to user.
     */
    [[nodiscard]] virtual string TAG() const = 0;

    /*
     * Description of policy, visible to user.
     */
    [[nodiscard]] virtual string DESCRIPTION() const = 0;

    static const int NEXT_PAGE_ABORT = 0x0; /* flag canceling onNextPage in @link PageIterator.h */
    static const int NEXT_PAGE_CONTINUE = 0x1; /* flag not interrupting onNextPage in  @link PageIterator.h routine */
    virtual void assignListenerFromBuilder(const PagePolicyBuilder &builder) = 0;

    /**
     * Override nextPage.
     * @param q list of questions from actual page
     * @return flag, if ABORT cancel default nextPage procedure, if CONTINUE, resume routine.
     */
    [[nodiscard]] virtual int onNextPage(const vector<Question> &q) const = 0;
};


#endif //QUIZWIZ_PAGEPOLICYWRAPPER_H
