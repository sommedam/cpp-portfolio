//
// Created by voky on 23.03.22.
//
#include <memory>
#include <vector>
#include "page/Page.h"
#include "page/PageIterator.h"

#ifndef QUIZWIZ_QUIZ_H
#define QUIZWIZ_QUIZ_H

using namespace std;
// Trida pro sledovani postupu kvizem.
class Quiz {
    // Seznam vsech stranek kvizu.
    vector<shared_ptr<Page>> pages;

public:
    Quiz() = default;

    explicit Quiz(vector<shared_ptr<Page>> &p) : pages(p) {}

public:
    friend std::ostream &operator<<(std::ostream &os, const Quiz &x);

    PageIterator getPageIterator(PagePolicyBuilder & builder);
    PageIterator getPageIterator();

    void addPage(const shared_ptr<Page> &page);

    /**
     * @return pocet vsech otazek v kvizu
     */
    int getQuestionsCount();

    /**
     * @return pocet spravnych odpovedi v kvizu
     */
    int getCorrectCount();

    /**
     * Spoji vypis vsech chyb v kvizu.
     * @return navraci kompletni zpravu o chybach pro uzivatele
     */
    string failAnswersOutputAggregated();

    [[nodiscard]] const vector<shared_ptr<Page>> &getPages() const;
};


#endif //QUIZWIZ_QUIZ_H
