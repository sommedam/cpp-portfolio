//
// Created by voky on 22.03.22.
//

#ifndef QUIZWIZ_CHOICEANSWER_H
#define QUIZWIZ_CHOICEANSWER_H


#include <random>
#include "../Answer.h"

/**
 * Answer with listed choices
 */
class ChoiceAnswer : public Answer {
protected:
    vector<pair<bool, string>> options; /* list of all posible optionsSet */
    set<char> chosenOptions; /* optionsSet answered by user, char refers to key in @link keyMappedOptions */
    /**
     * all optionsSet; mapped by assigned key of user input
     * @link method generateKeyMappedOptions() changes value of var.
     * */
    map<char, pair<bool, string>> keyMappedOptions;
    set<string> yes = {"ano", "a", "A", "ano", "Ano", "Y", "y", "yes", "Yes"}; /* user input accepted as true */
    set<string> no = {"nikoliv", "nikolivek", "n", "N", "ne", "Ne", "No", "no"}; /* user input accepted as false */

public:
    /**
     * @param options list of all possible optionsSet to be chosen from. maximum length of list is 26 (num. of small latin chars.).
     */
    explicit ChoiceAnswer(const vector<pair<bool, string>> &options);

    ChoiceAnswer() = default;

    /**
     * Randomized mapping of small latin letters to answer optionsSet.
     * Used to map user entered characters to chosen optionsSet.
     * @return reference to map<char_value, option>
     */
    const map<char, pair<bool, string>> &generateKeyMappedOptions();

    /**
     * @return set of answered optionsSet, char refers to key in @link keyMappedOptions
    */
    [[nodiscard]] const set<char> &getChosenOptions() const;

    /**
    * @return map of all optionsSet; mapped by assigned key of user input
    */
    [[nodiscard]] const map<char, pair<bool, string>> &getKeyMappedOptions() const;

    [[nodiscard]] bool correct() const override;
};

#endif //QUIZWIZ_CHOICEANSWER_H
