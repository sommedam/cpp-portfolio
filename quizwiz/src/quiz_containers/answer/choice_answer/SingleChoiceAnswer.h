//
// Created by voky on 12.04.22.
//

#ifndef QUIZWIZ_SINGLECHOICEANSWER_H
#define QUIZWIZ_SINGLECHOICEANSWER_H


#include <sstream>
#include "ChoiceAnswer.h"
#include "../exceptions/AnswerUserInputException.h"
#include "../../../gui/graphical/drawable/layout/Layout.h"

class SingleChoiceAnswer: public ChoiceAnswer {
public:
    explicit SingleChoiceAnswer(const vector<pair<bool, string>> &o);
    SingleChoiceAnswer()= default;

    /**
    * @param i input in format: "char1 [" ",]* char2 [" ",]* .. charN"
    */
    void setInput(string i) override;

    /**
    * @param fileInput in format "true{"option"}false{"option1",..,"optionN"}" order of true and false option set is not defined
    */
    void inputStream(std::istream &in)override;

    [[nodiscard]] string falseAnswerOutput() const override;

    shared_ptr<Layout> getSpecificQuestionLayout(const string &drawableId, const Position &relativePosition,
                                                 const function<void()> &observer) const override;

    /**
    * @param os in format "true{"option"}false{"option1",..,"optionN"}"
    */
    void outputStream(std::ostream &os) const override;

    [[nodiscard]] shared_ptr<Answer> answerCreatorWizard() const override;

    [[nodiscard]] string DESCRIPTION() const override;

    [[nodiscard]] string INPUT_FORMAT() const override;

};


#endif //QUIZWIZ_SINGLECHOICEANSWER_H
