//
// Created by voky on 12.04.22.
//

#include "MultichoiceAnswer.h"
#include "../../../gui/graphical/drawable/layout/project_layouts/ChoiceAnswerQuestionLayout.h"
#include "../exceptions/WizardFormatInputException.h"
#include "../exceptions/AnswerInputStreamException.h"

MultichoiceAnswer::MultichoiceAnswer(const vector<pair<bool, string>> &o) : ChoiceAnswer(o) {}

void MultichoiceAnswer::setInput(string i) {
    chosenOptions.clear(); /* overwrite previous chosen optionsSet */

    if (i.empty()) {
        return; /* just erase previous input */
    }
    // foreach char of input
    for (char c: i) {
        // keys are in interval of all small latin letters
        if (c >= 'a' && c <= 'z') {
            chosenOptions.insert(c);
        } else if (c == ' ' || c == ',') {
            continue; /* space and the other char is tolerated in between answered keys */
        } else {
            throw AnswerUserInputException(); /* unexpected characters are prohibited */
        }
    }
}

void MultichoiceAnswer::inputStream(istream &fileInput) {
    vector<pair<bool, string>> newOptions;
    // for {true, false}
    for (int i = 0; i < 2; i++) {
        bool currentBool; /* if reading set is of false or true */
        string prefix;
        getline(fileInput, prefix, '{');
        if (prefix == "true") {
            currentBool = true;
        } else if (prefix == "false") {
            currentBool = false;
        } else {
            throw AnswerInputStreamException(); /* unexpected prefix. check whitespace match */
        }

        string optionsSetString;
        char c;
        getline(fileInput, optionsSetString, '}'); /* read till end of optionsSet set */

        string nextOption; /* each option is build by attaching char by char */
        size_t optionsSetStringIndex = 0; /* index of currently read char in optionsSet set string */
        bool isCurrentlyBuilding = false; /* if currently processing option, or skipping intermediate characters and quotion marks */
        while (optionsSetStringIndex < optionsSetString.size()) {
            c = optionsSetString[optionsSetStringIndex++];
            if (c == '\"') {
                /* if quotion mark encountered; stop building and push to vector */
                if (isCurrentlyBuilding) {
                    newOptions.push_back(
                            pair<bool, string>(currentBool, nextOption)); /* option built, push to vector */
                    nextOption = ""; /* clear for next option build */
                }
                isCurrentlyBuilding = !isCurrentlyBuilding;
            } else {
                /* if quotion mark encountered; start building option string */
                if (isCurrentlyBuilding) {
                    nextOption += c; /* append to string */
                } else {
                    /* ignore whitespaces */
                }
            }
        }
    }
    options = newOptions;
}

void MultichoiceAnswer::outputStream(ostream &os) const {
    os << "true{";
    for (const auto &answer: options) {
        if (answer.first) {
            os << "\"";
            os << answer.second;
            os << "\",";
        }
    }
    os << "}";
    os << "false{";
    for (const auto &answer: options) {
        if (!answer.first) {
            os << "\"";
            os << answer.second;
            os << "\",";
        }
    }
    os << "}";
}

string MultichoiceAnswer::falseAnswerOutput() const {
    string r;
    r += "Spravne odpovedi: ";
    r += "\n";
    for (const auto &o: keyMappedOptions) {
        if (o.second.first) {
            r += "[";
            r += o.first;
            r += "] ";
            r += o.second.second;
            r += "\n";
        }
    }
    r+= "Vase odpovedi: ";
    r+="\n";
    for (char choosenKey: chosenOptions) {
        auto o = keyMappedOptions.at(choosenKey);
            r += "[";
            r += choosenKey;
            r += "] ";
            r += o.second;
            r += "\n";
    }
    return r;
}

shared_ptr<Layout>
MultichoiceAnswer::getSpecificQuestionLayout(const string &drawableId, const Position &relativePosition,
                                             const function<void()> &observer) const {
    auto singlechoice = make_shared<ChoiceAnswerQuestionLayout>(drawableId, relativePosition, observer);
    auto l = dynamic_pointer_cast<Layout>(singlechoice);
    return l;
}

string MultichoiceAnswer::INPUT_FORMAT() const {
    return "Vypiste znaky spravnych odpovedi vedle sebe (napr.: ab): ";
}

shared_ptr<Answer> MultichoiceAnswer::answerCreatorWizard() const {
    vector<pair<bool, string>> options;
    int optionNumber = 1; /* foreach inputed option writes number of option */
    int read=0; /* user input of how much options to add */
    string readInput;
    cout << "Kolik byste chteli pridat odpovedi na otazku? ";
    getline(cin, readInput);
    try{
        read = stoi(readInput);
    }catch (exception &e){
        throw WizardFormatInputException();
    }
    /* if input data mismatch or if user is willing to add no optionsSet, or more than latin letters @see choice_answer.getKeyMappedOptions() */
    if(read==0||read>26) {
        throw WizardFormatInputException();
    }
    for (int i = 0; i < read; i++) {
        string optionInput; /* user inputed option text */
        string isCorrectInput; /* user inputed if option is correct or not */
        bool correct = false; /* @link isCorrectInput casted to bool */
        cout << "Zadejte " << to_string(optionNumber++) << ". odpoved na otazku: ";
        getline(cin,optionInput);
        if (optionInput.empty()) { throw WizardFormatInputException(); }
        cout << "\tJe tato odpoved spravne? [ano / nikolivek]: ";
        getline(cin,isCorrectInput);
        if (yes.find(isCorrectInput) == yes.end()) {
            if (no.find(isCorrectInput) == no.end()) {
                throw WizardFormatInputException();
            }
        } else {
            correct = true;
        }
        options.emplace_back(correct, optionInput);
    }

    auto multichoice = make_shared<MultichoiceAnswer>(options);
    auto a = dynamic_pointer_cast<Answer>(multichoice);
    return a;
}

string MultichoiceAnswer::DESCRIPTION() const {
    return "Odpoved s vypsanymi moznostmi a vice spravnymi odpovedmi";
}
