//
// Created by voky on 22.03.22.
//

#ifndef QUIZWIZ_MULTICHOICEANSWER_H
#define QUIZWIZ_MULTICHOICEANSWER_H


#include <sstream>
#include <memory>
#include "../Answer.h"
#include "ChoiceAnswer.h"
#include "../exceptions/AnswerUserInputException.h"
#include "../../../gui/graphical/drawable/layout/Layout.h"
#include "../../../gui/Position.h"

/**
 * Answer with listed choices and multiple correct optionsSet
 */
class MultichoiceAnswer : public ChoiceAnswer {
public:
    explicit MultichoiceAnswer(const vector<pair<bool, string>> &o);

    MultichoiceAnswer() = default;

    /**
     * @param i input in format: "char1 [" ",]* char2 [" ",]* .. charN"
     */
    void setInput(string i) override;

    /**
     * @param fileInput in format "true{"option1",..,"optionN"}false{"option1",..,"optionN"}" order of true and false option set is not defined
     */
    void inputStream(std::istream &fileInput) override;

    /**
    * @param os in format "true{"option1",..,"optionN"}false{"option1",..,"optionN"}"
    */
    void outputStream(std::ostream &os) const override;

    /**
     * @return string in format [a] correct option1, [b] correct option2, ...
     */
    [[nodiscard]] string falseAnswerOutput() const override;


    shared_ptr<Layout> getSpecificQuestionLayout(const string &drawableId, const Position &relativePosition,
                                                 const function<void()> &observer) const override;

    [[nodiscard]] string INPUT_FORMAT() const override;

    [[nodiscard]] shared_ptr<Answer> answerCreatorWizard() const override;

    [[nodiscard]] string DESCRIPTION() const override;
};


#endif //QUIZWIZ_MULTICHOICEANSWER_H
