//
// Created by voky on 12.04.22.
//

#include "SingleChoiceAnswer.h"
#include "../../../gui/graphical/drawable/layout/project_layouts/ChoiceAnswerQuestionLayout.h"
#include "../exceptions/WizardFormatInputException.h"
#include "../exceptions/AnswerInputStreamException.h"

SingleChoiceAnswer::SingleChoiceAnswer(const vector<pair<bool, string>> &o) : ChoiceAnswer(o){}

void SingleChoiceAnswer::setInput(string i) {
    chosenOptions.clear(); /* overwrite previous chosen optionsSet */
    if (i.empty()) {
        return; /* just erase previous input */
    }
    size_t stringIndex = 0;
    // foreach char of input
    while (stringIndex != i.size()) {
        char c = i[stringIndex++];
        // keys are in interval of all small latin letters
        if (c >= 'a' && c <= 'z') {
            chosenOptions.insert(c);
        } else if (c == ' ' || c == ',') {
            continue; /* space and the other char is tolerated in between answered keys */
        } else {
            throw AnswerUserInputException(); /* unexpected characters are prohibited */
        }
    }
    // Pouze jedna otazka je spravne. Pokud uzivatel zada vice nez jednu, tak je chyba vstupu
    if (chosenOptions.size() > 1) {
        throw AnswerUserInputException();
    }
}

void SingleChoiceAnswer::inputStream(istream &in) {
    vector<pair<bool, string>> newOptions;
    // for {true, false}
    for (int i = 0; i < 2; i++) {
        bool currentBool;  /* if reading set is of false or true */
        string prefix;
        getline(in, prefix, '{');
        if (prefix == "true") {
            currentBool = true;
        } else if (prefix == "false") {
            currentBool = false;
        } else {
            throw AnswerInputStreamException(); /* unexpected prefix. check whitespace match */
        }

        string optionsSetString;
        char c;
        getline(in, optionsSetString, '}'); /* read till end of optionsSet set */

        string nextOption; /* each option is build by attaching char by char */
        size_t optionsSetStringIndex = 0; /* index of currently read char in optionsSet set string */
        bool isCurrentlyBuilding = false; /* if currently processing option, or skipping intermediate characters and quotion marks */
        while (optionsSetStringIndex < optionsSetString.size()) {
            c = optionsSetString[optionsSetStringIndex++];
            if (c == '\"') {
                /* if quotion mark encountered; stop building and push to vector */
                if (isCurrentlyBuilding) {
                    /* option built, push to vector */
                    newOptions.push_back(pair<bool, string>(currentBool, nextOption));
                    nextOption = ""; /* clear for next option build */
                }
                isCurrentlyBuilding = !(isCurrentlyBuilding);
            } else {
                /* if quotion mark encountered; start building option string */
                if (isCurrentlyBuilding) {
                    nextOption += c; /* append to string */
                } else {
                    /* ignore whitespaces */
                }
            }
        }
    }
    // single-choice specific
    int trueCount = 0;
    for (const auto &o: options) {
        if (o.first)trueCount++;
    }
    if (trueCount > 1) { throw AnswerUserInputException(); }
    options = newOptions;
}

string SingleChoiceAnswer::falseAnswerOutput() const {
    string r;
    r += "Spravna odpoved: ";
    r+="\n";
    for (const auto& o: keyMappedOptions) {
        if (o.second.first) {
            r += "[";
            r += o.first;
            r += "] ";
            r += o.second.second;
            r += "\n";
        }
    }
    r += "Vase odpoved: ";
    r+="\n";
    for (char choosenKey: chosenOptions) {
        auto o = keyMappedOptions.at(choosenKey);
        r += "[";
        r += choosenKey;
        r += "] ";
        r += o.second;
        r += "\n";
    }

    return r;
}

shared_ptr<Layout>
SingleChoiceAnswer::getSpecificQuestionLayout(const string &drawableId, const Position &relativePosition,
                                              const function<void()> &observer) const {
    auto singlechoice = make_shared<ChoiceAnswerQuestionLayout>(drawableId,relativePosition,observer);
    auto l = dynamic_pointer_cast<Layout>(singlechoice);
    return l;
}

string SingleChoiceAnswer::INPUT_FORMAT() const {
    return "Napiste znak spravne odpovedi (napr.: a): ";
}

shared_ptr<Answer> SingleChoiceAnswer::answerCreatorWizard() const {
    vector<pair<bool,string>> newoptions;
    int responseCount=1;
    string readInput;
    int read;
    cout<<"Kolik byste chteli pridat odpovedi na otazku? ";
    getline(cin,readInput);
    try{
        read = stoi(readInput);
    }catch (exception&e){
        throw WizardFormatInputException();
    }

    int yesCount=0;
    for (int i =0;i<read;i++) {
        string solutionIn;
        string correctIn;
        bool correct = false;
        cout << "Zadejte " << to_string(responseCount++) << ". odpoved na otazku: ";
        getline(cin,solutionIn);
        if(solutionIn.empty()){throw WizardFormatInputException();}
        cout << "\tJe tato odpoved spravne? [ano / ne]: ";
        getline(cin,correctIn);
        if (yes.find(correctIn) == yes.end()) {
            if (no.find(correctIn) == no.end()) {
                throw WizardFormatInputException();
            }
        } else {
            yesCount++;
            if(yesCount>1){
                throw WizardFormatInputException();
            }
            correct = true;
        }
        newoptions.emplace_back(correct, solutionIn);
    }
    auto singlechoice = make_shared<SingleChoiceAnswer>(newoptions);

    auto a = dynamic_pointer_cast<Answer>(singlechoice);
    return a;
}

void SingleChoiceAnswer::outputStream(ostream &os) const {
    os<< "true{";
    for (const auto &answer: options) {
        if (answer.first) {
            os<< "\"";
            os<< answer.second;
            os<< "\",";
        }
    }
    os<< "}";
    os<< "false{";
    for (const auto &answer: options) {
        if (!answer.first) {
            os<< "\"";
            os<< answer.second;
            os<< "\",";
        }
    }
    os<< "}";
}

string SingleChoiceAnswer::DESCRIPTION() const {
    return "Odpoved s vypsanymi moznostmi a jednou spravnou odpovedi";
}
