//
// Created by voky on 22.03.22.
//

#include "ChoiceAnswer.h"

ChoiceAnswer::ChoiceAnswer(const vector<pair<bool, string>> &options) {
    if (options.size() > 26) { throw exception(); } /** exceeded limit of list size. */
    this->options = options;
}

const map<char, pair<bool, string>> &ChoiceAnswer::generateKeyMappedOptions() {
    // STACKOVERFLOW
    unsigned seed = 0;
    // shuffle of optionsSet list.
    shuffle(options.begin(), options.end(), std::default_random_engine(seed));
    // END OF STACKOVERFLOW

    keyMappedOptions.clear();
    char beginChar = 'a';
    for (auto &i: options) {
        keyMappedOptions[beginChar] = i;
        beginChar++;
    }
    return keyMappedOptions;
}

const set<char> &ChoiceAnswer::getChosenOptions() const {
    return chosenOptions;
}

const map<char, pair<bool, string>> &ChoiceAnswer::getKeyMappedOptions() const {
    return keyMappedOptions;
}

bool ChoiceAnswer::correct() const {
    size_t correct = 0;
    /* Check if every correct option is not found in user inputed */
    for (const auto &o: keyMappedOptions) {
        char correctKey = o.first;
        // If inputed option is correct, find in set options
        if (o.second.first) {
            if (chosenOptions.find(correctKey) == chosenOptions.end()) {
                return false;
            }
            correct++;
        }
    }
    /* Check if arent some user inputed optionsSet extra */
    if (correct < chosenOptions.size()) {
        return false;
    }
    return true;
}
