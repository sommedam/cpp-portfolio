//
// Created by voky on 14.4.22.
//

#ifndef QUIZWIZ_ANSWERFACTORY_H
#define QUIZWIZ_ANSWERFACTORY_H


#include "Answer.h"
#include "text_field_answer/TextStrictAnswer.h"
#include "choice_answer/SingleChoiceAnswer.h"
#include "choice_answer/MultichoiceAnswer.h"
#include "number_field_answer/NumberIntervalAnswer.h"
#include "text_field_answer/TextSetAnswer.h"
#include "text_field_answer/TextMisspelledAnswer.h"

class AnswerFactory {
public:
    AnswerFactory() = default;

private:

    const vector<shared_ptr<Answer>> answers = {
            dynamic_pointer_cast<Answer>(make_shared<TextStrictAnswer>()),
            dynamic_pointer_cast<Answer>(make_shared<TextSetAnswer>()),
            dynamic_pointer_cast<Answer>(make_shared<TextMisspelledAnswer>()),
            dynamic_pointer_cast<Answer>(make_shared<NumberIntervalAnswer>()),
            dynamic_pointer_cast<Answer>(make_shared<MultichoiceAnswer>()),
            dynamic_pointer_cast<Answer>(make_shared<SingleChoiceAnswer>())
    };
public:
    static vector<shared_ptr<Answer>> ALL_ANSWERS(){
        vector<shared_ptr<Answer>> answers = {
                dynamic_pointer_cast<Answer>(make_shared<TextStrictAnswer>()),
                dynamic_pointer_cast<Answer>(make_shared<TextSetAnswer>()),
                dynamic_pointer_cast<Answer>(make_shared<TextMisspelledAnswer>()),
                dynamic_pointer_cast<Answer>(make_shared<NumberIntervalAnswer>()),
                dynamic_pointer_cast<Answer>(make_shared<MultichoiceAnswer>()),
                dynamic_pointer_cast<Answer>(make_shared<SingleChoiceAnswer>())
        };
        return answers;
    }

    string ALL_ANSWERS_LIST() {
        string r;
        int index=0;
         for(const auto &a : answers){
             r+="["+ to_string(index)+"] "+a->DESCRIPTION()+"\n";
             index++;
         }
        return r;
    }

    [[nodiscard]] function<shared_ptr<Answer>()> ANSWER_CREATOR_OF_INDEX(size_t index) const{
        function<shared_ptr<Answer>()> r=[this,index](){
            return answers.at(index)->answerCreatorWizard();
        };
        return r;
    }
};


#endif //QUIZWIZ_ANSWERFACTORY_H
