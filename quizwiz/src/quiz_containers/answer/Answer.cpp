//
// Created by voky on 04.04.22.
//

#include <ostream>
#include <memory>
#include "Answer.h"

string Answer::TAG() const {
    // Placeholder
    return DESCRIPTION();
}

std::ostream &operator<<(ostream &str, const Answer &a) {
    a.outputStream(str);
    return str;
}

std::ostream &operator<<(ostream &str, const shared_ptr<Answer> &a) {
    a->outputStream(str);
    return str;
}

std::istream &operator>>(istream &istr, Answer &a) {
    a.inputStream(istr);
    return istr;
}

std::istream &operator>>(istream &istr, shared_ptr<Answer> &a) {
    a->inputStream(istr);
    return istr;
}
