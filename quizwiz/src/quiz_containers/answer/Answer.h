//
// Created by voky on 04.04.22.
//

#ifndef QUIZWIZ_ANSWER_H
#define QUIZWIZ_ANSWER_H

#include "../../gui/graphical/drawable/layout/Layout.h"

using namespace std;

/**
 * Abstract class, is part of @class Question
 */
class Answer {
public:
    /**
     * Checks if user input is correct
     * @return boolean of value true if input correct
     */
    [[nodiscard]] virtual bool correct() const = 0;

    /**
     * Set user input
     * @param string i raw string from console input
     */
    virtual void setInput(string i) = 0;

    /**
     * Create summary of fails in user answered input
     * @return string of listed fails
     */
    [[nodiscard]] virtual string falseAnswerOutput() const = 0;

    /**
     * Creates layout of given Answer type.
     * @param drawableId id of created layout drawable
     * @param relativePosition relative position of created layout drawable
     * @param observer observer of created layout drawable
     * @return shared pointer of dynamicaly casted specific project_layouts
     */
    virtual shared_ptr<Layout> getSpecificQuestionLayout(const string &drawableId, const Position &relativePosition,
                                                         const function<void()> &observer) const = 0;

    /**
     * Starts sequence of steps to create specific answer
     * @attention method is using stdin, before calling stop any input listeners
     * @return shared pointer to created Answer
     */
    [[nodiscard]] virtual shared_ptr<Answer> answerCreatorWizard() const = 0;

    /**
     * Description of answer type
     * @return string of description
     */
    [[nodiscard]] virtual string DESCRIPTION() const = 0;

    /**
     * Name of type
     * @return string of TAG
     */
    [[nodiscard]] virtual string TAG()const;

    /**
     * Outputs format for export of Answer
     * @param str output stream to be writen
     * @param a Answer to be exported
     * @return output stream with appended export
     */
    friend std::ostream &operator<<(std::ostream &str, Answer const &a);
    friend std::ostream &operator<<(std::ostream &str, shared_ptr<Answer> const &a);

    /**
     * Imports Answer in stream format
     * @param istr input stream of Answer
     * @param a Answer to be writen to
     * @return input stream seeked by read characters
     */
    friend std::istream &operator>>(std::istream &istr, Answer &a);
    friend std::istream &operator>>(std::istream &istr, shared_ptr<Answer>& a);

    /**
     * Specifies format of user input
     * @return string of format pattern
     */
    [[nodiscard]] virtual string INPUT_FORMAT() const = 0;


protected:
    /**
     * substitude for >> operator
     */
    virtual void inputStream(std::istream &in) = 0;

    /**
     * substitude for << operator
     */
    virtual void outputStream(std::ostream &os) const = 0;
};



#endif //QUIZWIZ_ANSWER_H
