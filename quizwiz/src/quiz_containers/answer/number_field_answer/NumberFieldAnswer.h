//
// Created by voky on 1.6.22.
//

#ifndef QUIZWIZ_NUMBERFIELDANSWER_H
#define QUIZWIZ_NUMBERFIELDANSWER_H


#include "../Answer.h"
#include "../exceptions/AnswerUserInputException.h"

class NumberFieldAnswer : public Answer {
protected:
    int input=0;
    bool inputSet = false; /* answer is incorrect, if no value set. default value of input could impact quiz result calculation */

public:
    /**
     * Set number input. Other than number inputs will throw @link AnswerUserInputException.h
     * @param i raw user entered input
     */
    void setInput(string i) override {
        inputSet = true;
        if (i.empty()) {
            throw AnswerUserInputException();
        }
        try {
            input = stoi(i);
            inputSet = true;
        } catch (exception &exception) {
            throw AnswerUserInputException();
        };
    }
};



#endif //QUIZWIZ_NUMBERFIELDANSWER_H
