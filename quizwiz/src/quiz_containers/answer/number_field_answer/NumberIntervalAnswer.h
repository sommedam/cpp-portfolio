//
// Created by voky on 23.03.22.
//

#ifndef QUIZWIZ_NUMBERINTERVALANSWER_H
#define QUIZWIZ_NUMBERINTERVALANSWER_H
#include "sstream"
#include "../exceptions/AnswerUserInputException.h"
#include "../../../gui/graphical/drawable/layout/Layout.h"
#include "NumberFieldAnswer.h"

using namespace std;

/**
 *  Answer with open user input. Input is number, correct if number in defined interval.
 */
class NumberIntervalAnswer: public NumberFieldAnswer{
    std::pair<int,int> interval;

public:

    [[nodiscard]] string TAG() const override;

    NumberIntervalAnswer() = default;

    explicit NumberIntervalAnswer(const pair<int, int> &intervalAnswer);


/**
 * @return boolean of value true if input in defined interval, else returns false
 */
[[nodiscard]] bool correct() const override;


public:
    /**
    * @param os output in format: interval_lower;interval_higher
    */
    void outputStream(ostream &os) const override;

    /**
     * @param in input in format: interval_lower;interval_higher
     */
    void inputStream(istream &in) override;

    shared_ptr<Layout> getSpecificQuestionLayout(const string &drawableId, const Position &relativePosition,
                                                 const function<void()> &observer)const override;

    [[nodiscard]] string falseAnswerOutput() const override;
    [[nodiscard]] shared_ptr<Answer> answerCreatorWizard()const override;
    [[nodiscard]] string DESCRIPTION() const override;
    [[nodiscard]] string INPUT_FORMAT() const override;

    void setInterval(const pair<int, int> &interval);
};


#endif //QUIZWIZ_NUMBERINTERVALANSWER_H
