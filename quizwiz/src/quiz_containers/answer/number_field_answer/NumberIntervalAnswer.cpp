//
// Created by voky on 23.03.22.
//

#include "NumberIntervalAnswer.h"
#include "../../../gui/graphical/drawable/layout/project_layouts/TextAnswerQuestionLayout.h"
#include "../exceptions/AnswerInputStreamException.h"
#include "../exceptions/WizardFormatInputException.h"

NumberIntervalAnswer::NumberIntervalAnswer(const pair<int, int> &intervalAnswer) : interval(intervalAnswer) {}

bool NumberIntervalAnswer::correct() const {
    if(!inputSet){return false;} /* answer is required */
    return (input <= interval.second && input >= interval.first);
}

void NumberIntervalAnswer::outputStream(ostream &os) const {
    os << to_string(interval.first);
    os << ";";
    os << to_string(interval.second);
}

void NumberIntervalAnswer::inputStream(istream &in) {
    char c;
    bool buildingFirst= true; /* if processing string of left number in interval, or second */
    string first,second;
    while(in>>c){
        if(c==';'){
            buildingFirst=false;
            continue;
        }
        if(buildingFirst){
            first+=c;
        }else{
            second+=c;
        }
    }
    try {
        interval.first = stoi(first);
        interval.second = stoi(second);
    }catch(std::invalid_argument &arg) {
        throw AnswerInputStreamException();
    }catch (std::out_of_range &arg){
        throw AnswerInputStreamException();
    }
    if(interval.first > interval.second){
        throw AnswerInputStreamException();
    }
}

shared_ptr<Layout>
NumberIntervalAnswer::getSpecificQuestionLayout(const string &drawableId, const Position &relativePosition,
                                                const function<void()> &observer) const {
    auto layout  =  make_shared<TextAnswerQuestionLayout>(drawableId,relativePosition,observer);
    auto r = dynamic_pointer_cast<Layout>(layout);
    return r;
}

string NumberIntervalAnswer::falseAnswerOutput() const {
    string response;
    response +=
            "Spravna odpoved v rozsahu: " + to_string(interval.first) + " az " + to_string(interval.second)
            + "\nVase odpoved : " + to_string(input);
    response+"\n";
    return response;
}

string NumberIntervalAnswer::INPUT_FORMAT() const {
    return "Vypiste celociselnou odpoved, vysledek bude bran s rezervou: ";
}

shared_ptr<Answer> NumberIntervalAnswer::answerCreatorWizard() const {
    string downInput,upInput;
    cout<<"Zadejte celociselnou dolni zavoru: ";
    getline(cin, downInput);
    cout<<"Zadejte celociselnou horni zavoru: ";
    getline(cin, upInput);
    int down,up;
    try{
        down = stoi(downInput);
        up = stoi(upInput);
        if(down>up){
            throw exception();
        }
    }catch (exception & e){
        throw WizardFormatInputException();
    }
    auto textintervalanswer= make_shared<NumberIntervalAnswer>(pair<int,int>(down, up));
    auto a = dynamic_pointer_cast<Answer>(textintervalanswer);
    return a;
}

string NumberIntervalAnswer::DESCRIPTION() const {
    return "Otevrena celociselna odpoved v definovanem intervalu";
}

string NumberIntervalAnswer::TAG() const {return "interval";}

void NumberIntervalAnswer::setInterval(const pair<int, int> &interval) {
    NumberIntervalAnswer::interval = interval;
}
