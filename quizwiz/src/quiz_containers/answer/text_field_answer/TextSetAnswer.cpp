//
// Created by voky on 22.5.22.
//

#include "TextSetAnswer.h"
#include "../exceptions/AnswerUserInputException.h"
#include "../../../gui/graphical/drawable/layout/project_layouts/TextAnswerQuestionLayout.h"
#include "../exceptions/WizardFormatInputException.h"

bool TextSetAnswer::correct() const {
    return optionsSet.find(input) != optionsSet.end();
}

string TextSetAnswer::falseAnswerOutput() const {
    string r;
    r+="Spravne odpovedi:";
    auto optionsBegin = optionsSet.begin();
    for (size_t i = 0; i < optionsSet.size(); ++i) {
        r+=optionsBegin.operator*()+"\n";
        optionsBegin++;
    }
    r+="Vase odpoved: "+input;
    r+="\n";
    return r;
}

shared_ptr<Layout> TextSetAnswer::getSpecificQuestionLayout(const string &drawableId, const Position &relativePosition,
                                                            const function<void()> &observer) const {
    auto r = make_shared<TextAnswerQuestionLayout>(drawableId,relativePosition,observer);
    return r;
}

shared_ptr<Answer> TextSetAnswer::answerCreatorWizard() const {
    auto r = make_shared<TextSetAnswer>();
    cout<<"Kolik chcete zadat odpovedi v mnozine? ";
    string countInput;
    getline(cin,countInput);
    int questionCount;
    try{
        questionCount = stoi(countInput);
        if(questionCount==0){
            throw WizardFormatInputException();
        }
    }catch (exception & e){
        throw WizardFormatInputException();
    }
    for(int i =0; i<questionCount;i++) {
        cout << "Zadejte " << (i+1) << ". moznou odpoved: ";
        string option;
        getline(cin, option);
        if(option.empty()){
            throw WizardFormatInputException();
        }
        r->addToOptions(option);
    }
    return r;
}

string TextSetAnswer::DESCRIPTION() const {
    return "Otevrena odpoved v zadane mnozine";
}

string TextSetAnswer::TAG() const {
    return "set";
}

string TextSetAnswer::INPUT_FORMAT() const {
    return "Zadejte ciselnou odpoved: ";
}

void TextSetAnswer::inputStream(istream &in) {
    while (!in.eof()) {
        string option;
        getline(in, option,';');
        optionsSet.insert(option);
    }
}

void TextSetAnswer::outputStream(ostream &os) const {
    auto optionsIterator = optionsSet.begin();
    for(size_t i =0; i<optionsSet.size();i++){
        os<<*optionsIterator<<";";
        optionsIterator++;
    }
}

void TextSetAnswer::addToOptions(const string &option) {
    optionsSet.insert(option);
}
