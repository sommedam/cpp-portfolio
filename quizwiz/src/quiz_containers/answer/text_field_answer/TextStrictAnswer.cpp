//
// Created by voky on 23.03.22.
//

#include "sstream"
#include "TextStrictAnswer.h"
#include "../../../gui/graphical/drawable/layout/project_layouts/TextAnswerQuestionLayout.h"
#include "../exceptions/AnswerUserInputException.h"
#include "../exceptions/WizardFormatInputException.h"


void TextStrictAnswer::inputStream(istream &in) {
    char c;
    while (in >> c) {
        answer += c;
    }
}

bool TextStrictAnswer::correct() const {
    return (answer == TextFieldAnswer::input);
}

shared_ptr<Layout>
TextStrictAnswer::getSpecificQuestionLayout(const string &drawableId, const Position &relativePosition,
                                            const function<void()> &observer) const {
    auto layout = make_shared<TextAnswerQuestionLayout>(drawableId, relativePosition, observer);
    auto r = dynamic_pointer_cast<Layout>(layout);
    return r;
}

void TextStrictAnswer::outputStream(ostream &os) const {
    os << answer;
}

shared_ptr<Answer> TextStrictAnswer::answerCreatorWizard() const {
    string response;
    cout << "Zadejte spravnou odpoved: ";
    std::getline(std::cin, response);
    if(response.empty()){
        throw WizardFormatInputException();
    }

    auto textstrict = make_shared<TextStrictAnswer>();
    textstrict->setAnswer(response);
    auto a = dynamic_pointer_cast<Answer>(textstrict);
    return a;
}

void TextStrictAnswer::setAnswer(const string &answer) {
    TextStrictAnswer::answer = answer;
}

TextStrictAnswer::TextStrictAnswer(const string &answer) : answer(answer) {}

TextStrictAnswer::TextStrictAnswer() {}
