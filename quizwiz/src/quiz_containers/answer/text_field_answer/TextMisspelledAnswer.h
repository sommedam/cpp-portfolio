//
// Created by voky on 1.6.22.
//

#ifndef QUIZWIZ_TEXTMISSPELLEDANSWER_H
#define QUIZWIZ_TEXTMISSPELLEDANSWER_H


#include "TextFieldAnswer.h"
using namespace std;

/**
 *  Answer with open user input. Creator can define how much misspells are tolerated.
 */
class TextMisspelledAnswer: public TextFieldAnswer {
    string answer;
    int misspellsAllowed;


private:
    [[nodiscard]] bool isStringNearlyMatching(const string& a, const string& b) const;

public:
    /**
     * @param in input string of answer
     */
    void inputStream(istream &in) override;

    /**
    * @param in output string of answer
    */
    void outputStream(ostream &os) const override;

    shared_ptr<Layout> getSpecificQuestionLayout(const string &drawableId, const Position &relativePosition,
                                                 const function<void()> &observer) const override;


    [[nodiscard]] shared_ptr<Answer> answerCreatorWizard() const override;

    [[nodiscard]] string falseAnswerOutput() const override;

    /**
     * @return boolean of value true, if input is strictly matching defined correct answer
     */
    [[nodiscard]] bool correct() const override;

    [[nodiscard]] string INPUT_FORMAT() const override;

    [[nodiscard]] string DESCRIPTION() const override;

    void setMisspellsAllowed(int misspellsAllowed);

    void setAnswer(const string &answer);
};


#endif //QUIZWIZ_TEXTMISSPELLEDANSWER_H
