//
// Created by voky on 1.6.22.
//

#include "TextMisspelledAnswer.h"
#include "../exceptions/WizardFormatInputException.h"
#include "../../../gui/graphical/drawable/layout/project_layouts/TextAnswerQuestionLayout.h"
#include "../exceptions/AnswerInputStreamException.h"

void TextMisspelledAnswer::inputStream(istream &in) {
    getline(in,answer,';');
    string misspelledString;
    getline(in,misspelledString);
    try {
        misspellsAllowed = stoi(misspelledString);
    }catch(exception&e) {
        throw AnswerInputStreamException();
    }
    /*char c;
    bool buildingMisspelledNumber= false;
    string misspelledString;
    while (in >> c) {
        if(c==';') {
            buildingMisspelledNumber = true;
            continue;
        }
        if(!buildingMisspelledNumber) {
            answer += c;
        }else{
            misspelledString+=c;
        }
    }
    try {
        misspellsAllowed = stoi(misspelledString);
    }catch(exception&e) {
        throw AnswerInputStreamException();
    }*/
}

bool TextMisspelledAnswer::isStringNearlyMatching(const string& a, const string& b) const {
    int missmatchesCount=0;
    if (a.length() != b.length()) {
        return false;
    } else {
        for (long unsigned int i = 0; i < a.length(); i++) {
            if (a[i] != b[i]) {
                missmatchesCount++;
                if(missmatchesCount>misspellsAllowed)
                    return false;
            }
        }
    }
    return true;
}

bool TextMisspelledAnswer::correct() const {
    return isStringNearlyMatching(input,answer);
}

shared_ptr<Layout>
TextMisspelledAnswer::getSpecificQuestionLayout(const string &drawableId, const Position &relativePosition,
                                            const function<void()> &observer) const {
    auto layout = make_shared<TextAnswerQuestionLayout>(drawableId, relativePosition, observer);
    auto r = dynamic_pointer_cast<Layout>(layout);
    return r;
}

void TextMisspelledAnswer::outputStream(ostream &os) const {
    os << answer << ";"<<to_string(misspellsAllowed);
}

shared_ptr<Answer> TextMisspelledAnswer::answerCreatorWizard() const {
    string answerInput;
    cout << "Zadejte spravnou odpoved: ";
    std::getline(std::cin, answerInput);
    if(answerInput.empty()){
        throw WizardFormatInputException();
    }

    size_t misspellsAllowedInt;
    string misspellsAllowedInput;
    cout << "Kolik chcete prominout prekliku: ";
    getline(cin,misspellsAllowedInput);
    try{
        misspellsAllowedInt = stoi(misspellsAllowedInput);
    }catch(exception&e){
        throw WizardFormatInputException();
    }
    /* misspells must be lower than actual answer word */
    if(misspellsAllowedInt >= answerInput.size()){
        throw WizardFormatInputException();
    }

    auto textmisspelled = make_shared<TextMisspelledAnswer>();
    textmisspelled->setAnswer(answerInput);
    textmisspelled->setMisspellsAllowed(misspellsAllowedInt);
    auto a = dynamic_pointer_cast<Answer>(textmisspelled);
    return a;
}

string TextMisspelledAnswer::INPUT_FORMAT() const {
    return "Zadejte odpoved, nekolik prekliku bude tolerovano: ";
}

string TextMisspelledAnswer::DESCRIPTION() const {
    return "Otevrena odpoved, ve ktere je mozne udelat x preklepu";
}

string TextMisspelledAnswer::falseAnswerOutput() const {
    string response;
    response += "Spravna odpoved: " + answer + "\nVase odpoved: " + input;
    return response;
}

void TextMisspelledAnswer::setMisspellsAllowed(int misspellsAllowed) {
    TextMisspelledAnswer::misspellsAllowed = misspellsAllowed;
}

void TextMisspelledAnswer::setAnswer(const string &answer) {
    TextMisspelledAnswer::answer = answer;
}
