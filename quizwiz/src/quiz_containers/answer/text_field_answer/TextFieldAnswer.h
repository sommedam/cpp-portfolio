//
// Created by voky on 10.5.22.
//

#ifndef QUIZWIZ_TEXTFIELDANSWER_H
#define QUIZWIZ_TEXTFIELDANSWER_H

#include "../Answer.h"

using namespace std;

class TextFieldAnswer : public Answer {
protected:
    /** raw user entered input */
    string input;

public:
    void setInput(string i) override;


};



#endif //QUIZWIZ_TEXTFIELDANSWER_H
