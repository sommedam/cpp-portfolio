//
// Created by voky on 22.5.22.
//

#ifndef QUIZWIZ_TEXTSETANSWER_H
#define QUIZWIZ_TEXTSETANSWER_H


#include "TextFieldAnswer.h"

/**
 * Answer correct if user input in given set of correct options
 */
class TextSetAnswer : public TextFieldAnswer {
    set<string> optionsSet;

public:
    [[nodiscard]] bool correct() const override;

    [[nodiscard]] string falseAnswerOutput() const override;

    shared_ptr<Layout> getSpecificQuestionLayout(const string &drawableId, const Position &relativePosition,
                                                 const function<void()> &observer) const override;

    [[nodiscard]] shared_ptr<Answer> answerCreatorWizard() const override;

    [[nodiscard]] string DESCRIPTION() const override;

    [[nodiscard]] string TAG() const override;

    [[nodiscard]] string INPUT_FORMAT() const override;

public:
    void addToOptions(const string & option);

protected:
    void inputStream(istream &in) override;

    void outputStream(ostream &os) const override;
};


#endif //QUIZWIZ_TEXTSETANSWER_H
