//
// Created by voky on 23.03.22.
//

#ifndef QUIZWIZ_TEXTSTRICTANSWER_H
#define QUIZWIZ_TEXTSTRICTANSWER_H


#include <utility>

#include "TextFieldAnswer.h"
#include "../../../gui/graphical/drawable/layout/Layout.h"

/**
 *  Answer with open user input. String input is correct, if strictly matching defined string.
 */
class TextStrictAnswer: public TextFieldAnswer {
    string answer;

public:
    TextStrictAnswer();

    TextStrictAnswer(const string &answer);

    static string TAGs(){return "strict";}

    /**
     * @param in input string of answer
     */
    void inputStream(istream &in) override;

    /**
    * @param in output string of answer
    */
    void outputStream(ostream &os) const override;

    shared_ptr<Layout> getSpecificQuestionLayout(const string &drawableId, const Position &relativePosition,
                                                 const function<void()> &observer) const override;


    [[nodiscard]] shared_ptr<Answer> answerCreatorWizard() const override;

    [[nodiscard]] string falseAnswerOutput() const override {
        string response;
        response += "Spravna odpoved: " + answer + "\nVase odpoved: " + input;
        return response;
    }

    /**
     * @return boolean of value true, if input is strictly matching defined correct answer
     */
    [[nodiscard]] bool correct() const override;

    [[nodiscard]] string INPUT_FORMAT() const override {
        return "Zadejte odpoved v presnem zneni: ";
    }

    [[nodiscard]] string DESCRIPTION() const override {
        return "Otevrena odpoved, ve ktere je spravne jen uplna shoda zadaneho textu";
    }

    void setAnswer(const string &answer);
};


#endif //QUIZWIZ_TEXTSTRICTANSWER_H
