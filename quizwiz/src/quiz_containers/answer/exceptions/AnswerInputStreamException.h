//
// Created by voky on 23.5.22.
//

#ifndef QUIZWIZ_ANSWERINPUTSTREAMEXCEPTION_H
#define QUIZWIZ_ANSWERINPUTSTREAMEXCEPTION_H


#include <stdexcept>

/**
 * Exception indicating fail while importing from stream
 */
class AnswerInputStreamException: public std::invalid_argument {
public:
    AnswerInputStreamException() :
            invalid_argument("AnswerInputStreamException") {}
};


#endif //QUIZWIZ_ANSWERINPUTSTREAMEXCEPTION_H
