//
// Created by voky on 13.04.22.
//

#ifndef QUIZWIZ_ANSWERUSERINPUTEXCEPTION_H
#define QUIZWIZ_ANSWERUSERINPUTEXCEPTION_H

#include <stdexcept>

using namespace std;
/**
 * Exception indicating unexpected input when solving Answers
 */
class AnswerUserInputException: public invalid_argument {
public:
    AnswerUserInputException():
            invalid_argument ( "input error" ){}
};


#endif //QUIZWIZ_ANSWERUSERINPUTEXCEPTION_H
