//
// Created by voky on 14.04.22.
//

#ifndef QUIZWIZ_WIZARDFORMATINPUTEXCEPTION_H
#define QUIZWIZ_WIZARDFORMATINPUTEXCEPTION_H

#include <stdexcept>

using namespace std;
/**
 * Exception indicating unexpected input when creating Answer
 */
class WizardFormatInputException: public invalid_argument  {
public:
    WizardFormatInputException():
            invalid_argument ( "Wizard format input error" ){}

};


#endif //QUIZWIZ_WIZARDFORMATINPUTEXCEPTION_H
