//
// Created by voky on 23.03.22.
//

#include "Question.h"

void Question::setHeader(const string &h) {
    Question::header = h;
}

void Question::setAnswer(const shared_ptr<Answer> &a) {
    Question::answer = a;
}

bool Question::correct() {
    return answer->correct();
}

const string &Question::getHeader() const {
    return header;
}

const shared_ptr<Answer> &Question::getAnswer() const {
    return answer;
}

ostream &operator<<(ostream &os, Question &q) {
    os<<R"(question:"header"=')"+q.header+"\'";
    os<<"\n";
    os<<R"(answer:"type"=')";
    os<<q.answer->DESCRIPTION();
    os<<"\'";
    os<<R"(,"response"=')";
    os<<q.answer;
    os<<"\'\n";
    return os;
}
