//
// Created by voky on 14.04.22.
//

#include "WizardInputOutput.h"
#include "../page/page_policy/PagePolicyFactory.h"
#include "../answer/AnswerFactory.h"

Quiz WizardInputOutput::decodeFromFile(const string &filename) {
    PagePolicyFactory pagePolicyFactory;

    // Neva ze se znova inicializuje, decode se za beh aplikace provadi jen obcas
    // staticka metoda dava smysl
    const vector<shared_ptr<Answer>> allAnswers = AnswerFactory::ALL_ANSWERS();

    Quiz r;
    fstream quizFile;
    quizFile.open("saved_quizwiz/"+filename, ios::in);
    if (!quizFile) {
        cout << "No such file";
    } else {
        string line;
        getline(quizFile, line);
        if (line != "quizwiz") { throw WizardFormatInputException(); }

        getline(quizFile, line);
        auto firstPageInput = processLine(line);
        if(firstPageInput.first != "page"){throw WizardFormatInputException();}
        shared_ptr<Page> currentPage= make_shared<Page>(firstPageInput.second.at("name"));

        // "policy"='message;Hej neuspesny resiteli'
        string policyPayload = firstPageInput.second.at("policy");
        istringstream policyPayloadStream(policyPayload);
        policyPayloadStream >> std::noskipws;
        string policyTag;
        getline(policyPayloadStream,policyTag,';');
        string policyBundle;
        getline(policyPayloadStream,policyBundle);
        if(!policyTag.empty()){
        auto blankPolicy=pagePolicyFactory.NEW_PAGE_BY_TAG(policyTag);
            istringstream policyBundleStream(policyBundle);
            policyBundleStream>>blankPolicy;
            currentPage->setPolicy(blankPolicy);
        }

        while (getline(quizFile, line) && !line.empty()) {
            auto p = processLine(line);
            if(p.first=="page"){
                // Vlozit prave budovanout stranku do kvizu
                r.addPage(currentPage);
                // Zlozit novou stranku
                // todo first page
                currentPage= make_shared<Page>(p.second.find("name")->second);
               // currentPage->addFlags(p.second.find("FLAGS")->second);
            }else if(p.first=="question"){
                Question q;
                q.setHeader(p.second.find("header")->second);
                // Kazda otazka ma svoji odpoved
                getline(quizFile, line);
                auto answerInput = processLine(line);
                if(answerInput.first!="answer"){throw WizardFormatInputException();}
                string type = answerInput.second.find("type")->second;
                // Todo: zde se pokazde inicializuji vsechny mozne odpovedi. Performance.
                for(auto& answer:AnswerFactory::ALL_ANSWERS()){
                    if(answer->DESCRIPTION()==type){
                        istringstream iss(answerInput.second.find("response")->second);
                        iss>>answer;
                        q.setAnswer(answer);
                        break;
                    }
                }
                currentPage->addQuestion(q);
            }
        }
        r.addPage(currentPage);
    }
    cout<<"\n";
    cout<<r;
    quizFile.close();
    return r;
}

pair<string, map<string, string>> WizardInputOutput::processLine(const string &line) {
    string tag;
    map<string, string> attributeMap;

    istringstream lineStream(line);
    lineStream >> std::noskipws;

    getline(lineStream, tag, ':');
    char c;
    // Pro kazdy atribut s jeho hodnotou
    while (!lineStream.eof()) {
        string attribute;
        string attributeValue;

        if (lineStream.peek() == -1) { break; }

        // Preskocit bile znaky a precist uvozovku
        while (lineStream >> c && c != '\"') {
            if (c != ' ' && c != ',') {
                throw WizardFormatInputException();
            }
        }

        // Dokud nenajde parovou uvozovku cist nazev atributu
        while (lineStream >> c && c != '\"') {
            attribute += c;
        }
        // Preskocit bile znaky a precist uvozovku
        bool wasEqualCharRead = false;
        while (lineStream >> c && c != '\'') {
            if (c != ' ' && c != '=' && c != ',') {
                throw WizardFormatInputException();
            } else {
                // Dvojite rovnase je povoleno
                if (c == '=')wasEqualCharRead = true;
            }
        }
        // Mezi nazvem atributu a jeho hodnotou musi byt znamenko rovnase
        if (!wasEqualCharRead)throw WizardFormatInputException();

        // Dokud nenarazi na konec uvozovky
        while (lineStream >> c && c != '\'') {
            attributeValue += c;
        }
        attributeMap[attribute] = attributeValue;
    }
    //lineStream>>tag>>delim;

    return pair<string, map<string, string>>(tag, attributeMap);
}
