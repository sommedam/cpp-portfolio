//
// Created by voky on 14.04.22.
//

#ifndef QUIZWIZ_WIZARDINPUTOUTPUT_H
#define QUIZWIZ_WIZARDINPUTOUTPUT_H

#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
#include "../answer/exceptions/WizardFormatInputException.h"
#include "../Quiz.h"

class WizardInputOutput {
public:
    static
    Quiz decodeFromFile(const string &filename);

    /**
     * Extrahuje ze radku tag a vsechny vlastnosti ktere ma namapuje
     * @return vraci nazev tagu a namapovane vlastnosti s jejich hodnotami
     * <nazev_tagu, map<vlastnost,hodnota_vlastnosti>>
     */
    static pair<string, map<string, string>> processLine(const string& line);

};


#endif //QUIZWIZ_WIZARDINPUTOUTPUT_H
