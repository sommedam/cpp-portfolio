//
// Created by voky on 23.03.22.
//

#include "Quiz.h"


std::ostream &operator<<(ostream &os, const Quiz &x) {
    os << "quizwiz";
    os << "\n";
    for (auto &p: x.pages) {
        os << p;
    }
    return os;
}

PageIterator Quiz::getPageIterator(PagePolicyBuilder & builder) {
    return {pages.begin(), pages.end(),builder};
}

PageIterator Quiz::getPageIterator() {
    return {pages.begin(), pages.end()};
}

void Quiz::addPage(const shared_ptr<Page> &page) {
    pages.emplace_back(page);
}

int Quiz::getQuestionsCount() {
    int r = 0;
    for (const auto &p: pages) {
        r += p->getQuestions().size();
    }
    return r;
}

int Quiz::getCorrectCount() {
    int r = 0;
    for (const auto &p: pages) {
        for (const auto &q: p->getQuestions()) {
            if (q.getAnswer()->correct()) { r++; }
        }
    }
    return r;
}

string Quiz::failAnswersOutputAggregated() {
    string r;
    for (const auto &p: pages) {
        if (!p->correct()) {
            r += p->getPageName() + "\n";
            for (const auto &q: p->getQuestions()) {
                if (!q.getAnswer()->correct()) {
                    r += q.getHeader()+"\n";
                    r += q.getAnswer()->falseAnswerOutput() + "\n";
                }
            }
            r += "\n";
        }
    }
    return r;
}

const vector<shared_ptr<Page>> &Quiz::getPages() const {
    return pages;
}

