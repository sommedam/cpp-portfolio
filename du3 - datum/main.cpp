#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <stdexcept>
#endif /* __PROGTEST__ */
using namespace std;

void dateAssertAdd();
void dateAssertSub();
void incrementDecrementTest();
void assignTest();
void substractOfTwoTest();
int daysInMonth(int m,int y);
enum{January=1,February,March,April,May,June,
        July,August,September,October,November,December};

bool validateDaysInMonth(int m,int d,int y);
bool leapYear(int y);
//=================================================================================================
// a dummy exception class, keep this implementation
class InvalidDateException : public invalid_argument
{
public:
    InvalidDateException ( )
            : invalid_argument ( "invalid date or format" )
    {
    }
};
//=================================================================================================
// date_format manipulator - a dummy implementation. Keep this code unless you implement your
// own working manipulator.
ios_base & ( * date_format ( const char * fmt ) ) ( ios_base & x )
{
    return [] ( ios_base & ios ) -> ios_base & { return ios; };
}

//=================================================================================================

class CDate {
    int y;
    int m;
    int d;
public:
    CDate(int y, int m, int d) : y(y), m(m), d(d) {
        if (!validateDaysInMonth(m, d, y)) {
            throw InvalidDateException();
        }
        if (y > 9999 || y < 1000) {
            throw InvalidDateException();
        }

    }

    friend istream& operator>>(istream  &is, CDate &date) {
        string smonth, syear, sday;
        int imonth=0,iyear=0,iday=0;

        getline(is, syear, '-');
        getline(is, smonth, '-');
        getline(is, sday, '-');

        try {
            iyear = stoi(syear);
            imonth= stoi(smonth);
            iday = stoi(sday);
        }catch(invalid_argument & a) {
            is.setstate(std::ios::failbit);
        }catch (out_of_range & b){
            is.setstate(std::ios::failbit);
        }
        if(!validateDaysInMonth(imonth,iday,iyear)){
            is.setstate(std::ios::failbit);
        }else{
            date.m=imonth;
            date.y=iyear;
            date.d=iday;
        }
        return is;
    }

    friend std::ostream& operator<<(std::ostream &os, const CDate &x){
        os << x.y << "-";
        if (x.m < 10) { os << "0"; }
        os << x.m << "-";
        if (x.d < 10) { os << "0"; }
        os << x.d;
        return os;
    }

    // postfix
    CDate operator++(int)
    {
        CDate old = *this;
        operator++();
        return old;
    }

    // postfix
    CDate operator--(int)
    {
        CDate old = *this;
        operator--();
        return old;
    }

    // prefix
    CDate& operator--() {
        d--;
        if (d < 1) {
            m--;
            if (m < 1) {
                m = 12;
                y--;
            }
            d = daysInMonth(m - 1, y);
        }
        return *this;
    }

    // prefix
    CDate& operator++() {
            d++;
            if (d > daysInMonth(m, y)) {
                m++;
                d = 1;
                if (m > 12) {
                    m = 1;
                    y++;
                }
            }
            return *this;
    }
    bool operator<=(const CDate &rhs) const{
        return (*this<rhs||*this==rhs);
    }

    bool operator>=(const CDate &rhs) const{
        return (*this>rhs||*this==rhs);
    }
    bool operator!=(const CDate &rhs) const{
        return !(*this==rhs);
    }

    bool operator==(const CDate &rhs) const {
        if (this->m == rhs.m &&
            this->d == rhs.d &&
            this->y == rhs.y) {
            return true;
        } else {
            return false;
        }
    }
    CDate operator+(const int &right) const {
        // Prekopirovat obsah
        CDate r = *this;
        int daysToAdd = right;
        // Pocet dni do posledniho dne toho mesice
        int diff = daysInMonth(r.m, r.y) - r.d;
        if (daysToAdd <= diff) {
            r.d = r.d + daysToAdd;
            return r;
        } else {
            daysToAdd -= diff + 1;
            r.d = 1;
            r.m = r.m + 1;
            if (r.m > 12) {
                r.m = 1;
                r.y = r.y + 1;
            }
        }

        while (daysToAdd >= daysInMonth(r.m, r.y)) {
            daysToAdd -= daysInMonth(r.m, r.y);
            r.m = r.m + 1;
            if (r.m > 12) {
                r.m = 1;
                r.y = r.y + 1;
            }
        }

        r.d = r.d + daysToAdd;
        return r;
    }
    int operator-(const CDate &right) const {
        // LEFT je vetsi RIGHT je mensi
        CDate left= *this;
        CDate lesser(1970,1,1);
        CDate greater(1970,1,1);
        if(left<right){
            lesser=left;
            greater=right;
        }else{
            lesser=right;
            greater=left;
        }

        if(lesser.y == greater.y){
            if(lesser.m == greater.m){
                return greater.d-lesser.d;
            }else{
                // kazdy v jinem mesici
                // pocet do konce mesice
                int r = daysInMonth(lesser.m, lesser.y) - lesser.d;
                while(++lesser.m != greater.m){
                    r+= daysInMonth(lesser.m, lesser.y);
                }
                r+= greater.d;
                return r;
            }
        }else {
            // nejsou stejne roky
            int tillYearEnd = daysInMonth(lesser.m, lesser.y) - lesser.d;
            for (int i = lesser.m + 1; i <= 12; i++) {
                tillYearEnd += daysInMonth(i, lesser.y);
            }
            int daysBetweenYears = 0;
            for (int i = lesser.y + 1; i < greater.y; i++) {
                for(int j =1;j<=12;j++){
                    daysBetweenYears+= daysInMonth(j,i);
                }
            }
            int daysFromYearStart=0;
            int month=1;
            while(month != greater.m){
                daysBetweenYears+= daysInMonth(month,greater.y);
                month++;
            }
            daysFromYearStart+=greater.d;
            return tillYearEnd+daysBetweenYears+daysFromYearStart;
        }
    }
    bool operator>(const CDate &right)const {
        return (right<*this);
    }

    bool operator<(const CDate &right)const {
        if (this->y < right.y) {
            return true;
        } else if (this->y == right.y) {
            if (this->m < right.m) {
                return true;
            } else if(this->m==right.m) {
                if (this->d < right.d) {
                    return true;
                }
            }
        }
        return false;
    }

    CDate operator-(const int &right) const {
        // Prekopirovat obsah
        CDate r = *this;
        int daysToSub = abs(right);
        // Pocet dni do posledniho dne toho mesice
        int diff = r.d;
        // 2.1. - 1
        if (daysToSub < diff) {
            r.d = r.d - daysToSub;
            return r;
        } else {
            // Dojit na nejblizsiho 1.
            daysToSub -= r.d - 1;
            r.d = 1;
        }

        int lowerMonth = r.m - 1;
        int lowerYear = r.y;
        if (lowerMonth == 0) {
            lowerMonth = 12;
            lowerYear--;
        }
        // Pokud chces odecist (je unor) 31, pak jeste odecti cely mesic
        while (daysToSub >= daysInMonth(lowerMonth, lowerYear)) {
            daysToSub -= daysInMonth(lowerMonth, lowerYear);
            r.m = lowerMonth;
            r.y = lowerYear;

            lowerMonth--;
            if (lowerMonth == 0) {
                lowerMonth = 12;
                lowerYear--;
            }
        }
        if (daysToSub == 0) { return r; }
        daysToSub--;
        r.m--;
        if (r.m == 0) {
            r.m = 12;
            r.y--;
        }
        r.d = daysInMonth(r.m, r.y) - daysToSub;
        return r;
    }
};

#ifndef __PROGTEST__
int main ( void )
{
    ostringstream oss;
    istringstream iss;

    incrementDecrementTest();
   // assignTest();
   // substractOfTwoTest();
   // dateAssertAdd();
   // dateAssertSub();

    CDate a ( 2000, 1, 2 );
    CDate b ( 2010, 2, 3 );
    CDate c ( 2004, 2, 10 );
    oss . str ("");
    oss << a;
    assert ( oss . str () == "2000-01-02" );
    oss . str ("");
    oss << b;
    assert ( oss . str () == "2010-02-03" );
    oss . str ("");
    oss << c;
    assert ( oss . str () == "2004-02-10" );
    a = a + 1500;
    oss . str ("");
    oss << a;
    assert ( oss . str () == "2004-02-10" );
    //b = b - 40;
    b = b - 2000;
    oss . str ("");
    oss << b;
    assert ( oss . str () == "2004-08-13" );
     assert ( b - a == 185 );
     assert ( ( b == a ) == false );
     assert ( ( b != a ) == true );
     assert ( ( b <= a ) == false );
     assert ( ( b < a ) == false );
     assert ( ( b >= a ) == true );
     assert ( ( b > a ) == true );
     assert ( ( c == a ) == true );
     assert ( ( c != a ) == false );
     assert ( ( c <= a ) == true );
     assert ( ( c < a ) == false );
     assert ( ( c >= a ) == true );
     assert ( ( c > a ) == false );
      a = ++c;
      oss . str ( "" );
      oss << a << " " << c;
      assert ( oss . str () == "2004-02-11 2004-02-11" );
      a = --c;
      oss . str ( "" );
      oss << a << " " << c;
      assert ( oss . str () == "2004-02-10 2004-02-10" );
      a = c++;
      oss . str ( "" );
      oss << a << " " << c;
       assert ( oss . str () == "2004-02-10 2004-02-11" );
        a = c--;
        oss . str ( "" );
        oss << a << " " << c;
        assert ( oss . str () == "2004-02-11 2004-02-10" );
        iss . clear ();
        iss . str ( "2015-09-03" );
          assert ( ( iss >> a ) );
        oss . str ("");
         oss << a;
         assert ( oss . str () == "2015-09-03" );
         a = a + 70;
         oss . str ("");
         oss << a;
         assert ( oss . str () == "2015-11-12" );

          CDate d ( 2000, 1, 1 );
         try
         {
             CDate e ( 2000, 32, 1 );
             assert ( "No exception thrown!" == nullptr );
         }
         catch ( ... )
         {
         }
         iss . clear ();
         iss . str ( "2000-12-33" );
         assert ( ! ( iss >> d ) );
         oss . str ("");
         oss << d;
         assert ( oss . str () == "2000-01-01" );
         iss . clear ();
         iss . str ( "2000-11-31" );
         assert ( ! ( iss >> d ) );
         oss . str ("");
         oss << d;
          assert ( oss . str () == "2000-01-01" );
          iss . clear ();
          iss . str ( "2000-02-29" );
          assert ( ( iss >> d ) );
          oss . str ("");
          oss << d;
          assert ( oss . str () == "2000-02-29" );
          iss . clear ();
          iss . str ( "2001-02-29" );
          assert ( ! ( iss >> d ) );
          oss . str ("");
          oss << d;
          assert ( oss . str () == "2000-02-29" );
/*
          //-----------------------------------------------------------------------------
          // bonus test examples
          //-----------------------------------------------------------------------------
          CDate f ( 2000, 5, 12 );
          oss . str ("");
          oss << f;
          assert ( oss . str () == "2000-05-12" );
          oss . str ("");
          oss << date_format ( "%Y/%m/%d" ) << f;
          assert ( oss . str () == "2000/05/12" );
          oss . str ("");
          oss << date_format ( "%d.%m.%Y" ) << f;
          assert ( oss . str () == "12.05.2000" );
          oss . str ("");
          oss << date_format ( "%m/%d/%Y" ) << f;
          assert ( oss . str () == "05/12/2000" );
          oss . str ("");
          oss << date_format ( "%Y%m%d" ) << f;
          assert ( oss . str () == "20000512" );
          oss . str ("");
          oss << date_format ( "hello kitty" ) << f;
          assert ( oss . str () == "hello kitty" );
          oss . str ("");
          oss << date_format ( "%d%d%d%d%d%d%m%m%m%Y%Y%Y%%%%%%%%%%" ) << f;
          assert ( oss . str () == "121212121212050505200020002000%%%%%" );
          oss . str ("");
          oss << date_format ( "%Y-%m-%d" ) << f;
          assert ( oss . str () == "2000-05-12" );
          iss . clear ();
          iss . str ( "2001-01-1" );
          assert ( ! ( iss >> f ) );
          oss . str ("");
          oss << f;
          assert ( oss . str () == "2000-05-12" );
          iss . clear ();
          iss . str ( "2001-1-01" );
          assert ( ! ( iss >> f ) );
          oss . str ("");
          oss << f;
          assert ( oss . str () == "2000-05-12" );
          iss . clear ();
          iss . str ( "2001-001-01" );
          assert ( ! ( iss >> f ) );
          oss . str ("");
          oss << f;
          assert ( oss . str () == "2000-05-12" );
          iss . clear ();
          iss . str ( "2001-01-02" );
          assert ( ( iss >> date_format ( "%Y-%m-%d" ) >> f ) );
          oss . str ("");
          oss << f;
          assert ( oss . str () == "2001-01-02" );
          iss . clear ();
          iss . str ( "05.06.2003" );
          assert ( ( iss >> date_format ( "%d.%m.%Y" ) >> f ) );
          oss . str ("");
          oss << f;
          assert ( oss . str () == "2003-06-05" );
          iss . clear ();
          iss . str ( "07/08/2004" );
          assert ( ( iss >> date_format ( "%m/%d/%Y" ) >> f ) );
          oss . str ("");
          oss << f;daysInMonth
          assert ( oss . str () == "2004-07-08" );
          iss . clear ();
          iss . str ( "2002*03*04" );
          assert ( ( iss >> date_format ( "%Y*%m*%d" ) >> f ) );
          oss . str ("");
          oss << f;
          assert ( oss . str () == "2002-03-04" );
          iss . clear ();
          iss . str ( "C++09format10PA22006rulez" );
          assert ( ( iss >> date_format ( "C++%mformat%dPA2%Yrulez" ) >> f ) );
          oss . str ("");
          oss << f;
          assert ( oss . str () == "2006-09-10" );
          iss . clear ();
          iss . str ( "%12%13%2010%" );
          assert ( ( iss >> date_format ( "%%%m%%%d%%%Y%%" ) >> f ) );
          oss . str ("");
          oss << f;
          assert ( oss . str () == "2010-12-13" );

          CDate g ( 2000, 6, 8 );
          iss . clear ();
          iss . str ( "2001-11-33" );
          assert ( ! ( iss >> date_format ( "%Y-%m-%d" ) >> g ) );
          oss . str ("");
          oss << g;
          assert ( oss . str () == "2000-06-08" );
          iss . clear ();
          iss . str ( "29.02.2003" );
          assert ( ! ( iss >> date_format ( "%d.%m.%Y" ) >> g ) );
          oss . str ("");
          oss << g;
          assert ( oss . str () == "2000-06-08" );
          iss . clear ();
          iss . str ( "14/02/2004" );
          assert ( ! ( iss >> date_format ( "%m/%d/%Y" ) >> g ) );
          oss . str ("");
          oss << g;
          assert ( oss . str () == "2000-06-08" );
          iss . clear ();
          iss . str ( "2002-03" );
          assert ( ! ( iss >> date_format ( "%Y-%m" ) >> g ) );
          oss . str ("");
          oss << g;
          assert ( oss . str () == "2000-06-08" );
          iss . clear ();
          iss . str ( "hello kitty" );
          assert ( ! ( iss >> date_format ( "hello kitty" ) >> g ) );
          oss . str ("");
          oss << g;
          assert ( oss . str () == "2000-06-08" );
          iss . clear ();
          iss . str ( "2005-07-12-07" );
          assert ( ! ( iss >> date_format ( "%Y-%m-%d-%m" ) >> g ) );
          oss . str ("");
          oss << g;
          assert ( oss . str () == "2000-06-08" );
          iss . clear ();
          iss . str ( "20000101" );
          assert ( ( iss >> date_format ( "%Y%m%d" ) >> g ) );
          oss . str ("");
          oss << g;
          assert ( oss . str () == "2000-01-01" );*/

    return EXIT_SUCCESS;
}

void substractOfTwoTest(){
    CDate a(1900,5,12);
    CDate b(1900,5,12);
    int twohundred=200*365;
    for(int i=0;i<twohundred;i++){
        int difference=a-b;
        assert(difference==i);
        b++;
    }
    b=b-twohundred;
    for(int i=0;i<twohundred;i++){
        int difference=b-a;
        assert(difference==i);
        b++;
    }
}

void assignTest(){
    CDate date(1970,1,1);
    istringstream iss;
    iss.str("2000-01-01");
    assert(iss>>date);
    iss.clear();
    iss.str("2000-02-29");
    assert(iss>>date);
    iss.clear();
    iss.str("2001-02-29");
    assert(!(iss>>date));
    iss.clear();
    iss.str("xxxx-02-29");
    assert(!(iss>>date));
    iss.clear();
    iss.str("200102-29");
    assert(!(iss>>date));
    iss.clear();
    iss.str("2001 -02 -29");
    assert(!(iss>>date));


}

void incrementDecrementTest(){
    CDate a(1900,1,1);
    CDate b(1900,1,1);
    for(int i =0; i<200*365;i++){
        assert((a+i)==b++);
    }
    CDate c(1900,1,1);
    CDate d(1900,1,1);
    for(int i =0; i<200*365;i++){
        assert((c+i+1)==++d);
    }

    CDate e(2200,1,1);
    CDate f(2200,1,1);
    for(int i =0;i<200*365;i--) {
        CDate q = (e-i)-1;
        if (!(q == --f)) {
            assert(q == f);
        }
    }
}

void dateAssertAdd(){
    int assertYear=1900;
    int assertMonth=1;
    int assertDay=1;
    int twocenturies=72000;
    CDate a(assertYear,assertMonth,assertDay);
    for(int i=0; i<twocenturies;i++){
        // Preteceni dne
        if(assertDay > daysInMonth(assertMonth,assertYear)) {
            assertDay=1;
            assertMonth++;
            // Preteceni mesice
            if(assertMonth>12){
                assertYear++;
                assertMonth=1;
            }
        }

        assert((a+i)==CDate(assertYear,assertMonth,assertDay));
        assertDay++;
    }

    cout<<"add test done"<<endl;
}

void dateAssertSub(){
    int assertYear=2100;
    int assertMonth=1;
    int assertDay=1;
    int twocenturies=72000;
    CDate a(assertYear,assertMonth,assertDay);
    for(int i=0; i<twocenturies;i++){
        // Preteceni dne
        if(assertDay==0) {
            assertMonth--;
            if(assertMonth==0){
                assertYear--;
                assertMonth=12;
            }
            assertDay= daysInMonth(assertMonth,assertYear);
        }

        assert((a-i)==CDate(assertYear,assertMonth,assertDay));
        assertDay--;
    }

    cout<<"sub test done"<<endl;
}

#endif /* __PROGTEST__ */

int daysInMonth(int m,int y){
    int days=0;
    if(m==0){
        throw InvalidDateException();
    }
    switch (m) {
        case January:{
            days=31;
            break;
        }
        case February:{
            if(leapYear(y)){
                days=29;
            }else {
                days = 28;
            }
            break;
        }
        case March:{
            days=31;
            break;
        }
        case April:{
            days=30;
            break;
        }
        case May:{
            days=31;
            break;
        }
        case June:{
            days=30;
            break;
        }
        case July:{
            days=31;
            break;
        }
        case August:{
            days=31;
            break;
        }
        case September:{
            days=30;
            break;
        }
        case October:{
            days=31;
            break;
        }
        case November:{
            days=30;
            break;
        }
        case December:{
            days=31;
            break;
        }
        default:
            break;
    }
    return days;
}

bool leapYear(int y){
    if(y%400==0){
        return true;
    }else if(y%100==0){
        return false;
    }else if(y%4==0){
        return true;
    }else{
        return false;
    }
}
bool validateDaysInMonth(int m,int d,int y){
    if(d<=0){return false;}
    if(m>December||m<January){return false;}
    int days= daysInMonth(m,y);
    if(d>days){return false;}else{return true;}
}
