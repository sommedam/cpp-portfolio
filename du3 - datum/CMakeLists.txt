cmake_minimum_required(VERSION 3.21)
project(du3___datum)

set(CMAKE_CXX_STANDARD 14)

add_executable(du3___datum main.cpp)
