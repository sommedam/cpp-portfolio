#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <list>
#include <set>
#include <queue>
#include <stack>
#include <deque>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <memory>
using namespace std;
#endif /* __PROGTEST__ */


class CDate
{
    int y,m,d;
public:
    CDate(int y, int m, int d) : y(y), m(m), d(d) {}
    bool operator==(const CDate &rhs) const {
        if (this->m == rhs.m &&
            this->d == rhs.d &&
            this->y == rhs.y) {
            return true;
        } else {
            return false;
        }
    }
    friend std::ostream& operator<<(std::ostream &os, const CDate &x){
        os << x.y << "-";
        if (x.m < 10) { os << "0"; }
        os << x.m << "-";
        if (x.d < 10) { os << "0"; }
        os << x.d;
        return os;
    }
    bool operator<(const CDate &right)const {
        if (this->y < right.y) {
            return true;
        } else if (this->y == right.y) {
            if (this->m < right.m) {
                return true;
            } else if(this->m==right.m) {
                if (this->d < right.d) {
                    return true;
                }
            }
        }
        return false;
    }
};


void print(const list<pair<string, int>> &expiredListQtySorted){
    for(const auto& p : expiredListQtySorted){
        cout<<p.first<<": "<<p.second<<endl;
    }
}
void print(const map<string,vector<pair<int,CDate>>> &expiredListQtySorted){
    for(const auto& p : expiredListQtySorted){
        cout<<p.first<<" -----------"<<endl;
        for(const auto&b:p.second){
        cout<<b.second<<": "<<b.first<<endl;
        }
    }
}
void print(const map<CDate,map<string ,int>> &expiredListQtySorted){
    for(const auto& p : expiredListQtySorted){
        cout<<p.first<<" -----------"<<endl;
        for(const auto&b:p.second){
            cout<<b.first<<": "<<b.second<<endl;
        }
    }
}

bool isStringNearlyMatching(const string& a, const string& b) {
    int missmatchesCount=0;
    if (a.length() != b.length()) {
        return false;
    } else {
        for (long unsigned int i = 0; i < a.length(); i++) {
            if (a[i] != b[i]) {
                missmatchesCount++;
                if(missmatchesCount>1)
                return false;
            }
        }
    }
    return true;
}


class PendingTransaction{
private:
    pair<const string,int&> correspondingShoppingLine;
    vector<pair<int,CDate>>& batchesPtr;
    map<CDate, map<string,int>>& expiredMap;
    map<string,vector<pair<int,CDate>>>& productMapBatchDateSorted;
public:
    PendingTransaction(pair<const string, int&> &correspondingShoppingLine, vector<pair<int, CDate>> &batchesPtr,
                       map<CDate, map<string, int>> &expiredMap,map<string,vector<pair<int,CDate>>>&productMapBatchDateSorted) : correspondingShoppingLine(correspondingShoppingLine),
                                                                   batchesPtr(batchesPtr), expiredMap(expiredMap),productMapBatchDateSorted(productMapBatchDateSorted) {}
    PendingTransaction(pair<string, int> &correspondingShoppingLine, vector<pair<int, CDate>> &batchesPtr,
                       map<CDate, map<string, int>> &expiredMap,map<string,vector<pair<int,CDate>>>&productMapBatchDateSorted) : correspondingShoppingLine(pair<const string,int&>(correspondingShoppingLine.first,correspondingShoppingLine.second)),
                                                                   batchesPtr(batchesPtr), expiredMap(expiredMap),productMapBatchDateSorted(productMapBatchDateSorted) {}

    void doTransaction(){
        // Pocet odepsanych kusu celkove
        int removedCount=0;
        // Kolik zbyva k odecteni kusu na sklade
        int substractRemaining = correspondingShoppingLine.second;
        // Kolik celych sarzi se ma kompletne smazat.
        // Pro =0 znamena ze se nebude zadna cela sarze mazat pro =N se smaze N-1 sarzi.
        int removeUpperIndex=0;
        // Pro kazdou sarzi
        for (auto &batch: batchesPtr) {
            // Pokud se ma odecist min nez kolik je v sarzi tak odecist a odpis z listku je hotov
            if (substractRemaining < batch.first) {
                batch.first-=substractRemaining;
                removedCount+=substractRemaining;
                expiredMap[batch.second][correspondingShoppingLine.first]-=substractRemaining;
                break;
            } else {
                // Vymazat z indexovane sarze produkt, ktery je ted nulovy
                // Expired map se maze na miste, protoze slozitost je log n.
                // Vektor batches se maze pak dohromady, aby mazani kazdeho prvku nebylo o slozitosti n.
                // Pokud je sarze v mape nulova, tak mozna smazat sarzi todo
                auto expiredMapDate = expiredMap.find(batch.second);
                (*expiredMapDate).second.erase(correspondingShoppingLine.first);
                if((*expiredMapDate).second.empty()){
                    expiredMap.erase(expiredMapDate);
                }

                removeUpperIndex++;
                substractRemaining -= batch.first;
                removedCount+=batch.first;
            }
        }
        if(removeUpperIndex>0) {
            // Pokud remove upper index = 1, smaze se pouze prvni (nulta) sarze.
            int deleteFirstNelements = removeUpperIndex;
            //if (deleteFirstNelements == 0) {
            //    batchesPtr.erase(batchesPtr.begin());
            //} else {
                batchesPtr.erase(batchesPtr.begin(), batchesPtr.begin() + deleteFirstNelements);
            //}
            // Budou se mazat nulove zaznamy o produktech
            if(batchesPtr.empty()) {
                productMapBatchDateSorted.erase(correspondingShoppingLine.first);
            }
        }

        correspondingShoppingLine.second-=removedCount;
    }
};

struct DeleteEmptyLines{
    bool operator()(const pair<string,int>&line){
        return (line.second==0);
    }
};

struct PcsComparatorDESC{
    bool operator() (const pair<int,pair<string,int>&>& left,const int & right){
        return left.first>right;
    }
    bool operator() (const pair<string,int>& left,const int & right){
        return left.second>right;
    }
    bool operator() (const int& left,const  pair<string,int> & right){
        return left>right.second;
    }
};

struct CDateComparatorASC
{
    bool operator() (const CDate& left, const pair<CDate,map<string,int>> & right)
    {
        return left < right.first;
    }
    bool operator() (const pair<CDate,map<string,int>> & left,const CDate& right)
    {
        return left.first<right;
    }
    bool operator() (const CDate &left, const CDate & right)
    {
        return left < right;
    }
    bool operator() (const pair<int,CDate> & left, const CDate & right)
    {
        return left.second < right;
    }
    bool operator() (const CDate & left,const pair<int,CDate> & right)
    {
        return left < right.second;
    }
};

class CSupermarket
{
    map<string,vector<pair<int,CDate>>> productsMapBatchDateSorted;
    map<CDate, map<string,int>> expiredMap;
public:
    // default constructor
    CSupermarket& store (const string& name, CDate expiryDate,int count ) {
        // Zalozit vektor, pokud jeste neni pro unikatni produkt vytvoren
        if (productsMapBatchDateSorted.find(name) == productsMapBatchDateSorted.end()) {
            vector<pair<int, CDate>> v;
            v.emplace_back(count, expiryDate);
            productsMapBatchDateSorted[name] = v;
        } else {
            vector<pair<int, CDate>> &productBatches = productsMapBatchDateSorted[name];
            auto found = upper_bound(productBatches.begin(), productBatches.end(), expiryDate, CDateComparatorASC());
            if (found == productBatches.end()) {
                productBatches.emplace_back(count, expiryDate);
            } else {
                // Pokud najde zaznam se stejnou sarzi, tak pridat kusy k nemu
                if (found->second == expiryDate) {
                    found->first += count;
                } else {
                    productBatches.insert(found, pair<int, CDate>(count, expiryDate));
                }
            }
        }

        // Pokud neni namapovana sarze
        if (expiredMap.find(expiryDate) == expiredMap.end()) {
            map<string, int> nameMap;
            nameMap[name] = count;
            expiredMap[expiryDate] = nameMap;
        } else {
            // Pokud je sarze namapovana, ale neni namapovany nazev produktu v sarzi
            if (expiredMap[expiryDate].find(name) == expiredMap[expiryDate].end()) {
                expiredMap[expiryDate][name] = count;
            } else {
                expiredMap[expiryDate][name] += count;
            }
        }
        return *this;
    }

    void sell(list<pair<string,int>>&shoppingList) {
        vector<PendingTransaction> pendingTransactions;
        // vector je presne v poradi sarzi, tak jak se maji postupne odecitat
        // a1 - sarze nalezeneho vyrobku a2 - pocet k odecteni ze sarze a3 - kde je polozka na nakupnim listku
        //vector<tuple<vector<pair<int, CDate>> &, int, pair<string, int> &>> toSubstract;

        // Pro kazdy zaznam na nakupnim listku
        for (auto &shoppingListLine: shoppingList) {
            // Najit prsnou shodu
            auto found= productsMapBatchDateSorted.find(shoppingListLine.first);
            // Shoda existuje
            if (found != productsMapBatchDateSorted.end()) {
                // Vsechny sarze nalezeneho produktu
                pendingTransactions.emplace_back(shoppingListLine, found->second,expiredMap,productsMapBatchDateSorted);
            } else {
                // Hledat nepresne shody -----------
                int foundMatchingCount = 0;
                string foundMatch;
                for (auto& productInStorage:productsMapBatchDateSorted) {
                    if (isStringNearlyMatching(productInStorage.first, shoppingListLine.first)) {
                        foundMatchingCount++;
                        if (foundMatchingCount > 1) {
                            // Dva produkty, ktere maji shodu. Pak je to neplatne
                            // Pri break se nakupni listek v tomto predmetu neupravi
                            break;
                        }
                        foundMatch = productInStorage.first;
                    }
                }
                if (foundMatchingCount == 0||foundMatchingCount>1) {
                    // Nenalezen zadny produkt takoveho jmena. A to je chyba
                    // nic neupravovat
                }
                else {
                    // Nalezen jeden produkt podobneho jmena

                    pair<const string, int&> shoppingListLineCorrection(foundMatch,shoppingListLine.second);
                    pendingTransactions.emplace_back(shoppingListLineCorrection, productsMapBatchDateSorted[foundMatch],expiredMap,productsMapBatchDateSorted);
                }
            }
        }

        // Vse v poradku, muze probehnout transakce
        for(auto & transaction:pendingTransactions){
            transaction.doTransaction();
        }

        // Vymazat ze shopping listu nulove zaznamy. todo m*n
        shoppingList.erase(
                std::remove_if(shoppingList.begin(), shoppingList.end(), DeleteEmptyLines()),
                shoppingList.end());
    }

    // Zde staci vybudovat multimap az na konci. Zbytecne se udrzuje pri budovani.
    // nebyl cas to zlepsit
    list<pair<string,int>> expired (const CDate& date ) const{
        list<pair<string,int>> returnList;
        map<string,int> productQty;
        multimap<int, string> qtyProduct;

        // Prvni vyssi sarze, nez ta ktera je prosla
        auto firstNotRotten = lower_bound(expiredMap.begin(), expiredMap.end(), date, CDateComparatorASC());
        // Pro kazdou proslou sarzi
        while (firstNotRotten != expiredMap.begin()) {
            // Neprovede se ta prvni vyssi a begin se taky provede
            firstNotRotten--;
            // Pro kazdou polozku ze prosle sarze
            for (const auto &rottenItem: (*firstNotRotten).second) {
                auto productQtyItr = productQty.find(rottenItem.first);
                if(productQtyItr==productQty.end()){
                    // zalozit novy predmet
                    productQty[rottenItem.first]=rottenItem.second;
                    qtyProduct.insert(pair<int,string>(rottenItem.second,rottenItem.first));
                }else{
                    int valueBeforeChange = productQtyItr->second;
                    // Najit v mape predmet o stejnem jmene a kvantite
                    auto qtyProductPtr = qtyProduct.find(productQtyItr->second);
                    while(qtyProductPtr->second!=rottenItem.first){
                        qtyProductPtr++;
                    }
                    // Vymazat, bude se menit klic
                    // Edit 1
                    qtyProduct.erase(qtyProductPtr);
                    qtyProduct.insert(pair<int,string>(valueBeforeChange+rottenItem.second,rottenItem.first));
                    // Edit 2
                    productQtyItr->second+=rottenItem.second;
                }
            }
        }
        auto qtyProductItr = qtyProduct.end();
        while (qtyProductItr!=qtyProduct.begin()){
            qtyProductItr--;
            returnList.emplace_back(qtyProductItr->second,qtyProductItr->first);
        }
        return returnList;
    }

    void printMarkt(){
        print(productsMapBatchDateSorted);
        cout<<endl<<"EXPIRED MAP:";
        print(expiredMap);
    }

private:
};

void expiredTest(){
    CSupermarket s;
    s       . store ( "bread", CDate ( 2016, 4, 25 ), 100 )
            . store ( "bread", CDate ( 2016, 4, 30 ), 100 )
            . store ( "bread", CDate ( 2016, 4, 30 ), 100 )
            . store ( "butter", CDate ( 2020, 5, 10 ), 10 )
            . store ( "klobasa", CDate ( 2020, 5, 10 ), 10 )
            . store ( "lilek", CDate ( 2020, 5, 10 ), 10 )
            . store ( "ocet", CDate ( 2020, 5, 10 ), 10 )
            . store ( "okey", CDate ( 2016, 7, 18 ), 5 )
            . store ( "beer", CDate ( 2016, 8, 10 ), 50 )
            . store ( "klobasa", CDate ( 2016, 8, 10 ), 20 )
            . store ( "lilek", CDate ( 2016, 8, 10 ), 10 );
    list<pair<string,int>> expiredList=s.expired(CDate(2021,1,1));
    cout<<"Expired: "<<endl;
    print(expiredList);
}

void sellTest(){
    // Test finding removed
    CSupermarket s;
    list<pair<string,int>> shoppingList;
    s       . store ( "corny", CDate ( 2016, 4, 25 ), 100 );
    shoppingList.emplace_back("corny",100);
    shoppingList.emplace_back("dorny",100);
    s.sell(shoppingList);
  //  print(shoppingList);




    //Test multiple remove
    s       . store ( "bread", CDate ( 2016, 4, 25 ), 100 )
            . store ( "bread", CDate ( 2016, 4, 30 ), 100 )
            . store ( "bread", CDate ( 2016, 5, 10 ), 100 )
            . store ( "bread", CDate ( 2016, 5, 11 ), 10 );
    shoppingList.clear();
   // shoppingList.emplace_back("bread",301);
    s.sell(shoppingList);

    shoppingList.clear();
  //  s.printMarkt();
    shoppingList.emplace_back("bread",10);
    s.sell(shoppingList);
  //  cout<<"Bread not given: "<<shoppingList.begin()->second;

}

#ifndef __PROGTEST__
int main ( void )
{
   // expiredTest();
   // return 0;
    //   sellTest();
 //   equipmentTest();

    //test();
    CSupermarket s;
    s       . store ( "bread", CDate ( 2016, 4, 25 ), 100 )
            . store ( "bread", CDate ( 2016, 4, 30 ), 100 )
            . store ( "butter", CDate ( 2016, 5, 10 ), 10 )
            . store ( "okey", CDate ( 2016, 7, 18 ), 5 )
            . store ( "beer", CDate ( 2016, 8, 10 ), 50 );



    list<pair<string,int> > l0 = s . expired ( CDate ( 2018, 4, 30 ) );
    assert ( l0 . size () == 4 );
     assert ( ( l0 == list<pair<string,int> > { { "bread", 200 }, { "beer", 50 }, { "butter", 10 }, { "okey", 5 } } ) );

    list<pair<string,int> > l1 { { "bread", 2 }, { "Coke", 5 }, { "butter", 20 } };
    s . sell ( l1 );
    assert ( l1 . size () == 2 );
    assert ( ( l1 == list<pair<string,int> > { { "Coke", 5 }, { "butter", 10 } } ) );

    list<pair<string,int> > l2 = s . expired ( CDate ( 2016, 4, 30 ) );
    assert ( l2 . size () == 1 );
    assert ( ( l2 == list<pair<string,int> > { { "bread", 98 } } ) );

    list<pair<string,int> > l3 = s . expired ( CDate ( 2016, 5, 20 ) );
    assert ( l3 . size () == 1 );
    assert ( ( l3 == list<pair<string,int> > { { "bread", 198 } } ) );

    list<pair<string,int> > l4 { { "bread", 105 } };
    s . sell ( l4 );
    assert ( l4 . size () == 0 );
    assert ( ( l4 == list<pair<string,int> > {  } ) );

    list<pair<string,int> > l5 = s . expired ( CDate ( 2017, 1, 1 ) );
    assert ( l5 . size () == 3 );
    assert ( ( l5 == list<pair<string,int> > { { "bread", 93 }, { "beer", 50 }, { "okey", 5 } } ) );

    s . store ( "Coke", CDate ( 2016, 12, 31 ), 10 );

    list<pair<string,int> > l6 { { "Cake", 1 }, { "Coke", 1 }, { "cake", 1 }, { "coke", 1 }, { "cuke", 1 }, { "Cokes", 1 } };
    s . sell ( l6 );
    assert ( l6 . size () == 3 );
    assert ( ( l6 == list<pair<string,int> > { { "cake", 1 }, { "cuke", 1 }, { "Cokes", 1 } } ) );

    list<pair<string,int> > l7 = s . expired ( CDate ( 2017, 1, 1 ) );
    assert ( l7 . size () == 4 );
    assert ( ( l7 == list<pair<string,int> > { { "bread", 93 }, { "beer", 50 }, { "Coke", 7 }, { "okey", 5 } } ) );

    s . store ( "cake", CDate ( 2016, 11, 1 ), 5 );

    list<pair<string,int> > l8 { { "Cake", 1 }, { "Coke", 1 }, { "cake", 1 }, { "coke", 1 }, { "cuke", 1 } };
       s . sell ( l8 );
      assert ( l8 . size () == 2 );
   assert ( ( l8 == list<pair<string,int> > { { "Cake", 1 }, { "coke", 1 } } ) );

    list<pair<string,int> > l9 = s . expired ( CDate ( 2017, 1, 1 ) );
    assert ( l9 . size () == 5 );
    assert ( ( l9 == list<pair<string,int> > { { "bread", 93 }, { "beer", 50 }, { "Coke", 6 }, { "okey", 5 }, { "cake", 3 } } ) );

    list<pair<string,int> > l10 { { "cake", 15 }, { "Cake", 2 } };
    s . sell ( l10 );
    //s.printMarkt();
    assert ( l10 . size () == 2 );
    assert ( ( l10 == list<pair<string,int> > { { "cake", 12 }, { "Cake", 2 } } ) );

    list<pair<string,int> > l11 = s . expired ( CDate ( 2017, 1, 1 ) );
    assert ( l11 . size () == 4 );
    assert ( ( l11 == list<pair<string,int> > { { "bread", 93 }, { "beer", 50 }, { "Coke", 6 }, { "okey", 5 } } ) );

    list<pair<string,int> > l12 { { "Cake", 4 } };
    s . sell ( l12 );
    assert ( l12 . size () == 0 );
    assert ( ( l12 == list<pair<string,int> > {  } ) );

    list<pair<string,int> > l13 = s . expired ( CDate ( 2017, 1, 1 ) );
    assert ( l13 . size () == 4 );
    assert ( ( l13 == list<pair<string,int> > { { "bread", 93 }, { "beer", 50 }, { "okey", 5 }, { "Coke", 2 } } ) );

    list<pair<string,int> > l14 { { "Beer", 20 }, { "Coke", 1 }, { "bear", 25 }, { "beer", 10 } };
    s . sell ( l14 );
    assert ( l14 . size () == 1 );
    assert ( ( l14 == list<pair<string,int> > { { "beer", 5 } } ) );

    s . store ( "ccccb", CDate ( 2019, 3, 11 ), 100 )
            . store ( "ccccd", CDate ( 2019, 6, 9 ), 100 )
            . store ( "dcccc", CDate ( 2019, 2, 14 ), 100 );

    list<pair<string,int> > l15 { { "ccccc", 10 } };
    s . sell ( l15 );
    assert ( l15 . size () == 1 );
    assert ( ( l15 == list<pair<string,int> > { { "ccccc", 10 } } ) );/**/

    return EXIT_SUCCESS;
}
#endif /* __PROGTEST__ */