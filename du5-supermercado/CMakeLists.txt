cmake_minimum_required(VERSION 3.22)
project(du5_supermercado)

set(CMAKE_CXX_STANDARD 14)

add_executable(du5_supermercado main.cpp)
